<table class="container content" align="center" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: inherit;width: 580px;margin: 0 auto;">
	<tr style="padding: 0;vertical-align: top;text-align: left;">
		<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: top;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;border-collapse: collapse;">
			<table class="row note" style="border-spacing: 0;border-collapse: collapse;padding: 0px;vertical-align: top;text-align: left;width: 100%;position: relative;display: block;">
				<tr style="padding: 0;vertical-align: top;text-align: left;">
					<td class="wrapper last" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 10px 20px 0px 0px;vertical-align: top;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;position: relative;padding-right: 0px;border-collapse: collapse;">
						<h2 style="color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;padding: 0;margin: 5px 0 15px 0;text-align: left;line-height: 1.3;word-break: normal;font-size: 30px;display: block;">Hola <?php echo $user['User']['first_name'].' '.$user['User']['last_name']; ?>!</h2>
						<h4 style="color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;padding: 0;margin: 5px 0 15px 0;text-align: left;line-height: 1.3;word-break: normal;font-size: 22px;display: block;">Has solicitado un cambio de contraseña para ingresar a nuestro sistema</h4>
						<p style="margin: 0;margin-bottom: 10px;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;padding: 0;text-align: left;line-height: 19px;font-size: 14px;">
							Por favor has click en el siguiente enlace para reestablecer tu contraseña:
						</p>
						<!-- BEGIN: Note Panel -->
						<table class="twelve columns" style="margin-bottom: 10px;border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;margin: 0 auto;width: 580px;">
							<tr style="padding: 0;vertical-align: top;text-align: left;">
								<td class="panel" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 10px;vertical-align: top;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;background: #ECF8FF;border: 0;border-collapse: collapse;">
									<?php
										echo $this->Html->link(
											'http://www.ss.com/reestablecer-acceso/'.$code,
											$this->Html->url(array(
												'controller' => 'users',
												'action' => 'change_password',
												$code
											), true),
											array(
												'style' => 'color: #2ba6cb;text-decoration: none;'
											)
										);
									?>
								</td>
								<td class="expander" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: top;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;visibility: hidden;width: 0px;border-collapse: collapse;"></td>
							</tr>
						</table>
						<p style="margin: 0;margin-bottom: 10px;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;padding: 0;text-align: left;line-height: 19px;font-size: 14px;">
							Si al darle click al enlace no se abre tu navegador automáticamente, copia y pega el enlace en el navegador de tu preferencia.
						</p>
						<!-- END: Note Panel -->
					</td>
				</tr>
			</table>
			<span class="devider" style="border-bottom: 1px solid #eee;margin: 15px -15px;display: block;"></span>
			<table class="row" style="border-spacing: 0;border-collapse: collapse;padding: 0px;vertical-align: top;text-align: left;width: 100%;position: relative;display: block;">
				<tr style="padding: 0;vertical-align: top;text-align: left;">
					<td class="wrapper last" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 10px 20px 0px 0px;vertical-align: top;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;position: relative;padding-right: 0px;border-collapse: collapse;">
						<!-- BEGIN: Disscount Content -->
						<table class="twelve columns" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;margin: 0 auto;width: 580px;">
							<tr style="padding: 0;vertical-align: top;text-align: left;">
								<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0px 0px 10px;vertical-align: top;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;border-collapse: collapse;">
									<p style="margin: 0;margin-bottom: 10px;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;padding: 0;text-align: left;line-height: 19px;font-size: 14px;">
										Si no solicitaste un cambio de contraseña haz caso omiso a este mensaje
									</p>
								</td>
								<td class="expander" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: top;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;visibility: hidden;width: 0px;border-collapse: collapse;"></td>
							</tr>
						</table>
						<!-- END: Disscount Content -->
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>