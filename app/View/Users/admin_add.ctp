<?php
	echo $this->Html->css(array(
		'plugins/bootstrap-datepicker/css/datepicker',
		'plugins/bootstrap-fileinput/bootstrap-fileinput',
		'plugins/bootstrap-switch/css/bootstrap-switch.min',
		'plugins/select2/select2',
		'plugins/select2/select2-metronic'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
		'plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.es',
		'plugins/bootstrap-fileinput/bootstrap-fileinput',
		'plugins/bootstrap-switch/js/bootstrap-switch.min',
		'plugins/jquery-validation/dist/jquery.validate.min',
		'plugins/jquery-validation/localization/messages_es',
		'plugins/jquery-validation/dist/additional-methods.min',
		'plugins/select2/select2.min',
		'/js/users/admin_add'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Usuarios',
		array(
			'action' => 'index',
			'controller' => 'users',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Nuevo',
		array(
			'action' => 'add',
			'controller' => 'users',
			'admin' => true
			)
		);
?>

<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-plus"></i>Nuevo Usuario</div>
				<div class="actions">
					<?php
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Usuarios', array('controller'=>'users', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon blue-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<?php
				echo $this->Form->create('User',
					array(
						'inputDefaults' => array(
							'div' => false,
							'label' => false,
							'class' => 'form-control',
							'error' => false
						),
						'class' => 'form-horizontal form-row-seperated',
						'enctype' => 'multipart/form-data',
						'novalidate' => true
					)
				);
			?>
				<div class="form-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						Existen errores en el formulario. Por favor verifica tus datos.
					</div>
					<!-- Preffix -->
					<div class="form-group <?php echo $this->Form->isFieldError('title') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Título</label>
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-certificate"></i></span>
								<?php echo $this->Form->input('title'); ?>
							</div>
							<?php echo $this->Form->isFieldError('title') ? $this->Form->error('title', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
							<span class="help-block">Ej. Lic. , PhD, Prof.</span>
						</div>
					</div>
					<!-- END Preffix -->
					<!-- Name -->
					<div class="form-group <?php echo $this->Form->isFieldError('name') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Nombre(s)<span class="required">*</span></label>
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<?php echo $this->Form->input('name'); ?>
							</div>
							<?php echo $this->Form->isFieldError('name') ? $this->Form->error('name', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Name -->
					<!-- Paternal Surname Name -->
					<div class="form-group <?php echo $this->Form->isFieldError('paternal_surname') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Apellido paterno<span class="required">*</span></label>
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<?php echo $this->Form->input('paternal_surname'); ?>
							</div>
							<?php echo $this->Form->isFieldError('paternal_surname') ? $this->Form->error('paternal_surname', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Paternal Surname Name -->
					<!-- Maternal Surname Name -->
					<div class="form-group <?php echo $this->Form->isFieldError('maternal_surname') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Apellido materno<span class="required">*</span></label>
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<?php echo $this->Form->input('maternal_surname'); ?>
							</div>
							<?php echo $this->Form->isFieldError('maternal_surname') ? $this->Form->error('maternal_surname', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Maternal Surname Name -->
					<!-- Birthday -->
					<div class="form-group <?php echo $this->Form->isFieldError('birthday') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Fecha de nacimiento</label>
						<div class="col-md-4">
							<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-end-date="-18y">
								<?php echo $this->Form->input('birthday', array('type'=>'text', 'readonly'=>true)); ?>
								<span class="input-group-btn">
									<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
							</div>
							<?php echo $this->Form->isFieldError('birthday') ? $this->Form->error('birthday', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
							<!-- /input-group -->
						</div>
					</div>
					<!-- END Birthday -->
					<!-- Email -->
					<div class="form-group <?php echo $this->Form->isFieldError('email') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Correo electrónico<span class="required">*</span></label>
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<?php echo $this->Form->input('email'); ?>
							</div>
							<?php echo $this->Form->isFieldError('email') ? $this->Form->error('email', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Email -->
					<!-- Password -->
					<div class="form-group <?php echo $this->Form->isFieldError('new_password') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Contraseña<span class="required">*</span></label>
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-key"></i></span>
								<?php echo $this->Form->input('new_password', array('type' => 'password')); ?>
							</div>
							<?php echo $this->Form->isFieldError('new_password') ? $this->Form->error('new_password', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Password -->
					<!-- Password Confirm -->
					<div class="form-group <?php echo $this->Form->isFieldError('password_confirm') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Confirmar contraseña<span class="required">*</span></label>
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-key"></i></span>
								<?php echo $this->Form->input('password_confirm', array('type' => 'password')); ?>
							</div>
							<?php echo $this->Form->isFieldError('password_confirm') ? $this->Form->error('password_confirm', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Password Confirm -->
					<!-- Role -->
					<div class="form-group">
						<label class="control-label col-md-3">Rol</label>
						<div class="col-md-3">
							<?php
								$options = array('1'=>'Super Usuario', '2'=>'Administrador');
								echo $this->Form->input('role', array('options'=>$options, 'default'=>'2'));
							?>
							<span class="help-block">Selecciona un rol de usuario.</span>
						</div>
					</div>
					<!-- END Role -->
					<!-- Active status -->
					<div class="form-group">
						<label class="control-label col-md-3">Activo</label>
						<div class="col-md-9">
							<?php
								echo $this->Form->checkbox('active', array('type' => 'checkbox', 'class' => 'make-switch', 'checked' => true, 'data-on-label' => 'Sí', 'data-off-label' => 'No', 'data-on' => 'success', 'data-off' => 'danger'));
							?>
							<span class="help-block unique">Indica si el usuario se encontrará activo o inactivo</span>
						</div>
					</div>
					<!-- END Active status -->
					<!-- Image upload -->
					<div class="form-group last <?php echo $this->Form->isFieldError('image') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Imagen</label>
						<div class="col-md-9">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
									<?php echo $this->Html->image('/assets/img/img-holdit.png'); ?>
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
								<div>
									<?php echo $this->Form->isFieldError('image') ? $this->Form->error('image', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
									<span class="btn default btn-file">
										<span class="fileinput-new"> Seleccionar</span>
										<span class="fileinput-exists"> Cambiar</span>
										<?php echo $this->Form->file('image'); ?>
									</span>
									<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> Eliminar</a>
								</div>
							</div>
							<div class="clearfix margin-top-10">
								<span class="label label-danger"> NOTA!</span>
								<span>El archivo no debe ser mayor a <strong>5 MB</strong> y debe estar en formato <strong>JPG, PNG o GIF</strong>.</span>
							</div>
						</div>
					</div>
					<!-- END Image upload -->
				</div>
				<div class="form-actions fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-offset-3 col-md-9">
								<?php
									echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
										array('type' => 'submit', 'class' => 'btn green')
										).'&nbsp;';

									$cancelOptions = array('action' => 'index', 'controller' => 'users', 'admin' => true);
									echo $this->Form->button('Cancelar',
										array(
											'type' => 'button',
											'class' => 'btn red',
											'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
											)
										);
								?>
							</div>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'FormValidation.init();',
		array('inline' => false)
	);
?>