<?php
	echo $this->Html->css(array(
		'css/pages/profile'
		), null, array('inline' => false)
	);

	$this->Html->addCrumb('Usuarios',
		array(
			'action' => 'index',
			'controller' => 'users',
			'admin' => true
			)
		);
	$this->Html->addCrumb('Ver', null);

	$this->Html->addCrumb($user['User']['fullname'],
		array(
			'action' => 'view',
			'controller' => 'users',
			'admin' => true,
			$user['User']['id'],
			Format::clean($user['User']['fullname'])
			)
		);
?>
<div class="row profile">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">&nbsp;</div>
				<div class="actions">
					<?php
						echo AppController::isSuperUser() || (!AppController::isSuperUser() && $ownAccount) ? $this->Html->link('<i class="fa fa-pencil"></i> Editar', array('controller'=>'users', 'action' => 'edit', 'admin'=>true, $user['User']['id'], Format::clean($user['User']['fullname'])), array('class'=>'btn default yellow-stripe', 'escape'=>false)).'&nbsp;' : '';
						echo AppController::isSuperUser() ? $this->Html->link('<i class="m-icon-swapleft"></i> Usuarios', array('controller'=>'users', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon blue-stripe', 'escape'=>false)) : '';
					?>
				</div>
			</div>
			<div class="portlet-body form">
				<div class="row">
					<div class="col-md-3">
						<ul class="list-unstyled profile-nav">
							<li>
								<?php
									$img = !empty($user['User']['image']) ? 'images/user_img_'.$user['User']['image'] : '/assets/img/img-holdit-edit-profile.png';
									echo $this->Html->image($img, array('class'=>'img-responsive'));
									echo $ownAccount ? $this->Html->link('editar', array('action'=>'edit', 'admin'=>true, $user['User']['id'], Format::clean($user['User']['fullname']), '#' => 'avatar'), array('class'=>'profile-edit')) : '';
								?>
							</li>
						</ul>
					</div>
					<div class="col-md-9">
						<!-- BEGIN FORM-->
						<form class="form-horizontal" role="form">
							<h2 class="margin-bottom-20"> Información de Usuario - <?php echo $user['User']['fullname']; ?> </h2>
							<h3 class="form-section">Personal</h3>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Título:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo !empty($user['User']['title']) ? $user['User']['title'] : '-'; ?>
											</p>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Teléfono:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo !empty($user['User']['telephone']) ? $user['User']['telephone'] : '-'; ?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Nombre(s):</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo $user['User']['name']; ?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">R.F.C:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo !empty($user['User']['rfc']) ? $user['User']['rfc'] : '-'; ?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Apellido Paterno:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo $user['User']['paternal_surname']; ?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">CURP:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo !empty($user['User']['curp']) ? $user['User']['curp'] : '-'; ?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Apellido Materno:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo $user['User']['maternal_surname']; ?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Correo electrónico:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo $this->Text->autoLinkEmails($user['User']['email']); ?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Fecha de Nacimiento:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo !empty($user['User']['birthday']) ? $this->Time->format($user['User']['birthday'], '%d/%m/%y') : '-'; ?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->

							<h3 class="form-section">Cuenta</h3>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Tipo:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php
													echo $user['User']['role'] == 1 ? '<span class="label label-default">Super Usuario</span>' : '<span class="label label-info">Administrador</span>';
												?>
											</p>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label col-md-4">Estado:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php
													echo $user['User']['active'] == 1 ? '<span class="label label-success">Activo</span>' : '<span class="label label-danger">Inactivo</span>';
												?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
						</form>
						<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>