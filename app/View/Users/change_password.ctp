<?php
	echo $this->Html->script(array(
		'/js/users/change_password',
		), array('inline' => false)
	);
?>
<!-- BEGIN LOGIN FORM -->
<?php
	echo $this->Form->create(
		'User',
		array(
			'url' => array('action' => 'change_password', 'controller' => 'users', $code),
			'class' => 'change-password-form',
			'inputDefaults'=> array(
				'label' => false,
				'div' => false,
				'class' => 'form-control placeholder-no-fix',
				'autocomplete' => 'off',
				'error' => false
			),
			'novalidate' => true
		)
	);
?>
	<h3 class="form-title">Cambiar Contraseña</h3>
	<p>Ingresa una nueva contraseña para reestablecer tu acceso al sistema</p>
	<div class="form-group <?php echo $this->Form->isFieldError('new_password') ? 'has-error' : ''; ?>">
		<label class="control-label visible-ie8 visible-ie9">Contraseña nueva</label>
		<div class="input-icon">
			<i class="fa fa-lock"></i>
			<?php echo $this->Form->input('new_password', array('placeholder'=>'Contraseña nueva', 'type'=>'password')); ?>
		</div>
		<?php echo $this->Form->isFieldError('new_password') ? $this->Form->error('new_password', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
	</div>
	<div class="form-group <?php echo $this->Form->isFieldError('password_confirm') ? 'has-error' : ''; ?>">
		<label class="control-label visible-ie8 visible-ie9">Confirmar contraseña</label>
		<div class="input-icon">
			<i class="fa fa-lock"></i>
			<?php echo $this->Form->input('password_confirm', array('placeholder'=>'Confirmar contraseña', 'type'=>'password')); ?>
		</div>
		<?php echo $this->Form->isFieldError('password_confirm') ? $this->Form->error('password_confirm', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
	</div>
	<div class="form-actions">
		<?php
			echo $this->Form->button('Guardar '.'<i class="fa fa-check-circle-o"></i>', array('type'=> 'submit', 'class'=>'btn blue pull-right'));
		?>
	</div>
<?php echo $this->Form->end(); ?>
<!-- END LOGIN FORM -->

<?php
	echo $this->Html->scriptBlock(
		'ChangePassword.init();',
		array('inline' => false)
	);
?>