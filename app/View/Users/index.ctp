<?php
echo $this->Html->css(array(
	'portal/css/pages/prices'
	), null, array('inline' => false)
);
?>
<!-- BEGIN BREADCRUMBS -->
<div class="row breadcrumbs margin-bottom-20">
	<div class="container">
		<div class="col-md-4 col-sm-4">
			<h1>Investigadores</h1>
		</div>
		<div class="col-md-8 col-sm-8">
			<ul class="pull-right breadcrumb">
				<li>
					<?php
						echo $this->Html->link('Inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => false));
					?>
				</li>
				<li class="active">Investigadores</li>
			</ul>
		</div>
	</div>
</div>
<!-- END BREADCRUMBS -->

<!-- BEGIN CONTAINER -->
<div class="container min-hight">
	<!-- BEGIN ABOUT INFO -->
	<div class="row margin-bottom-30">
		<!-- BEGIN INFO BLOCK -->
		<div class="col-md-12 space-mobile">
			<h2>Investigadores Anáhuac Mayab</h2>
			<p>La universidad Anáhuac Mayab se enorgullece de trabajar con los mejores investigadores de la región, que al mismo tiempo cumplen la función de docente e investigador. Cada investigador tiene toda una carrera que ha quedado marcada, trabajos, proyectos, artículos y numerosas investigaciones que le brindan a esta universidad la vanguardia en el área de ciencias e ingeniería. A continuación se presentan a los investigadores activos en la universidad.</p>
			<!-- BEGIN LISTS -->
			<!-- <div class="row front-lists-v1">
				<div class="col-md-6">
					<ul class="list-unstyled margin-bottom-20">
						<li><i class="fa fa-check"></i> Officia deserunt molliti</li>
						<li><i class="fa fa-check"></i> Consectetur adipiscing </li>
						<li><i class="fa fa-check"></i> Deserunt fpicia</li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="list-unstyled">
						<li><i class="fa fa-check"></i> Officia deserunt molliti</li>
						<li><i class="fa fa-check"></i> Consectetur adipiscing </li>
						<li><i class="fa fa-check"></i> Deserunt fpicia</li>
					</ul>
				</div>
			</div> -->
			<!-- END LISTS -->
		</div>
		<!-- END INFO BLOCK -->
	</div>
	<!-- END ABOUT INFO -->

	<!-- BEGIN OUR TEAM -->
	<div class="row front-team">
		<ul class="list-unstyled">
			<?php if (isset($users) && !empty($users)) : ?>
				<?php foreach ($users as $scientist) : ?>
				<li class="col-md-3 space-mobile">
					<div class="thumbnail">
						<?php
							$image = isset($scientist['User']) && !empty($scientist['User']['image']) ? 'images/user_profile_' . $scientist['User']['image'] : '/assets/portal/img/preview-holdit.png';
							echo $this->Html->image($image);
						?>
						<h3>
							<?php echo $this->Html->link($scientist['User']['title'].' '.$scientist['User']['fullname'], array('controller' => 'profiles', 'action' => 'view', $scientist['Profile']['id'], Format::clean($scientist['User']['fullname']), 'admin' => false)); ?>
							<small><?php echo $scientist['Profile']['profession']; ?></small>
						</h3>
					</div>
				</li>
				<?php endforeach; ?>
			<?php else: ?>
				<p>No se encuentran investigadores</p>
			<?php endif; ?>
		</ul>
	</div>
	<!-- END OUR TEAM -->
</div>
<!-- END CONTAINER