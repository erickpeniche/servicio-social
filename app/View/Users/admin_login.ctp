<!-- BEGIN LOGIN FORM -->
<?php
	echo $this->Form->create(
		'User',
		array(
			'action' => 'login',
			'class' => 'login-form',
			'inputDefaults'=> array(
				'label' => false,
				'div' => false,
				'class' => 'form-control placeholder-no-fix',
				//'autocomplete' => 'off',
				'error' => false
			),
			'novalidate' => true
		)
	);
?>
	<h3 class="form-title">Inicia Sesión</h3>
	<div class="form-group <?php echo $this->Form->isFieldError('email') ? 'has-error' : ''; ?>">
		<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
		<label class="control-label visible-ie8 visible-ie9">Correo Electrónico</label>
		<div class="input-icon">
			<i class="fa fa-envelope"></i>
			<?php echo $this->Form->input('email', array('placeholder'=>'Correo Electrónico')); ?>
		</div>
		<?php echo $this->Form->isFieldError('email') ? $this->Form->error('email', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
	</div>
	<div class="form-group <?php echo $this->Form->isFieldError('password') ? 'has-error' : ''; ?>">
		<label class="control-label visible-ie8 visible-ie9">Contraseña</label>
		<div class="input-icon">
			<i class="fa fa-lock"></i>
			<?php echo $this->Form->input('password', array('placeholder'=>'Contraseña')); ?>
		</div>
		<?php echo $this->Form->isFieldError('password') ? $this->Form->error('password', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
	</div>
	<div class="form-actions">
		<label class="checkbox">
			<input type="checkbox" name="remember" value="1"/> No cerrar sesión
		</label>
		<?php
			echo $this->Form->button('Iniciar '.'<i class="m-icon-swapright m-icon-white"></i>', array('type'=> 'submit', 'class'=>'btn blue pull-right'));
		?>
	</div>
	<!-- <div class="login-options">
		<h4>O conéctate con tus redes sociales</h4>
		<ul class="social-icons">
			<li>
				<a class="facebook" data-original-title="facebook" href="#">
				</a>
			</li>
			<li>
				<a class="twitter" data-original-title="Twitter" href="#">
				</a>
			</li>
			<li>
				<a class="googleplus" data-original-title="Goole Plus" href="#">
				</a>
			</li>
			<li>
				<a class="linkedin" data-original-title="Linkedin" href="#">
				</a>
			</li>
		</ul>
	</div> -->
	<div class="forget-password">
		<h4>Olvidaste tu contraseña?</h4>
		<p>
			Haz click <a href="javascript:;"  id="forget-password">aquí</a>
			para restablecer tu contraseña.
		</p>
	</div>
<?php echo $this->Form->end(); ?>
<!-- END LOGIN FORM -->
<!-- BEGIN FORGOT PASSWORD FORM -->
<?php
	echo $this->Form->create(
		'User',
		array(
			'action' => 'forget_password',
			'class' => 'forget-form',
			'inputDefaults'=> array(
				'label' => false,
				'div' => false,
				'class' => 'form-control placeholder-no-fix',
				//'autocomplete' => 'off',
				'error' => false
			),
			'novalidate' => true
		)
	);
?>
	<h3>Olvidaste tu contraseña?</h3>
	<p>Ingresa tu dirección de correo electrónico para recibir instrucciones de cómo restablecer tu contraseña.</p>
	<div class="form-group <?php echo $this->Form->isFieldError('recovery_email') ? 'has-error' : ''; ?>">
		<div class="input-icon">
			<i class="fa fa-envelope"></i>
			<?php echo $this->Form->input('recovery_email', array('placeholder'=>'Correo Electrónico')); ?>
		</div>
		<?php echo $this->Form->isFieldError('recovery_email') ? $this->Form->error('recovery_email', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
	</div>
	<div class="form-actions">
		<button type="button" id="back-btn" class="btn">
		<i class="m-icon-swapleft"></i> Atrás </button>
		<button type="submit" data-loading-text="Enviando..." class="loading-btn-frm btn blue pull-right">
		Enviar <i class="m-icon-swapright m-icon-white"></i>
		</button>
	</div>
<?php echo $this->Form->end(); ?>
<!-- END FORGOT PASSWORD FORM -->