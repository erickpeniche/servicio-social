<?php
	echo $this->Html->css(array(
		'plugins/select2/select2',
		'plugins/select2/select2-metronic',
		'plugins/data-tables/DT_bootstrap'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/select2/select2.min',
		'plugins/data-tables/jquery.dataTables',
		'plugins/data-tables/DT_bootstrap',
		'/js/users/admin_index'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Usuarios',
		array(
			'action' => 'index',
			'controller' => 'users',
			'admin' => true
			)
		);
?>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-group"></i>Usuarios</div>
				<div class="actions">
					<?php echo $this->Html->link('<i class="fa fa-plus"></i> Nuevo</a>',
						array(
							'action' => 'add',
							'controller' => 'users'
						),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="users_table">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Apellido Paterno</th>
								<th>Apellido Materno</th>
								<th>Rol</th>
								<th>E-mail</th>
								<th>Estado</th>
								<th>Creado</th>
								<th>Modificado</th>
								<th class="actions">Acciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($users as $user): ?>
							<tr>
								<td><?php echo $user['User']['name']; ?></td>
								<td><?php echo $user['User']['paternal_surname']; ?></td>
								<td><?php echo $user['User']['maternal_surname']; ?></td>
								<td>
									<?php
										echo $user['User']['role'] == 1 ? '<span class="label label-default">Super Usuario</span>' : '<span class="label label-info">Administrador</span>';
									?>
								</td>
								<td><?php echo $user['User']['email']; ?></td>
								<td>
									<?php
										echo $user['User']['active'] == 1 ? '<span class="label label-success">Activo</span>' : '<span class="label label-danger">Inactivo</span>';
									?>
								</td>
								<td><?php echo $user['User']['created']; ?></td>
								<td><?php echo $user['User']['modified']; ?></td>
								<td class="actions" width="20%">
									<div class="btn-group btn-group btn-group-justified">
										<?php
											echo $this->Html->link('<i class="fa fa-info-circle"></i> Ver', array('action' => 'view', $user['User']['id'], Format::clean($user['User']['fullname'])), array('class'=>'btn btn-sm btn-info', 'escape'=>false));
											echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $user['User']['id'], Format::clean($user['User']['fullname'])), array('class'=>'btn btn-sm yellow', 'escape'=>false));
											echo $loggedUser['id'] != $user['User']['id'] ? $this->Html->link('<i class="fa fa-trash-o"></i> Eliminar', array('action' => 'delete', $user['User']['id']), array('id' => 'delete-btn', 'class'=>'btn btn-sm red', 'escape' => false, 'data-id' => $user['User']['id'], 'data-name' => $user['User']['fullname'])) : '';
										?>
									</div>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'AdminIndex.init();',
		array('inline' => false)
	);
?>