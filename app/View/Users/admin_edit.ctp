<?php
	echo $this->Html->css(array(
		'plugins/bootstrap-datepicker/css/datepicker',
		'plugins/bootstrap-fileinput/bootstrap-fileinput',
		'plugins/bootstrap-switch/css/bootstrap-switch.min',
		'plugins/select2/select2',
		'plugins/select2/select2-metronic',
		'css/pages/profile'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
		'plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.es',
		'plugins/bootstrap-fileinput/bootstrap-fileinput',
		'plugins/bootstrap-switch/js/bootstrap-switch.min',
		'plugins/jquery-validation/dist/jquery.validate.min',
		'plugins/jquery-validation/localization/messages_es',
		'plugins/jquery-validation/dist/additional-methods.min',
		'plugins/select2/select2.min',
		'/js/users/admin_edit'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Usuarios',
		array(
			'action' => 'index',
			'controller' => 'users',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Editar',
		array(
			'action' => 'edit',
			'controller' => 'users',
			'admin' => true,
			$user['User']['id'],
			Format::clean($user['User']['fullname'])
			)
		);

	$this->Html->addCrumb($user['User']['fullname'],
		array(
			'action' => 'view',
			'controller' => 'users',
			'admin' => true,
			$user['User']['id'],
			Format::clean($user['User']['fullname'])
			)
		);
?>

<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-pencil"></i>Editar Usuario</div>
				<div class="actions">
					<?php
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Usuarios', array('controller'=>'users', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon blue-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row profile-account">
					<div class="col-md-3">
						<ul class="ver-inline-menu tabbable margin-bottom-10">
							<li class="active">
								<a data-toggle="tab" href="#personal">
									<i class="fa fa-user"></i> Información Personal
								</a>
								<span class="after"></span>
							</li>
							<?php if (AppController::isSuperUser() && !$ownAccount) : ?>
							<li>
								<a data-toggle="tab" href="#cuenta">
									<i class="fa fa-cog"></i> Cuenta
								</a>
							</li>
							<?php endif; ?>
							<?php if($ownAccount): ?>
							<li>
								<a data-toggle="tab" href="#avatar">
									<i class="fa fa-picture-o"></i> Avatar
								</a>
							</li>
							<li>
								<a data-toggle="tab" href="#contrasena">
									<i class="fa fa-lock"></i> Cambiar Contraseña
								</a>
							</li>
							<?php endif; ?>
						</ul>
					</div>
					<div class="col-md-9">
						<div class="tab-content">
							<div id="personal" class="tab-pane active">
								<div class="form">
									<!-- BEGIN FORM-->
									<?php
										echo $this->Form->create('User',
											array(
												'inputDefaults' => array(
													'div' => false,
													'label' => false,
													'class' => 'form-control',
													'error' => false
												),
												'class' => 'form-horizontal form-row-seperated personal-form',
												'novalidate' => true
											)
										);

										echo $this->Form->hidden('id');
									?>
										<div class="form-body">
											<div class="alert alert-danger display-hide">
												<button class="close" data-close="alert"></button>
												Existen errores en el formulario. Por favor verifica tus datos.
											</div>
											<!-- Preffix -->
											<div class="form-group <?php echo $this->Form->isFieldError('title') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">Título</label>
												<div class="col-md-9">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-certificate"></i></span>
														<?php echo $this->Form->input('title'); ?>
													</div>
													<?php echo $this->Form->isFieldError('title') ? $this->Form->error('title', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
													<span class="help-block">Ej. Lic. , PhD, Prof.</span>
												</div>
											</div>
											<!-- END Preffix -->
											<!-- Name -->
											<div class="form-group <?php echo $this->Form->isFieldError('name') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">Nombre(s)<span class="required">*</span></label>
												<div class="col-md-9">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user"></i></span>
														<?php echo $this->Form->input('name'); ?>
													</div>
													<?php echo $this->Form->isFieldError('name') ? $this->Form->error('name', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
												</div>
											</div>
											<!-- END Name -->
											<!-- Paternal Surname Name -->
											<div class="form-group <?php echo $this->Form->isFieldError('paternal_surname') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">Apellido paterno<span class="required">*</span></label>
												<div class="col-md-9">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user"></i></span>
														<?php echo $this->Form->input('paternal_surname'); ?>
													</div>
													<?php echo $this->Form->isFieldError('paternal_surname') ? $this->Form->error('paternal_surname', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
												</div>
											</div>
											<!-- END Paternal Surname Name -->
											<!-- Maternal Surname Name -->
											<div class="form-group <?php echo $this->Form->isFieldError('maternal_surname') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">Apellido materno<span class="required">*</span></label>
												<div class="col-md-9">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user"></i></span>
														<?php echo $this->Form->input('maternal_surname'); ?>
													</div>
													<?php echo $this->Form->isFieldError('maternal_surname') ? $this->Form->error('maternal_surname', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
												</div>
											</div>
											<!-- END Maternal Surname Name -->
											<!-- Birthday -->
											<div class="form-group <?php echo $this->Form->isFieldError('birthday') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">Fecha de nacimiento</label>
												<div class="col-md-4">
													<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-end-date="-18y">
														<?php echo $this->Form->input('birthday', array('type'=>'text', 'readonly'=>true)); ?>
														<span class="input-group-btn">
															<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
														</span>
													</div>
													<?php echo $this->Form->isFieldError('birthday') ? $this->Form->error('birthday', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
													<!-- /input-group -->
												</div>
											</div>
											<!-- END Birthday -->
											<!-- Telephone -->
											<div class="form-group <?php echo $this->Form->isFieldError('telephone') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">Teléfono</label>
												<div class="col-md-9">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-phone"></i></span>
														<?php echo $this->Form->input('telephone'); ?>
													</div>
													<?php echo $this->Form->isFieldError('telephone') ? $this->Form->error('telephone', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
												</div>
											</div>
											<!-- END Telephone -->
											<!-- RFC -->
											<div class="form-group <?php echo $this->Form->isFieldError('rfc') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">R.F.C.</label>
												<div class="col-md-9">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user"></i></span>
														<?php echo $this->Form->input('rfc'); ?>
													</div>
													<?php echo $this->Form->isFieldError('rfc') ? $this->Form->error('rfc', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
												</div>
											</div>
											<!-- END RFC -->
											<!-- Curp -->
											<div class="form-group <?php echo $this->Form->isFieldError('curp') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">CURP</label>
												<div class="col-md-9">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-user"></i></span>
														<?php echo $this->Form->input('curp'); ?>
													</div>
													<?php echo $this->Form->isFieldError('curp') ? $this->Form->error('curp', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
												</div>
											</div>
											<!-- END Curp -->
										</div>
										<div class="form-actions fluid">
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-offset-3 col-md-9">
														<?php
															echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
																array('type' => 'submit', 'class' => 'btn green loading-btn-frm', 'data-loading-text' => 'Guardando...')
																).'&nbsp;';

															$cancelOptions = array('action' => 'index', 'controller' => 'users', 'admin' => true);
															echo $this->Form->button('Cancelar',
																array(
																	'type' => 'button',
																	'class' => 'btn red cancel-btn-frm',
																	'onclick' => "location.href='".$this->Html->url($cancelOptions)."'",
																	'data-loading-text' => 'Cancelar'
																	)
																);
														?>
													</div>
												</div>
											</div>
										</div>
									<?php echo $this->Form->end(); ?>
									<!-- END FORM-->
								</div>
							</div>
							<?php if (AppController::isSuperUser() && !$ownAccount) : ?>
							<div id="cuenta" class="tab-pane">
								<div class="form">
									<!-- BEGIN FORM-->
									<?php
										echo $this->Form->create('User',
											array(
												'inputDefaults' => array(
													'div' => false,
													'label' => false,
													'class' => 'form-control',
													'error' => false
												),
												'class' => 'form-horizontal form-row-seperated account-form',
												'novalidate' => true
											)
										);

										echo $this->Form->hidden('id');
									?>
										<div class="form-body">
											<!-- Role -->
											<div class="form-group">
												<label class="control-label col-md-3">Rol</label>
												<div class="col-md-3">
													<?php
														$options = array('1'=>'Super Usuario', '2'=>'Administrador');
														echo $this->Form->input('role', array('options'=>$options, 'default'=>'2'));
													?>
													<span class="help-block">Selecciona un rol de usuario.</span>
												</div>
											</div>
											<!-- END Role -->
											<!-- Active status -->
											<div class="form-group">
												<label class="control-label col-md-3">Activo</label>
												<div class="col-md-9">
													<?php
														echo $this->Form->checkbox('active', array('type' => 'checkbox', 'class' => 'make-switch', 'data-on-label' => 'Sí', 'data-off-label' => 'No', 'data-on' => 'success', 'data-off' => 'danger'));
													?>
													<span class="help-block unique">Indica si el usuario se encontrará activo o inactivo</span>
												</div>
											</div>
											<!-- END Active status -->
										</div>
										<div class="form-actions fluid">
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-offset-3 col-md-9">
														<?php
															echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
																array('type' => 'submit', 'class' => 'btn green loading-btn-frm', 'data-loading-text' => 'Guardando...')
																).'&nbsp;';

															$cancelOptions = array('action' => 'index', 'controller' => 'users', 'admin' => true);
															echo $this->Form->button('Cancelar',
																array(
																	'type' => 'button',
																	'class' => 'btn red cancel-btn-frm',
																	'onclick' => "location.href='".$this->Html->url($cancelOptions)."'",
																	'data-loading-text' => 'Cancelar'
																	)
																);
														?>
													</div>
												</div>
											</div>
										</div>
									<?php echo $this->Form->end(); ?>
									<!-- END FORM-->
								</div>
							</div>
							<?php endif; ?>
							<?php if($ownAccount): ?>
							<div id="avatar" class="tab-pane">
								<div class="form">
									<!-- BEGIN FORM-->
									<?php
										echo $this->Form->create('User',
											array(
												'inputDefaults' => array(
													'div' => false,
													'label' => false,
													'class' => 'form-control',
													'error' => false
												),
												'class' => 'form-horizontal form-row-seperated avatar-form',
												'enctype' => 'multipart/form-data',
												'novalidate' => true
											)
										);

										echo $this->Form->hidden('id');
									?>
										<div class="form-body">
											<div class="alert alert-danger display-hide">
												<button class="close" data-close="alert"></button>
												Existen errores en el formulario. Por favor verifica tus datos.
											</div>
											<!-- Image upload -->
											<div class="form-group last <?php echo $this->Form->isFieldError('image') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">Imagen</label>
												<div class="col-md-9">
													<div class="fileinput fileinput-new" data-provides="fileinput">
														<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
															<?php
																$image = !empty($user['User']['image']) ? 'images/user_img_'.$user['User']['image'] : '/assets/img/img-holdit.png';
																echo $this->Html->image($image);
															?>
														</div>
														<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
														<div>
															<?php echo $this->Form->isFieldError('image') ? $this->Form->error('image', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
															<span class="btn default btn-file">
																<span class="fileinput-new"> Seleccionar</span>
																<span class="fileinput-exists"> Cambiar</span>
																<?php echo $this->Form->file('image'); ?>
															</span>
															<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> Eliminar</a>
														</div>
													</div>
													<div class="clearfix margin-top-10">
														<span class="label label-danger"> NOTA!</span>
														<span>El archivo no debe ser mayor a <strong>5 MB</strong> y debe estar en formato <strong>JPG, PNG o GIF</strong>.</span>
													</div>
												</div>
											</div>
											<!-- END Image upload -->
										</div>
										<div class="form-actions fluid">
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-offset-3 col-md-9">
														<?php
															echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
																array('type' => 'submit', 'class' => 'btn green')
																).'&nbsp;';

															$cancelOptions = array('action' => 'index', 'controller' => 'users', 'admin' => true);
															echo $this->Form->button('Cancelar',
																array(
																	'type' => 'button',
																	'class' => 'btn red',
																	'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
																	)
																);
														?>
													</div>
												</div>
											</div>
										</div>
									<?php echo $this->Form->end(); ?>
									<!-- END FORM-->
								</div>
							</div>
							<div id="contrasena" class="tab-pane">
								<div class="form">
									<!-- BEGIN FORM-->
									<?php
										echo $this->Form->create('User',
											array(
												'inputDefaults' => array(
													'div' => false,
													'label' => false,
													'class' => 'form-control',
													'error' => false
												),
												'class' => 'form-horizontal form-row-seperated password-form',
												'novalidate' => true,
												'url' => array(
													'action' => 'edit',
													'controller' => 'users',
													'admin' => true,
													'#' => 'contrasena'
												)
											)
										);

										echo $this->Form->hidden('id');
									?>
										<div class="form-body">
											<div class="note note-warning">
												<h4 class="block">Atención!</h4>
												<p>Al cambiar tu contraseña tu sesión actual finalizará y seras redireccionado a la vista de inicio de sesión</p>
											</div>
											<div class="alert alert-danger display-hide">
												<button class="close" data-close="alert"></button>
												Existen errores en el formulario. Por favor verifica tus datos.
											</div>
											<!-- Current Password -->
											<div class="form-group <?php echo $this->Form->isFieldError('_current_password') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">Contraseña<span class="required">*</span></label>
												<div class="col-md-4">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-key"></i></span>
														<?php echo $this->Form->input('_current_password', array('type' => 'password')); ?>
													</div>
													<?php echo $this->Form->isFieldError('_current_password') ? $this->Form->error('_current_password', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
												</div>
											</div>
											<!-- END Current Password -->
											<!-- New Password -->
											<div class="form-group <?php echo $this->Form->isFieldError('new_password') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">Nueva contraseña<span class="required">*</span></label>
												<div class="col-md-4">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-key"></i></span>
														<?php echo $this->Form->input('new_password', array('type' => 'password')); ?>
													</div>
													<?php echo $this->Form->isFieldError('new_password') ? $this->Form->error('new_password', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
												</div>
											</div>
											<!-- END New Password -->
											<!-- Password Confirm -->
											<div class="form-group <?php echo $this->Form->isFieldError('password_confirm') ? 'has-error' : ''; ?>">
												<label class="control-label col-md-3">Confirmar contraseña<span class="required">*</span></label>
												<div class="col-md-4">
													<div class="input-group">
														<span class="input-group-addon"><i class="fa fa-key"></i></span>
														<?php echo $this->Form->input('password_confirm', array('type' => 'password')); ?>
													</div>
													<?php echo $this->Form->isFieldError('password_confirm') ? $this->Form->error('password_confirm', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
												</div>
											</div>
											<!-- END Password Confirm -->
										</div>
										<div class="form-actions fluid">
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-offset-3 col-md-9">
														<?php
															echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
																array('type' => 'submit', 'class' => 'btn green')
																).'&nbsp;';

															$cancelOptions = array('action' => 'index', 'controller' => 'users', 'admin' => true);
															echo $this->Form->button('Cancelar',
																array(
																	'type' => 'button',
																	'class' => 'btn red',
																	'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
																	)
																);
														?>
													</div>
												</div>
											</div>
										</div>
									<?php echo $this->Form->end(); ?>
									<!-- END FORM-->
								</div>
							</div>
							<?php endif; ?>
						</div>
					</div>
					<!--end col-md-9-->
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'Edit.init();',
		array('inline' => false)
	);
?>