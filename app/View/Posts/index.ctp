        <!-- BEGIN BREADCRUMBS -->
        <div class="row breadcrumbs margin-bottom-40">
            <div class="container">
                <div class="col-md-4 col-sm-4">
                    <h1>Blog</h1>
                </div>
                <div class="col-md-8 col-sm-8">
                    <ul class="pull-right breadcrumb">
                        <li>
                        	<?php
                        		echo $this->Html->link('Inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => false));
                        	?>
                        </li>
                        <li class="active">Blog</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END BREADCRUMBS -->

		<!-- BEGIN CONTAINER -->
		<div class="container min-hight">
			<!-- BEGIN BLOG -->
			<div class="row">
				<!-- BEGIN LEFT SIDEBAR -->
				<div class="col-md-9 col-sm-9 blog-posts margin-bottom-40">
					<?php foreach ($posts as $key => $post): ?>
					<div class="row">
						<div class="col-md-4 col-sm-4">
							<?php
								$image = isset($post['Post']) && !empty($post['Post']['image']) ? '/img/images/post_list_' . $post['Post']['image'] : '';
								echo $this->Html->image($image, array('class' => 'img-responsive'));
							?>
						</div>
						<div class="col-md-8 col-sm-8">
							<h2>
								<?php echo $this->Html->link($post['Post']['title'], array('controller' => 'posts', 'action' => 'view', $post['Post']['id'], Format::clean($post['Post']['title']))) ?>
							</h2>
							<ul class="blog-info">
								<li>
									<i class="fa fa-user"></i>
									<?php echo $post['User']['fullname']; ?>
								</li>
								<li>
									<i class="fa fa-calendar"></i>
									<?php echo $this->Time->format($post['Post']['published_date'], '%d/%m/%Y - %H:%M'); ?>
								</li>
								<?php if (isset($post['Post']) && !empty($post['Post']['tags'])) : ?>
								<?php $tags = explode(',', $post['Post']['tags']); ?>
								<li><i class="fa fa-tags"></i>
									<?php foreach ($tags as $tag) : ?>
										 <?php echo $tag.', '; ?>
									<?php endforeach; ?>
								</li>
								<?php endif; ?>
							</ul>
							<p><?php echo $this->Text->truncate(strip_tags($post['Post']['content']), 450, array('ellipsis'=>'...', 'exact'=>true)); ?></p>
							<?php echo $this->Html->link('Leer más <i class="icon-angle-right"></i>', array('controller' => 'posts', 'action' => 'view', $post['Post']['id'], Format::clean($post['Post']['title'])), array('class' => 'more', 'escape' => false)); ?>
						</div>
					</div>
					<?php if ($key > (count($posts)-1)) : ?>
					<hr class="blog-post-sep">
					<?php endif; ?>
					<?php endforeach; ?>
				</div>
				<!-- END LEFT SIDEBAR -->
			</div>
			<!-- END BEGIN BLOG -->
		</div>
		<!-- END CONTAINER -->