<?php
	echo $this->Html->css(array(
		'plugins/select2/select2',
		'plugins/select2/select2-metronic',
		'plugins/data-tables/DT_bootstrap'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/select2/select2.min',
		'plugins/data-tables/jquery.dataTables',
		'plugins/data-tables/DT_bootstrap',
		'/js/posts/admin_index'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Blog',
		array(
			'action' => 'index',
			'controller' => 'posts',
			'admin' => true
		)
	);
?>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box grey">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-comments"></i>Posts</div>
				<div class="actions">
					<?php  echo $this->Html->link('<i class="fa fa-plus"></i> Agregar</a>',
						array(
							'action' => 'add',
							'controller' => 'posts'
						),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="posts_table">
						<thead>
							<th>Título</th>
							<th>Autor</th>
							<th>Fecha de publicación</th>
							<th>Fecha de creación</th>
							<th class="actions">Acciones</th>
						</thead>
						<tbody>
							<?php foreach ($posts as $post): ?>
							<tr>
								<td><?php echo isset($post['Post']) && !empty($post['Post']['title']) ? $post['Post']['title'] : '-'; ?></td>
								<td>
									<?php echo isset($post['User']) && !empty($post['User']['fullname']) ? $this->Html->link($post['User']['fullname'], array('controller' => 'users', 'action' => 'view', $post['User']['id'], Format::clean($post['User']['fullname']))) : '-'; ?>
									</td>
								<td>
									<?php if($post['Post']['published']==1): ?>
									<?php echo $this->Time->format($post['Post']['published_date'], '%d/%m/%Y - %H:%M'); ?>
									<?php else: ?>
									<span class="label label-warning">NO PUBLICADO</span>
									<?php endif; ?>
								</td>
								<td><?php echo $this->Time->format($post['Post']['created'], '%d/%m/%Y - %H:%M'); ?></td>
								<td class="actions">
									<div class="btn-group btn-group btn-group-justified">
										<?php
											echo $this->Html->link('<i class="fa fa-info-circle"></i> Ver', array('action' => 'view', $post['Post']['id'], Format::clean($post['Post']['title'])), array('class'=>'btn btn-sm btn-info', 'escape'=>false));

											if ($post['Post']['user_id'] == $loggedUser['id']) :
											echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $post['Post']['id'], Format::clean($post['Post']['title'])), array('class'=>'btn btn-sm yellow', 'escape'=>false));
											endif;

											if (AppController::isSuperUser()) :
											echo $this->Html->link('<i class="fa fa-trash-o"></i> Eliminar', array('action' => 'delete', $post['Post']['id']), array('id' => 'delete-btn', 'class'=>'btn btn-sm red', 'escape' => false, 'data-id' => $post['Post']['id'], 'data-name' => $post['Post']['title']));

											endif;

											if ($post['Post']['user_id'] == $loggedUser['id']) :
											$option = array('icon' => $post['Post']['published'] == 0 ? 'eye-slash':'eye', 'title' => $post['Post']['published'] == 0 ? 'Publicar':'Ocultar');
											echo $this->Html->link('<i class="fa fa-'.$option['icon'].'"></i>', array('action' => 'toggle_publish', $post['Post']['id']), array('class'=>'btn btn-sm default', 'escape' => false, 'title' => $option['title']));
											endif;
										?>
									</div>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'AdminIndex.init();',
		array('inline' => false)
	);
?>