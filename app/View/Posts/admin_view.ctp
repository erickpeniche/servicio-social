<?php
	echo $this->Html->css(array(
		/*'plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min',
		'plugins/jquery-file-upload/css/jquery.fileupload',
		'plugins/jquery-file-upload/css/jquery.fileupload-ui',*/
		'css/pages/blog'
		), null, array('inline' => false)
	);

	/*echo $this->Html->script(array(
		'plugins/jquery-file-upload/js/vendor/jquery.ui.widget',
		'plugins/jquery-file-upload/js/vendor/tmpl.min',
		'plugins/jquery-file-upload/js/vendor/load-image.min',
		'plugins/jquery-file-upload/js/vendor/canvas-to-blob.min',
		'plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min',
		'plugins/jquery-file-upload/js/jquery.iframe-transport',
		'plugins/jquery-file-upload/js/jquery.fileupload',
		'plugins/jquery-file-upload/js/jquery.fileupload-process',
		'plugins/jquery-file-upload/js/jquery.fileupload-image',
		'plugins/jquery-file-upload/js/jquery.fileupload-audio',
		'plugins/jquery-file-upload/js/jquery.fileupload-video',
		'plugins/jquery-file-upload/js/jquery.fileupload-validate',
		'plugins/jquery-file-upload/js/jquery.fileupload-ui'
		), array('inline' => false)
	);*/
?>
<!--[if (gte IE 8)&(lt IE 10)]>
<?php //echo $this->Html->script(array('plugins/jquery-file-upload/js/cors/jquery.xdr-transport'), array('inline' => false)); ?>
<![endif]-->
<?php
	/*echo $this->Html->script(array(
		'/js/posts/admin_view'
		), array('inline' => false)
	);*/
	$this->Html->addCrumb('Blog',
		array(
			'action' => 'index',
			'controller' => 'posts',
			'admin' => true
			)
		);

	$this->Html->addCrumb($post['Post']['title'],
		array(
			'action' => 'view',
			'controller' => 'posts',
			'admin' => true,
			$post['Post']['id'],
			Format::clean($post['Post']['title'])
			)
		);
	$tags = explode(',', $post['Post']['tags']);
?>

<div class="row profile">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">&nbsp;</div>
				<div class="actions">
					<?php
						echo $ownAccount ? $this->Html->link('<i class="fa fa-pencil"></i> Editar', array('controller'=>'posts', 'action' => 'edit', 'admin'=>true, $post['Post']['id'], Format::clean($post['Post']['title'])), array('class'=>'btn default yellow-stripe', 'escape'=>false)).'&nbsp;' : '';
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Blog', array('controller'=>'posts', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon green-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12 blog-page">
						<div class="row">
							<div class="col-md-9 article-block">
								<h3><?php echo $post['Post']['title']; ?></h3>
								<div class="blog-tag-data">
									<?php if (isset($post['Post']) && !empty($post['Post']['image'])) : ?>
										<?php
											echo $this->Html->image('/img/images/post_view_'.$post['Post']['image'], array('class' => 'img-responsive'))
										?>
									<?php endif; ?>
									<div class="row">
										<div class="col-md-6">
											<ul class="list-inline blog-tags">
												<?php if (isset($post['Post']) && !empty($post['Post']['tags'])) : ?>
												<li>
													<i class="fa fa-tags"></i>
													<?php foreach ($tags as $tag) : ?>
													<a >
														 <?php echo $tag; ?>
													</a>
													<?php endforeach; ?>
												</li>
												<?php endif; ?>
											</ul>
										</div>
										<div class="col-md-6 blog-tag-data-inner">
											<ul class="list-inline">
												<li>
													<i class="fa fa-calendar"></i>
													<a>
														<?php echo $this->Time->format($post['Post']['created'], '%B %d, %Y'); ?>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<!--end news-tag-data-->
								<div>
									<?php echo $post['Post']['content']; ?>
								</div>
								<!-- <hr>
								<div class="media">
									<h3>Comments</h3>
									<a href="#" class="pull-left">
										<img alt="" src="assets/img/blog/9.jpg" class="media-object">
									</a>
									<div class="media-body">
										<h4 class="media-heading">Media heading
										<span>
											 5 hours ago /
											<a href="#">
												 Reply
											</a>
										</span>
										</h4>
										<p>
											 Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
										</p>
										<hr>
										<div class="media">
											<a href="#" class="pull-left">
												<img alt="" src="assets/img/blog/5.jpg" class="media-object">
											</a>
											<div class="media-body">
												<h4 class="media-heading">Media heading
												<span>
													 17 hours ago /
													<a href="#">
														 Reply
													</a>
												</span>
												</h4>
												<p>
													 Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
												</p>
											</div>
										</div>
										<hr>
										<div class="media">
											<a href="#" class="pull-left">
												<img alt="" src="assets/img/blog/7.jpg" class="media-object">
											</a>
											<div class="media-body">
												<h4 class="media-heading">Media heading
												<span>
													 2 days ago /
													<a href="#">
														 Reply
													</a>
												</span>
												</h4>
												<p>
													 Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
												</p>
											</div>
										</div>
									</div>
								</div> -->
							</div>
							<!--end col-md-9-->
							<div class="col-md-3 blog-sidebar">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<h3 class="panel-title">Datos del Post</h3>
									</div>
									<div class="panel-body">
										<dl>
											<dt>Fecha de publicación</dt>
											<dd>
												<?php echo isset($post['Post']) && $post['Post']['published'] == 1 ? $this->Time->format($post['Post']['published_date'], '%d/%m/%Y - %H:%M') : '<span class="label label-warning">NO PUBLICADO</span>'; ?>
											</dd><br>
											<dt>Fecha de creación</dt>
											<dd><?php echo $this->Time->format($post['Post']['created'], '%d/%m/%Y - %H:%M'); ?></dd><br>
											<dt>Última modificación</dt>
											<dd><?php echo $this->Time->format($post['Post']['modified'], '%d/%m/%Y - %H:%M'); ?></dd>
										</dl>
									</div>
								</div>
							</div>
							<!--end col-md-3-->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>