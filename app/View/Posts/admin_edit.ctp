<?php
	echo $this->Html->css(array(
		'plugins/bootstrap-fileinput/bootstrap-fileinput',
		'plugins/bootstrap-switch/css/bootstrap-switch.min',
		'plugins/jquery-tags-input/jquery.tagsinput',
		'plugins/select2/select2',
		'plugins/select2/select2-metronic'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/ckeditor/ckeditor',
		'plugins/bootstrap-fileinput/bootstrap-fileinput',
		'plugins/bootstrap-switch/js/bootstrap-switch.min',
		'plugins/jquery-validation/dist/jquery.validate.min',
		'plugins/jquery-validation/localization/messages_es',
		'plugins/jquery-validation/dist/additional-methods.min',
		'plugins/select2/select2.min',
		'plugins/jquery-tags-input/jquery.tagsinput.min',
		'/js/posts/admin_edit'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Blog',
		array(
			'action' => 'index',
			'controller' => 'posts',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Editar',
		array(
			'action' => 'edit',
			'controller' => 'posts',
			'admin' => true,
			$post['Post']['id'],
			Format::clean($post['Post']['title'])
		)
	);

	$this->Html->addCrumb($post['Post']['title'], null);
?>
<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-pencil"></i>Editar Post</div>
				<div class="actions">
					<?php
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Blog', array('controller'=>'posts', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon green-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<?php
				echo $this->Form->create('Post',
					array(
						'inputDefaults' => array(
							'div' => false,
							'label' => false,
							'class' => 'form-control',
							'error' => false
						),
						'class' => 'form-horizontal form-row-seperated',
						'enctype' => 'multipart/form-data',
						'novalidate' => true
					)
				);

				echo $this->Form->hidden('id');
			?>
				<div class="form-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						Existen errores en el formulario. Por favor verifica tus datos.
					</div>

					<!-- Title -->
					<div class="form-group <?php echo $this->Form->isFieldError('title') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Título<span class="required">*</span></label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-bookmark"></i></span>
								<?php echo $this->Form->input('title'); ?>
							</div>
							<?php echo $this->Form->isFieldError('title') ? $this->Form->error('title', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Title -->

					<!-- Content -->
					<div class="form-group <?php echo $this->Form->isFieldError('content') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Descripción<span class="required">*</span></label>
						<div class="col-md-9">
							<?php echo $this->Form->textarea('content', array('class' => 'ckeditor form-control', 'rows' => 6)); ?>
							<?php echo $this->Form->isFieldError('content') ? $this->Form->error('content', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Content -->

					<!-- Tags -->
					<div class="form-group">
						<label class="control-label col-md-3">Etiquetas</label>
						<div class="col-md-9">
							<?php echo $this->Form->input('tags', array('class'=>'form-control tags')); ?>
						</div>
					</div>
					<!-- END Tags -->

					<!-- Image upload -->
					<div class="form-group <?php echo $this->Form->isFieldError('image') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Imagen</label>
						<div class="col-md-9">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
									<?php
										$image = !empty($post['Post']['image']) ? '/img/images/post_img_edt_'.$post['Post']['image'] : '/assets/img/img-holdit.png';
										echo $this->Html->image($image);
									?>
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
								<div>
									<?php echo $this->Form->isFieldError('image') ? $this->Form->error('image', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
									<span class="btn default btn-file">
										<span class="fileinput-new"> Seleccionar</span>
										<span class="fileinput-exists"> Cambiar</span>
										<?php echo $this->Form->file('image'); ?>
									</span>
									<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> Eliminar</a>
								</div>
							</div>
							<div class="clearfix margin-top-10">
								<span class="label label-danger"> NOTA!</span>
								<span>El archivo no debe ser mayor a <strong>5 MB</strong> y debe estar en formato <strong>JPG, PNG o GIF</strong>.</span>
							</div>
						</div>
					</div>
					<!-- END Image upload -->

					<!-- Published status -->
					<div class="form-group last">
						<label class="control-label col-md-3">¿Publicar el Post?</label>
						<div class="col-md-9">
							<?php
								echo $this->Form->checkbox('published', array('type' => 'checkbox', 'class' => 'make-switch', 'data-on-label' => 'Sí', 'data-off-label' => 'No', 'data-on' => 'success', 'data-off' => 'danger'));
							?>
						</div>
					</div>
					<!-- END Published status -->

				</div>
				<div class="form-actions fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-offset-3 col-md-9">
								<?php
									echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
										array('type' => 'submit', 'class' => 'btn green')
										).'&nbsp;';

									$cancelOptions = array('action' => 'index', 'controller' => 'posts', 'admin' => true);
									echo $this->Form->button('Cancelar',
										array(
											'type' => 'button',
											'class' => 'btn red',
											'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
											)
										);
								?>
							</div>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'AdminEdit.init();',
		array('inline' => false)
	);
?>
