<?php
echo $this->Html->css(array(
	'portal/plugins/fancybox/source/jquery.fancybox',
	), null, array('inline' => false)
);

echo $this->Html->script(array(
	'portal/plugins/fancybox/source/jquery.fancybox.pack',
	), array('inline' => false)
);
?>

<!-- BEGIN BREADCRUMBS -->
<div class="row breadcrumbs margin-bottom-40">
	<div class="container">
		<div class="col-md-4 col-sm-4">
			<h1>Post</h1>
		</div>
		<div class="col-md-8 col-sm-8">
			<ul class="pull-right breadcrumb">
				<li>
					<?php
						echo $this->Html->link('Inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => false));
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link('Blog', array('controller' => 'posts', 'action' => 'index', 'admin' => false));
					?>
				</li>
				<li class="active"><?php echo $post['Post']['title']; ?></li>
			</ul>
		</div>
	</div>
</div>
<!-- END BREADCRUMBS -->

<!-- BEGIN CONTAINER -->
<div class="container min-hight">
	<!-- BEGIN BLOG -->
	<div class="row">
		<!-- BEGIN LEFT SIDEBAR -->
		<div class="col-md-9 blog-item margin-bottom-40">
			<div class="blog-item-img">
				<!-- BEGIN CAROUSEL -->
				<div class="front-carousel">
					<div id="myCarousel" class="carousel slide">
						<!-- Carousel items -->
						<div class="carousel-inner">
							<div class="active item">
								<?php
									$image = isset($post['Post']) && !empty($post['Post']['image']) ? 'images/post_view_'.$post['Post']['image'] : '/assets/portal/img/slider-hold.png';
									echo $this->Html->image($image);
								?>
							</div>
						</div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
				</div>
				<!-- END CAROUSEL -->
			</div>
			<h2><?php echo $post['Post']['title']; ?></h2>
			<?php echo $post['Post']['content']; ?>
			<ul class="blog-info">
				<li><i class="fa fa-user"></i> por <?php echo $post['User']['fullname']; ?></li>
				<li><i class="fa fa-calendar"></i> <?php echo $this->Time->format($post['Post']['published_date'], '%d/%m/%Y - %H:%M'); ?></li>
			</ul>
		</div>
		<!-- END LEFT SIDEBAR -->
	</div>
	<!-- END BEGIN BLOG -->
</div>
<!-- END CONTAINER -->