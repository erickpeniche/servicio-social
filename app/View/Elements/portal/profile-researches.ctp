<?php
	$wrapper = 0;
	$boxes = 0;
?>
<!-- BEGIN TAB GRID CONTENT -->
<?php if(!empty($researches)): ?>
	<?php $boxes = 0; ?>
	<?php foreach($researches as $research): ?>
		<?php $boxes++ ?>
		<?php if ($boxes%4 == 1) : ?>
			<?php $wrapper++;?>
		<div class="row margin-bottom-40">
		<?php endif ?>
			<div class="col-md-3">
				<div class="pricing">
					<?php if ($research['Research']['working'] == 1) : ?>
					<div class="pricing-head">
						<h3><?php echo $research['Research']['title']; ?> <span><?php echo $research['Research']['working'] == 1 ? 'Actualmente involucrado' : ''; ?></span></h3>
					</div>
					<?php else: ?>
					<h3><?php echo $research['Research']['title']; ?></h3>
					<?php endif; ?>
					<ul class="pricing-content list-unstyled">
						<li><?php echo $this->Text->truncate(strip_tags($research['Research']['description']), 150, array('ellipsis'=>'...', 'exact'=>true)); ?></li>
					</ul>
					<div class="pricing-footer">
						<?php
							echo $this->Html->link('Ver', array('controller' => 'researches', 'action' => 'view', $research['Research']['id'], Format::clean($research['Research']['title']), 'admin' => false), array('class' => 'btn theme-btn'));
						?>
					</div>
				</div>
			</div>
		<?php if ($boxes%4 == 0 || sizeof($researches) == $boxes) : ?>
		</div>
		<?php endif ?>
	<?php endforeach; ?>
<?php else: ?>
<div class="row margin-bottom-40">
	<div class="col-md-12">
		<p>No se encuentran investigaciones</p>
	</div>
</div>
<?php endif; ?>