<?php
	$activeMenus = array(
		'home' => array(
			'active' => isset($activeMenu) && $activeMenu == 'home' ? 'active' : ''
		),
		'users' => array(
			'active' => isset($activeMenu) && $activeMenu == 'users' ? 'active' : ''
		),
		'projects' => array(
			'active' => isset($activeMenu) && $activeMenu == 'projects' ? 'active' : ''
		),
		'publications' => array(
			'active' => isset($activeMenu) && $activeMenu == 'publications' ? 'active' : ''
		),
		'blog' => array(
			'active' => isset($activeMenu) && $activeMenu == 'blog' ? 'active' : ''
		)
	);
?>
<div class="navbar-collapse collapse">
	<ul class="nav navbar-nav">
		<li class="<?php echo $activeMenus['home']['active']; ?>">
				<?php
					echo $this->Html->link('Inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => false));
				?>
		</li>
		<li class="<?php echo $activeMenus['users']['active']; ?>">
				<?php
					echo $this->Html->link('Investigadores', array('controller' => 'users', 'action' => 'index', 'admin' => false));
				?>
		</li>
		<li class="<?php echo $activeMenus['projects']['active']; ?>">
				<?php
					echo $this->Html->link('Proyectos', array('controller' => 'projects', 'action' => 'index', 'admin' => false));
				?>
		</li>
		<li class="<?php echo $activeMenus['publications']['active']; ?>">
				<?php
					echo $this->Html->link('Publicaciones', array('controller' => 'publications', 'action' => 'index', 'admin' => false));
				?>
		</li>
		<li class="<?php echo $activeMenus['blog']['active']; ?>">
			<?php
				echo $this->Html->link('Blog', array('controller' => 'posts', 'action' => 'index', 'admin' => false));
			?>
		</li>
	</ul>
</div>