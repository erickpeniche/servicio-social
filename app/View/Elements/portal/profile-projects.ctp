<?php
	$wrapper = 0;
	$boxes = 0;
?>
<!-- BEGIN TAB GRID CONTENT -->
<?php if(!empty($projects)): ?>
	<?php $boxes = 0; ?>
	<?php foreach($projects as $project): ?>
		<?php $boxes++ ?>
		<?php if ($boxes%4 == 1) : ?>
			<?php $wrapper++;?>
		<div class="row margin-bottom-40">
		<?php endif ?>
			<div class="col-md-3">
				<div class="pricing">
					<?php if ($project['Project']['working'] == 1) : ?>
					<div class="pricing-head">
						<h3><?php echo $project['Project']['title']; ?> <span><?php echo $project['Project']['working'] == 1 ? 'Actualmente involucrado' : ''; ?></span></h3>
					</div>
					<?php else: ?>
					<h3><?php echo $project['Project']['title']; ?></h3>
					<?php endif; ?>
					<ul class="pricing-content list-unstyled">
						<li><?php echo $this->Text->truncate(strip_tags($project['Project']['description']), 150, array('ellipsis'=>'...', 'exact'=>true)); ?></li>
					</ul>
					<div class="pricing-footer">
						<?php
							echo $this->Html->link('Ver', array('controller' => 'projects', 'action' => 'view', $project['Project']['id'], Format::clean($project['Project']['title']), 'admin' => false), array('class' => 'btn theme-btn'));
						?>
					</div>
				</div>
			</div>
		<?php if ($boxes%4 == 0 || sizeof($projects) == $boxes) : ?>
		</div>
		<?php endif ?>
	<?php endforeach; ?>
<?php else: ?>
<div class="row margin-bottom-40">
	<div class="col-md-12">
		<p>No se encuentran proyectos</p>
	</div>
</div>
<?php endif; ?>