<div class="margin-top-10">
	<?php if (isset($ownAccount) && $ownAccount) : ?>
	<div class="mix-filter">
		<blockquote>
			<p style="font-size:16px">
				<?php
					$query = array(
						'model' => $model,
						'controller' => $controller,
						'foreign_key' => $foreignKey,
						'slug' => $slug,
						'singular' => $humanSingular,
						'plural' => $humanPlural,
						'name' => $dataName
					);
				?>
				 Haz click en <?php echo $this->Html->link('<i class="fa fa-plus"></i> Agregar',
					array(
						'action' => 'add',
						'controller' => 'default_images',
						'?' => http_build_query($query)
					),
					array(
						'class' => 'btn green',
						'escape' => false)
					);
				?> para añadir una imagen
			</p>
		</blockquote>
	</div>
	<?php endif; ?>
	<div class="row mix-grid">
	<?php if (isset($data) && !empty($data)) : ?>
		<?php foreach ($data as $image) : ?>
		<div class="col-md-3 col-sm-4 mix">
			<div class="mix-inner">
				<?php
					echo $this->Html->image('images/'.strtolower($model).'_preview_'.$image['file'], array('class' => 'img-responsive'));
				?>
				<div class="mix-details">
					<?php if (isset($image) && !empty($image['description'])) : ?>
					<h4><?php echo $image['description']; ?></h4>
					<?php endif; ?>
					<?php
						if ($ownAccount) {
							echo $this->Html->link('<i class="fa fa-trash-o"></i>', array('controller' => 'default_images', 'action' => 'delete', $image['id'], 'admin' => true, '?' => http_build_query($query)), array('id' => 'delete-image-btn', 'class' => 'mix-link', 'escape' => false, 'data-id' => $image['id'], 'data-name' => ''));
						}

						echo $this->Html->link('<i class="fa fa-search"></i>', '/img/images/'.strtolower($model).'_'.$image['file'], array('class' => 'mix-preview fancybox-button', 'escape' => false, 'title' => $image['description'], 'data-rel' => 'fancybox-button'));
					?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	<?php endif; ?>
	</div>
</div>