<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover" id="social_data">
		<thead>
			<tr>
				<th>Red Social</th>
				<th>Enlace</th>
				<th class="actions">Acciones</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($configurations as $configuration): ?>
			<?php if($configuration['Configuration']['category']=='3'): ?>
			<tr>
				<td width="40%">
					<a data-original-title="<?php echo $configuration['Configuration']['icon']; ?>" class="social-icon social-icon-color <?php echo $configuration['Configuration']['icon']; ?>"></a>
					<?php echo $configuration['Configuration']['name']; ?>
				</td>
				<td width="40%"><?php echo $this->Text->autoLinkUrls($configuration['Configuration']['value'], array('target' => '_blank')); ?></td>
				<td class="actions" width="20%">
					<div class="btn-group btn-group btn-group-justified">
						<?php
							echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $configuration['Configuration']['category'],  $configuration['Configuration']['id'], Format::clean($configuration['Configuration']['name'])), array('class'=>'btn btn-sm yellow', 'escape'=>false));
						?>
					</div>
				</td>
			</tr>
			<?php endif; ?>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>