<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover" id="contact_data">
		<thead>
			<tr>
				<th>Nombre de la configuración</th>
				<th>Valor</th>
				<th class="actions">Acciones</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($configurations as $configuration): ?>
			<?php if($configuration['Configuration']['category']=='1'): ?>
			<tr>
				<td width="40%">
					<i class="fa fa-<?php echo $configuration['Configuration']['icon']; ?>"></i>
					<?php echo $configuration['Configuration']['name']; ?>
				</td>
				<td width="40%"><?php echo $configuration['Configuration']['value']; ?></td>
				<td class="actions" width="20%">
					<div class="btn-group btn-group btn-group-justified">
						<?php
							echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $configuration['Configuration']['category'],  $configuration['Configuration']['id'], Format::clean($configuration['Configuration']['name'])), array('class'=>'btn btn-sm yellow', 'escape'=>false));
						?>
					</div>
				</td>
			</tr>
			<?php endif; ?>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>