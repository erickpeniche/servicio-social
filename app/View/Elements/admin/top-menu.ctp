<ul class="nav navbar-nav pull-right">
	<!-- BEGIN USER LOGIN DROPDOWN -->
	<li class="dropdown user">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			<?php
				$avatar = !empty($user['User']['image']) ? 'images/avatar_'.$user['User']['image'] : '/assets/img/avatar-sm.png' ;
				echo $this->Html->image($avatar);
			?>
			<span class="username"><?php echo $userFullName;?></span>
			<i class="fa fa-angle-down"></i>
		</a>
		<ul class="dropdown-menu">
			<li>
				<?php
					echo $this->Html->link('<i class="fa fa-user"></i> Mi cuenta</a>',
						array(
							'controller' => 'users',
							'action' => 'view',
							'admin' => true,
							$user['User']['id'],
							Format::clean($user['User']['fullname'])
						),
						array('escape' => false)
					);
				?>
			</li>
			<li class="divider"></li>
			<li>
				<a href="javascript:;" id="trigger_fullscreen">
					<i class="fa fa-arrows"></i> Pantalla Completa
				</a>
			</li>
			<li>
				<?php
					echo $this->Html->link('<i class="fa fa-key"></i> Cerrar sesión</a>',
						array(
							'controller' => 'users',
							'action' => 'logout',
							'admin' => true
						),
						array('escape' => false)
					);
				?>
			</li>
		</ul>
	</li>
	<!-- END USER LOGIN DROPDOWN -->
</ul>