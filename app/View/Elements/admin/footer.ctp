<div class="footer">
	<div class="footer-inner">
		<?php echo date('Y'); ?> &copy; <?php echo $config['App']['configurations']['title']; ?>
	</div>
	<div class="footer-tools">
		<span class="go-top">
			<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>