<div class="margin-top-10">
	<?php if (isset($ownAccount) && $ownAccount) : ?>
	<div class="mix-filter">
		<blockquote>
			<p style="font-size:16px">
				<?php
					$query = array(
						'model' => $model,
						'controller' => $controller,
						'foreign_key' => $foreignKey,
						'slug' => $slug,
						'singular' => $humanSingular,
						'plural' => $humanPlural,
						'name' => $dataName
					);
				?>
				 Haz click en <?php echo $this->Html->link('<i class="fa fa-plus"></i> Agregar',
					array(
						'action' => 'add',
						'controller' => 'default_archives',
						'?' => http_build_query($query)
					),
					array(
						'class' => 'btn green',
						'escape' => false)
					);
				?> para añadir un documento
			</p>
		</blockquote>
	</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-md-12">
			<div class="portlet">
				<div class="portlet-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-advance table-hover">
							<thead>
								<tr>
									<th>
										Nombre
									</th>
									<th class="hidden-xs">
										Tamaño
									</th>
									<th>
										Fecha de subida
									</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							<?php if (isset($data) && !empty($data)) : ?>
								<?php foreach ($data as $archive) : ?>
								<tr>
									<td>
										<?php
											echo $this->Html->link($archive['name'], array('controller' => 'default_archives', 'action' => 'download', $archive['id']));
										?>
									</td>
									<td class="hidden-xs">
										<?php echo $this->Number->toReadableSize($archive['size']); ?>
									</td>
									<td>
										<?php echo $this->Time->format($archive['created'], '%d/%m/%Y - %H:%M'); ?>
									</td>
									<td><i class="fa"></i>
										<?php
											echo $this->Html->link('<i class="fa fa-download"></i> Descargar', array('controller' => 'default_archives', 'action' => 'download', $archive['id'], 'admin' => false, '?' => http_build_query($query)), array('escape' => false, 'class' => 'btn default btn-xs blue-stripe')).'&nbsp;';
											if ($ownAccount) {
												echo $this->Html->link('<i class="fa fa-trash-o"></i> Eliminar', array('controller' => 'default_archives', 'action' => 'delete', $archive['id'], 'admin' => true, '?' => http_build_query($query)), array('id' => 'delete-archive-btn', 'class'=>'btn btn-xs red', 'escape' => false, 'data-id' => $archive['id'], 'data-name' => $archive['name'], 'escape' => false));
											}
										?>
									</td>
								</tr>
								<?php endforeach; ?>
							<?php else: ?>
								<tr>
									<td colspan="4">
										No se encuentran documentos disponibles
									</td>
								</tr>
							<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>