<?php
	$activeMenus = array(
		'home' => array(
			'active' => $activeMenu == 'home' ? 'active' : '',
			'selected' => $activeMenu == 'home' ? '<span class="selected"></span>' : ''
		),
		'main_images' => array(
			'active' => $activeMenu == 'main_images' ? 'active' : '',
			'selected' => $activeMenu == 'main_images' ? '<span class="selected"></span>' : ''
		),
		'posts' => array(
			'active' => $activeMenu == 'posts' ? 'active' : '',
			'selected' => $activeMenu == 'posts' ? '<span class="selected"></span>' : ''
		),
		'projects' => array(
			'active' => $activeMenu == 'projects' ? 'active' : '',
			'selected' => $activeMenu == 'projects' ? '<span class="selected"></span>' : ''
		),
		'publications' => array(
			'active' => $activeMenu == 'publications' ? 'active' : '',
			'selected' => $activeMenu == 'publications' ? '<span class="selected"></span>' : ''
		),
		'profiles' => array(
			'active' => $activeMenu == 'profiles' ? 'active' : '',
			'selected' => $activeMenu == 'profiles' ? '<span class="selected"></span>' : ''
		),
		'users' => array(
			'active' => $activeMenu == 'users' ? 'active' : '',
			'selected' => $activeMenu == 'users' ? '<span class="selected"></span>' : ''
		),
		'configurations' => array(
			'active' => $activeMenu == 'configurations' ? 'active' : '',
			'selected' => $activeMenu == 'configurations' ? '<span class="selected"></span>' : ''
		)
	);
?>
<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<ul class="page-sidebar-menu">
			<li class="sidebar-toggler-wrapper">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler hidden-phone">
				</div>
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
			</li>
			<li>&nbsp;</li>
			<li class="start <?php echo $activeMenus['home']['active']; ?>">
				<?php
					echo $this->Html->link(
						'<i class="fa fa-home"></i><span class="title">Inicio</span>'.$activeMenus['home']['selected'],
						array(
							'controller' => 'pages',
							'action' => 'home',
							'admin' => true
						),
						array('escape' => false)
					);
				?>
			</li>
			<li class="<?php echo $activeMenus['main_images']['active']; ?>">
				<?php
					echo $this->Html->link(
						'<i class="fa fa-picture-o"></i><span class="title">Imágenes de portada</span>'.$activeMenus['main_images']['selected'],
						array(
							'controller' => 'main_images',
							'action' => 'index',
							'admin' => true
						),
						array('escape' => false)
					);
				?>
			</li>
			<li class="<?php echo $activeMenus['posts']['active']; ?>">
				<?php
					echo $this->Html->link(
						'<i class="fa fa-comments"></i><span class="title">Blog</span>'.$activeMenus['posts']['selected'],
						array(
							'controller' => 'posts',
							'action' => 'index',
							'admin' => true
						),
						array('escape' => false)
					);
				?>
			</li>
			<li class="<?php echo $activeMenus['projects']['active']; ?>">
				<?php
					echo $this->Html->link(
						'<i class="fa fa-flask"></i><span class="title">Proyectos</span>'.$activeMenus['projects']['selected'],
						array(
							'controller' => 'projects',
							'action' => 'index',
							'admin' => true
						),
						array('escape' => false)
					);
				?>
			</li>
			<li class="<?php echo $activeMenus['publications']['active']; ?>">
				<?php
					echo $this->Html->link(
						'<i class="fa fa-download"></i><span class="title">Publicaciones</span>'.$activeMenus['publications']['selected'],
						array(
							'controller' => 'publications',
							'action' => 'index',
							'admin' => true
						),
						array('escape' => false)
					);
				?>
			</li>
			<?php if(AppController::isSuperUser()): ?>
			<li class="<?php echo $activeMenus['profiles']['active']; ?>">
				<?php
					echo $this->Html->link(
						'<i class="fa fa-suitcase"></i><span class="title">Perfiles</span>'.$activeMenus['profiles']['selected'],
						array(
							'controller' => 'profiles',
							'action' => 'index',
							'admin' => true
						),
						array('escape' => false)
					);
				?>
			</li>
			<li class="<?php echo $activeMenus['users']['active']; ?>">
				<?php
					echo $this->Html->link(
						'<i class="fa fa-group"></i><span class="title">Usuarios</span>'.$activeMenus['users']['selected'],
						array(
							'controller' => 'users',
							'action' => 'index',
							'admin' => true
						),
						array('escape' => false)
					);
				?>
			</li>
			<li class="last <?php echo $activeMenus['configurations']['active']; ?>">
				<?php
					echo $this->Html->link(
						'<i class="fa fa-cogs"></i><span class="title">Configuraciones</span>'.$activeMenus['configurations']['selected'],
						array(
							'controller' => 'configurations',
							'action' => 'index',
							'admin' => true
						),
						array('escape' => false)
					);
				?>
			</li>
			<?php endif; ?>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>