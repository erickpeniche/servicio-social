<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	<div class="dashboard-stat <?php echo $color;?>">
		<div class="visual">
			<i class="fa fa-<?php echo $icon;?>"></i>
		</div>
		<div class="details">
			<?php
				echo $number == '' ? '&nbsp;' : $this->Html->tag('div', $number, array('class'=>'number'));
				echo $this->Html->tag('div', $title, array('class'=>'desc'));
			?>
		</div>
		<?php
			echo $this->Html->link(
				'Ver <i class="m-icon-swapright m-icon-white"></i>',
				array(
					'action' => 'index',
					'controller' => $ctrlr,
					'admin' => true
				),
				array(
					'class' => 'more',
					'escape' => false
				)
			);
		?>
	</div>
</div>