<?php
	/*
	*	Toastr types:
	*	toastr.error (red)
	*	toastr.info (blue)
	*	toastr.success (green)
	*	toastr.warning (yellow/orange)
	*/
	echo $this->Html->scriptBlock(
		'toastr.'.$type.'('."'".$message."'".', '."'".$title."'".');',
		array('inline' => false)
	);
?>