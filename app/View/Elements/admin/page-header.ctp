<div class="row">
	<div class="col-md-12">
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			Panel de Administración&nbsp;
			<small><?php echo isset($config['App']['configurations']['title']) ? $config['App']['configurations']['title'] : ''; ?></small>
		</h3>
		<ul class="page-breadcrumb breadcrumb">
			<?php $crumbs = $this->Html->getCrumbs2(
				array(
					'text' => 'Inicio',
					'url' => array('controller' => 'pages', 'action' => 'home', 'admin' => true)
					)
				);
			?>
			<?php foreach ($crumbs as $key => $crumb):?>
			<li>
				<?php
					echo $key==0 ? '<i class="fa fa-home"></i>' : '';
					echo $crumb;
					echo $key < count($crumbs)-1 ? '<i class="fa fa-angle-right"></i>' : '';
				?>
			</li>
			<?php endforeach; ?>
		</ul>
		<!-- END PAGE TITLE & BREADCRUMB-->
	</div>
</div>