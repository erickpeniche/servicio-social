<?php
	echo $this->Html->css(array(
		'plugins/select2/select2',
		'plugins/select2/select2-metronic',
		'plugins/data-tables/DT_bootstrap'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/select2/select2.min',
		'plugins/data-tables/jquery.dataTables',
		'plugins/data-tables/DT_bootstrap',
		'/js/projects/admin_index'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Proyectos',
		array(
			'action' => 'index',
			'controller' => 'projects',
			'admin' => true
			)
		);
?>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-flask"></i>Proyectos</div>
				<div class="actions">
					<?php echo $this->Html->link('<i class="fa fa-plus"></i> Agregar</a>',
						array(
							'action' => 'add',
							'controller' => 'projects'
						),
						array(
							'class' => 'btn default',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="projects_table">
						<thead>
							<tr>
								<th>Proyecto</th>
								<th>Encargado de Proyecto</th>
								<th>Tema</th>
								<th>Creado por</th>
								<th>Última modificación</th>
								<th class="actions">Acciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($projects as $project): ?>
							<tr>
								<td><?php echo $project['Project']['title']; ?></td>
								<td><?php echo isset($project['Project']) && !empty($project['Project']['responsable']) ? $project['Project']['responsable'] : '-'; ?></td>
								<td><?php echo isset($project['Project']) && !empty($project['Project']['topic']) ? $project['Project']['topic'] : '-'; ?></td>
								<td><?php echo $project['User']['fullname']; ?></td>
								<td><?php echo $this->Time->format($project['Project']['modified'], '%d/%m/%Y - %H:%M'); ?></td>
								<td class="actions">
									<div class="btn-group btn-group btn-group-justified">
										<?php
											echo $this->Html->link('<i class="fa fa-info-circle"></i> Ver', array('action' => 'view', $project['Project']['id'], Format::clean($project['Project']['title'])), array('class'=>'btn btn-sm btn-info', 'escape'=>false));
											if ($project['Project']['user_id'] == $loggedUser['id']) :
												echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $project['Project']['id'], Format::clean($project['User']['fullname'])), array('class'=>'btn btn-sm yellow', 'escape'=>false));
												echo $this->Html->link('<i class="fa fa-trash-o"></i> Eliminar', array('action' => 'delete', $project['Project']['id']), array('id' => 'delete-btn', 'class'=>'btn btn-sm red', 'escape' => false, 'data-id' => $project['Project']['id'], 'data-name' => $project['Project']['title']));

												$option = array('icon' => $project['Project']['main'] == 0 ? 'star-o':'star', 'title' => $project['Project']['main'] == 0 ? 'Destacar':'Quitar destacado');
												echo $this->Html->link('<i class="fa fa-'.$option['icon'].'"></i>', array('action' => 'toggle_main', $project['Project']['id']), array('class'=>'btn btn-sm default', 'escape' => false, 'title' => $option['title']));
												$option = array('icon' => $project['Project']['public'] == 0 ? 'eye-slash':'eye', 'title' => $project['Project']['public'] == 0 ? 'Marcar como público':'Marcar como privado');
												echo $this->Html->link('<i class="fa fa-'.$option['icon'].'"></i>', array('action' => 'toggle_public', $project['Project']['id']), array('class'=>'btn btn-sm default', 'escape' => false, 'title' => $option['title']));
											endif;
										?>
									</div>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'AdminIndex.init();',
		array('inline' => false)
	);
?>