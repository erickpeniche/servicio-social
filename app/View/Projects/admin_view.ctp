<?php
	echo $this->Html->css(array(
		'plugins/fancybox/source/jquery.fancybox',
		'css/pages/profile',
		'css/pages/portfolio'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/jquery-mixitup/jquery.mixitup.min',
		'plugins/fancybox/source/jquery.fancybox.pack',
		'/js/projects/admin_view'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Proyectos',
		array(
			'action' => 'index',
			'controller' => 'projects',
			'admin' => true
			)
		);

	$this->Html->addCrumb($project['Project']['title'],
		array(
			'action' => 'view',
			'controller' => 'projects',
			'admin' => true,
			$project['Project']['id'],
			Format::clean($project['Project']['title'])
			)
		);
?>

<div class="row profile">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">&nbsp;</div>
				<div class="actions">
					<?php
						echo $ownAccount ? $this->Html->link('<i class="fa fa-pencil"></i> Editar', array('controller'=>'projects', 'action' => 'edit', 'admin'=>true, $project['Project']['id'], Format::clean($project['Project']['title'])), array('class'=>'btn default yellow-stripe', 'escape'=>false)).'&nbsp;' : '';
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Proyectos', array('controller'=>'projects', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon green-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12">
						<?php if (isset($project['Project']) && $project['Project']['working'] == 1 && $ownAccount) : ?>
						<div class="alert alert-info alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
							<i class="fa fa-info-circle"></i> <strong>Actualmente te encuentras trabajando o involucrado en este proyecto</strong>
						</div>
						<?php endif; ?>
						<div class="tabbable tabbable-custom tabbable-full-width">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#general" data-toggle="tab">
										 General
									</a>
								</li>
								<li>
									<a href="#images" data-toggle="tab">
										 Imágenes
									</a>
								</li>
								<li>
									<a href="#documents" data-toggle="tab">
										 Documentos
									</a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="general">
									<div class="row">
										<div class="col-md-8">
											<form class="form-horizontal" role="form">
												<h2 class="margin-bottom-20"> <?php echo $project['Project']['title']; ?></h2>
												<h3 class="form-section">Datos del proyecto</h3>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-4">Tema:</label>
															<div class="col-md-8">
																<p class="form-control-static">
																	<?php echo !empty($project['Project']['topic']) ? $project['Project']['topic'] : '-'; ?>
																</p>
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-4">Fecha de inicio:</label>
															<div class="col-md-8">
																<p class="form-control-static">
																	<?php echo !empty($project['Project']['start_date']) ? $this->Time->format($project['Project']['start_date'], '%d/%m/%Y') : '-'; ?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-4">Responsable de proyecto:</label>
															<div class="col-md-8">
																<p class="form-control-static">
																	<?php echo !empty($project['Project']['responsable']) ? $project['Project']['responsable'] : '-'; ?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-4">Fecha de finalización:</label>
															<div class="col-md-8">
																<p class="form-control-static">
																	<?php echo !empty($project['Project']['end_date']) ? $this->Time->format($project['Project']['end_date'], '%d/%m/%Y') : '-'; ?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-4">Lugar de realización:</label>
															<div class="col-md-8">
																<p class="form-control-static">
																	<?php echo !empty($project['Project']['place']) ? $project['Project']['place'] : '-'; ?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-4">Visibilidad:</label>
															<div class="col-md-8">
																<p class="form-control-static">
																	<?php echo $project['Project']['public'] == 1 ? '<span class="label label-info">Público</span>' : '<span class="label label-default">Privado</span>'; ?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<?php if (isset($project['Project']) && $project['Project']['main'] == 1) : ?>
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-4"><i class="fa fa-star"></i></label>
															<div class="col-md-8">
																<p class="form-control-static">
																	<strong>Proyecto destacado</strong>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<?php endif; ?>

												<h3 class="form-section">Descripción</h3>
												<div class="row">
													<div class="col-md-12">
														<?php echo $project['Project']['description']; ?>
													</div>
												</div>
											</form>
										</div>
										<div class="col-md-4">
											<?php if (isset($project['Project']) && !empty($project['Project']['coworkers'])) : ?>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h3 class="panel-title"><i class="fa fa-group"></i> Involucrados / Participantes</h3>
												</div>
												<div class="panel-body">
													<?php echo $project['Project']['coworkers']; ?>
												</div>
											</div>
											<?php endif; ?>
										</div>
									</div>
								</div>
								<!--tab_1_2-->
								<div class="tab-pane" id="images">
									<?php
										echo $this->element(
											'admin/default_image_gallery',
											array(
												'model' => 'Project',
												'controller' => 'projects',
												'foreignKey' => $project['Project']['id'],
												'data' =>$project['DefaultImage'],
												'ownAccount' => $ownAccount,
												'slug' => Format::clean($project['Project']['title']),
												'humanSingular' => 'proyecto',
												'humanPlural' => 'proyectos',
												'dataName' => $project['Project']['title']
											)
										);
									?>
								</div>
								<!--end tab-pane-->
								<div class="tab-pane" id="documents">
									<?php
										echo $this->element(
											'admin/default_archive_gallery',
											array(
												'model' => 'Project',
												'controller' => 'projects',
												'foreignKey' => $project['Project']['id'],
												'data' =>$project['DefaultArchive'],
												'ownAccount' => $ownAccount,
												'slug' => Format::clean($project['Project']['title']),
												'humanSingular' => 'proyecto',
												'humanPlural' => 'proyectos',
												'dataName' => $project['Project']['title']
											)
										);
									?>
								</div>
								<!--end tab-pane-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
	echo $this->Html->scriptBlock(
		'AdminView.init();',
		array('inline' => false)
	);
?>