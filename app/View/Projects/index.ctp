<?php
echo $this->Html->css(array(
	'portal/plugins/fancybox/source/jquery.fancybox',
	'portal/css/pages/portfolio'
	), null, array('inline' => false)
);

echo $this->Html->script(array(
	'portal/plugins/fancybox/source/jquery.fancybox.pack',
	'portal/plugins/jquery.mixitup.min',
	'/js/projects/index'
	), array('inline' => false)
);
?>
<!-- BEGIN BREADCRUMBS -->
<div class="row breadcrumbs margin-bottom-40">
	<div class="container">
		<div class="col-md-4 col-sm-4">
			<h1>Proyectos</h1>
		</div>
		<div class="col-md-8 col-sm-8">
			<ul class="pull-right breadcrumb">
				<li>
					<?php
					echo $this->Html->link('Inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => false));
					?>
				</li>
				<li class="active">Proyectos</li>
			</ul>
		</div>
	</div>
</div>
<!-- END BREADCRUMBS -->

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN CONTAINER -->
		<div class="container min-hight portfolio-page margin-bottom-40">
			<!-- BEGIN FILTER -->
			<div class="filter-v1">
				<ul class="mix-filter">
					<li class="filter active" data-filter="all">Todos</li>
					<?php foreach ($users as $user) : ?>
					<li class="filter" data-filter="user_<?php echo $user['User']['id']; ?>"><?php echo $user['User']['title'].' '.$user['User']['fullname'] ?></li>
					<?php endforeach; ?>
				</ul>
				<div class="row mix-grid thumbnails">
				<?php if (isset($projects) && !empty($projects)) : ?>
					<?php foreach ($projects as $project) : ?>
					<div style="display: inline-block;  opacity: 1;" class="col-md-3 col-sm-4 mix user_<?php echo $project['Project']['user_id']; ?> mix_all">
						<div class="mix-inner">
							<?php
								$image = isset($project['DefaultImage']) && !empty($project['DefaultImage']) ? '/img/images/project_preview_'.$project['DefaultImage'][0]['file'] : '/assets/portal/img/preview-holdit.png';
								echo $this->Html->image($image, array('class' => 'img-responsive'));
							?>
							<div class="mix-details">
								<?php if (isset($project['Project']) && !empty($project['Project']['title'])) : ?>
								<h4><?php echo $project['Project']['title']; ?></h4>
								<?php endif; ?>
								<?php
									echo $this->Html->link('<i class="fa fa-link"></i>', array('controller' => 'projects', 'action' => 'view', $project['Project']['id'], Format::clean($project['Project']['title']), 'admin' => false), array('class' => 'mix-link', 'escape' => false));

									$fullImage = isset($project['DefaultImage']) && !empty($project['DefaultImage']) ? '/img/images/project_preview_'.$project['DefaultImage'][0]['file'] : '/assets/portal/img/preview-holdit.png';
									echo $this->Html->link('<i class="fa fa-search"></i>', $fullImage, array('class' => 'mix-preview fancybox-button', 'escape' => false, 'title' => $project['Project']['title'], 'data-rel' => 'fancybox-button'));
								?>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				<?php else: ?>
				<?php endif; ?>
				</div>
			</div>
			<!-- END FILTER -->
		</div>
		<!-- END CONTAINER -->
	</div>
</div>

<?php
	echo $this->Html->scriptBlock(
		'Index.init();',
		array('inline' => false)
	);
?>