<?php
	echo $this->Html->css(array(
		'plugins/bootstrap-datepicker/css/datepicker',
		'plugins/bootstrap-switch/css/bootstrap-switch.min',
		'plugins/select2/select2',
		'plugins/select2/select2-metronic'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
		'plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.es',
		'plugins/ckeditor/ckeditor',
		'plugins/bootstrap-switch/js/bootstrap-switch.min',
		'plugins/jquery-validation/dist/jquery.validate.min',
		'plugins/jquery-validation/localization/messages_es',
		'plugins/jquery-validation/dist/additional-methods.min',
		'plugins/select2/select2.min',
		'/js/projects/admin_edit'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Proyectos',
		array(
			'action' => 'index',
			'controller' => 'projects',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Editar',
		array(
			'action' => 'edit',
			'controller' => 'projects',
			'admin' => true,
			$project['Project']['id'],
			Format::clean($project['Project']['title'])
			)
		);

	$this->Html->addCrumb($project['Project']['title'], null);
?>

<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-pencil"></i>Editar Proyecto</div>
				<div class="actions">
					<?php
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Proyectos', array('controller'=>'projects', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon green-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<?php
				echo $this->Form->create('Project',
					array(
						'inputDefaults' => array(
							'div' => false,
							'label' => false,
							'class' => 'form-control',
							'error' => false
						),
						'class' => 'form-horizontal form-row-seperated',
						'novalidate' => true
					)
				);

				echo $this->Form->hidden('id');
			?>
				<div class="form-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						Existen errores en el formulario. Por favor verifica tus datos.
					</div>
					<!-- Title -->
					<div class="form-group <?php echo $this->Form->isFieldError('title') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Título<span class="required">*</span></label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-bookmark"></i></span>
								<?php echo $this->Form->input('title'); ?>
							</div>
							<?php echo $this->Form->isFieldError('title') ? $this->Form->error('title', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Title -->
					<!-- Topic -->
					<div class="form-group">
						<label class="control-label col-md-3">Tema</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-book"></i></span>
								<?php echo $this->Form->input('topic'); ?>
							</div>
						</div>
					</div>
					<!-- END Topic -->
					<!-- Responsable -->
					<div class="form-group">
						<label class="control-label col-md-3">Responsable del proyecto</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<?php echo $this->Form->input('responsable'); ?>
							</div>
						</div>
					</div>
					<!-- END Responsable -->
					<!-- Place -->
					<div class="form-group">
						<label class="control-label col-md-3">Ubicación o lugar del proyecto</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-globe"></i></span>
								<?php echo $this->Form->input('place'); ?>
							</div>
						</div>
					</div>
					<!-- END Place -->
					<!-- Start date -->
					<div class="form-group <?php echo $this->Form->isFieldError('start_date') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Fecha de comienzo</label>
						<div class="col-md-9">
							<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
								<?php echo $this->Form->input('start_date', array('type'=>'text', 'readonly'=>true)); ?>
								<span class="input-group-btn">
									<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
							</div>
							<?php echo $this->Form->isFieldError('start_date') ? $this->Form->error('start_date', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
							<!-- /input-group -->
						</div>
					</div>
					<!-- END Start date -->
					<!-- End date -->
					<div class="form-group <?php echo $this->Form->isFieldError('end_date') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Fecha de finalización</label>
						<div class="col-md-9">
							<div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd">
								<?php echo $this->Form->input('end_date', array('type'=>'text', 'readonly'=>true)); ?>
								<span class="input-group-btn">
									<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
							</div>
							<?php echo $this->Form->isFieldError('end_date') ? $this->Form->error('end_date', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
							<!-- /input-group -->
						</div>
					</div>
					<!-- END End date -->
					<!-- Description -->
					<div class="form-group <?php echo $this->Form->isFieldError('description') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Descripción<span class="required">*</span></label>
						<div class="col-md-9">
							<?php echo $this->Form->textarea('description', array('class' => 'ckeditor form-control', 'rows' => 6)); ?>
							<?php echo $this->Form->isFieldError('description') ? $this->Form->error('description', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Description -->
					<!-- Coworkers -->
					<div class="form-group">
						<label class="control-label col-md-3">Participantes/Involucrados</label>
						<div class="col-md-9">
							<?php echo $this->Form->textarea('coworkers', array('class' => 'ckeditor form-control', 'rows' => 6)); ?>
						</div>
					</div>
					<!-- END Coworkers -->
					<!-- Working status -->
					<div class="form-group">
						<label class="control-label col-md-3">¿Trabajas actualmente en este proyecto?</label>
						<div class="col-md-9">
							<?php
								echo $this->Form->checkbox('working', array('type' => 'checkbox', 'class' => 'make-switch', 'data-on-label' => 'Sí', 'data-off-label' => 'No', 'data-on' => 'success', 'data-off' => 'danger'));
							?>
						</div>
					</div>
					<!-- END Working status -->
					<!-- Public status -->
					<div class="form-group">
						<label class="control-label col-md-3">Visible al público</label>
						<div class="col-md-9">
							<?php
								echo $this->Form->checkbox('public', array('type' => 'checkbox', 'class' => 'make-switch', 'data-on-label' => 'Sí', 'data-off-label' => 'No', 'data-on' => 'success', 'data-off' => 'danger'));
							?>
							<span class="help-block unique">Indica si el proyecto puede ser visto publicamente</span>
						</div>
					</div>
					<!-- END Public status -->
					<!-- Main status -->
					<div class="form-group">
						<label class="control-label col-md-3">Proyecto destacado</label>
						<div class="col-md-9">
							<?php
								echo $this->Form->checkbox('main', array('type' => 'checkbox', 'class' => 'make-switch', 'data-on-label' => 'Sí', 'data-off-label' => 'No', 'data-on' => 'success', 'data-off' => 'danger'));
							?>
							<span class="help-block unique">Indica si el proyecto es destacado, aparecerá en la portada del sitio</span>
						</div>
					</div>
					<!-- END Main status -->
				</div>
				<div class="form-actions fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-offset-3 col-md-9">
								<?php
									echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
										array('type' => 'submit', 'class' => 'btn green')
										).'&nbsp;';

									$cancelOptions = array('action' => 'index', 'controller' => 'projects', 'admin' => true);
									echo $this->Form->button('Cancelar',
										array(
											'type' => 'button',
											'class' => 'btn red',
											'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
											)
										);
								?>
							</div>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'FormValidation.init();',
		array('inline' => false)
	);
?>