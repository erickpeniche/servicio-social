<?php
echo $this->Html->css(array(
	'portal/plugins/fancybox/source/jquery.fancybox',
	'portal/css/pages/portfolio',
	'portal/plugins/bxslider/jquery.bxslider',
	), null, array('inline' => false)
);

echo $this->Html->script(array(
	'portal/plugins/fancybox/source/jquery.fancybox.pack',
	'plugins/bxslider/jquery.bxslider.min',
	//'/js/projects/index'
	), array('inline' => false)
);
?>

<!-- BEGIN BREADCRUMBS -->
<div class="row breadcrumbs margin-bottom-40">
	<div class="container">
		<div class="col-md-4 col-sm-4">
			<h1>Proyecto</h1>
		</div>
		<div class="col-md-8 col-sm-8">
			<ul class="pull-right breadcrumb">
				<li>
					<?php
						echo $this->Html->link('Inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => false));
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link('Proyectos', array('controller' => 'projects', 'action' => 'index', 'admin' => false));
					?>
				</li>
				<li class="active"><?php echo $project['Project']['title']; ?></li>
			</ul>
		</div>
	</div>
</div>
<!-- END BREADCRUMBS -->

<!-- BEGIN CONTAINER -->
<div class="container min-hight portfolio-page margin-bottom-30">
	<!-- BEGIN PORTFOLIO ITEM -->
	<div class="row">
		<!-- BEGIN CAROUSEL -->
		<?php if (isset($project['DefaultImage']) && !empty($project['DefaultImage'])) : ?>
		<div class="col-md-5 front-carousel">
			<div id="myCarousel" class="carousel slide">
				<!-- Carousel items -->
				<div class="carousel-inner">
					<?php foreach ($project['DefaultImage'] as $key => $image) : ?>
					<div class="<?php echo $key == 0 ? 'active' : ''; ?> item">
						<?php echo $this->Html->image('images/project_preview_' . $image['file']); ?>
						<div class="carousel-caption">
							<?php if (isset($image) && !empty($image['description'])) : ?>
							<p><?php echo $image['description']; ?></p>
							<?php endif; ?>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
				<!-- Carousel nav -->
				<a class="carousel-control left" href="#myCarousel" data-slide="prev">
					<i class="fa fa-angle-left"></i>
				</a>
				<a class="carousel-control right" href="#myCarousel" data-slide="next">
					<i class="fa fa-angle-right"></i>
				</a>
			</div>
		</div>
		<?php endif; ?>
		<!-- END CAROUSEL -->

		<!-- BEGIN PORTFOLIO DESCRIPTION -->
		<div class="col-md-<?php echo isset($project['DefaultImage']) && !empty($project['DefaultImage']) ? 7 : 12; ?>">
			<h2><?php echo $project['Project']['title']; ?></h2><small>por <?php echo $project['User']['title'].' '.$project['User']['fullname']; ?></small><hr>
			<div class="row front-lists-v2 margin-bottom-15">
				<div class="col-md-12">
					<ul class="list-unstyled">
						<li><strong>Tema:</strong> <?php echo !empty($project['Project']['topic']) ? $project['Project']['topic'] : '-'; ?></li>
						<li><strong>Responsable del proyecto:</strong> <?php echo !empty($project['Project']['responsable']) ? $project['Project']['responsable'] : '-'; ?></li>
						<li><strong>Lugar de realización:</strong> <?php echo !empty($project['Project']['place']) ? $project['Project']['place'] : '-'; ?></li>
						<li><strong>Fecha de inicio:</strong> <?php echo !empty($project['Project']['start_date']) ? $this->Time->format($project['Project']['start_date'], '%d/%m/%Y') : '-'; ?></li>
						<li><strong>Fecha de finalización:</strong> <?php echo !empty($project['Project']['end_date']) ? $this->Time->format($project['Project']['end_date'], '%d/%m/%Y') : '-'; ?></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- END PORTFOLIO DESCRIPTION -->
	</div>
	<!-- END PORTFOLIO ITEM -->

	<!-- BEGIN BLOCKQUOTE BLOCK -->
	<div class="row quote-v1">
		<div class="col-md-12 quote-v1-inner">
			<span>Descripción</span>
		</div>
	</div>
	<!-- END BLOCKQUOTE BLOCK -->

	<div class="clearfix"></div>

	<!-- BEGIN RECENT WORKS -->
	<div class="row">
		<div class="col-md-12">
			<?php echo $project['Project']['description']; ?>
		</div>
	</div>
	<!-- END RECENT WORKS -->

	<!-- BEGIN BLOCKQUOTE BLOCK -->
	<div class="row quote-v1">
		<div class="col-md-12 quote-v1-inner">
			<span>Involucrados / Participantes</span>
		</div>
	</div>
	<!-- END BLOCKQUOTE BLOCK -->

	<div class="clearfix"></div>

	<!-- BEGIN RECENT WORKS -->
	<div class="row">
		<div class="col-md-12">
			<?php echo !empty($project['Project']['coworkers']) ?$project['Project']['coworkers'] : ''; ?>
		</div>
	</div>
	<!-- END RECENT WORKS -->

	<!-- BEGIN BLOCKQUOTE BLOCK -->
	<div class="row quote-v1">
		<div class="col-md-12 quote-v1-inner">
			<span>Documentos y descargas</span>
		</div>
	</div>
	<!-- END BLOCKQUOTE BLOCK -->

	<div class="clearfix"></div>

	<!-- BEGIN RECENT WORKS -->
	<div class="row">
		<div class="col-md-12">
			<?php if (isset($project['DefaultArchive']) && !empty($project['DefaultArchive'])) : ?>
				<ul class="nav sidebar-categories margin-bottom-40">
				<?php foreach ($project['DefaultArchive'] as $archive) : ?>
					<li><?php echo $this->Html->link('<i class="fa fa-download"></i> ' . $archive['name'], array('controller' => 'default_archives', 'action' => 'download', $archive['id']), array('escape' => false)); ?></li>
				<?php endforeach; ?>
				</ul>
			<?php else: ?>
				<p>No se encuentran documentos para este proyecto</p>
			<?php endif; ?>
		</div>
	</div>
	<!-- END RECENT WORKS -->
</div>
<!-- END CONTAINER -->