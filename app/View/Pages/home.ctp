<!-- BEGIN REVOLUTION SLIDER -->
<div class="fullwidthbanner-container slider-main">
	<div class="fullwidthabnner">
		<ul id="revolutionul" style="display:none;">
		<!-- THE NEW SLIDE -->
		<?php if (isset($covers) && !empty($covers)) : ?>
			<?php foreach ($covers as $image) : ?>
			<li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="5000" data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">
				<?php
					echo $this->Html->image('images/main_slider_' . $image['MainImage']['image']);
				?>
				<?php if (isset($image) && !empty($image['MainImage']['description'])) : ?>
				<div class="caption lft slide_title_white slide_item_left"
					 data-x="87"
					 data-y="245"
					 data-speed="400"
					 data-start="2000"
					 data-easing="easeOutExpo">
					 <span class="slide_title_white_bold"><?php echo $image['MainImage']['description']; ?></span>
				</div>
				<?php endif; ?>
				<?php if (isset($image) && !empty($image['MainImage']['link'])) : ?>
					<?php
						echo $this->Html->link('Ver', $image['MainImage']['link'], array('class' => 'caption lft btn dark slide_btn slide_item_left', 'data-x' => 187, 'data-y' => 315, 'data-speed' => 400, 'data-start' => 3000, 'data-easing' => 'easeOutExpo'));
					?>
				<?php endif; ?>
			</li>
			<?php endforeach; ?>
		<?php else: ?>
			<li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="5000" data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">
				<?php
					echo $this->Html->image('/assets/portal/img/slider-hold.png');
				?>
			</li>
		<?php endif; ?>
		</ul>
		<div class="tp-bannertimer tp-bottom"></div>
	</div>
</div>
<!-- END REVOLUTION SLIDER -->

<!-- BEGIN CONTAINER -->
<div class="container">
	<!-- BEGIN RECENT PROJECTS -->
	<div class="row recent-work margin-bottom-40">
		<div class="col-md-3">
			<h2>Proyectos destacados</h2>
			<p>Descripción de proyectos destacados</p>
		</div>
		<div class="col-md-9">
			<?php if (isset($projects) && !empty($projects)) : ?>
			<ul class="bxslider">
				<?php foreach ($projects as $project) : ?>
				<li>
					<em>
						<?php
							$image = isset($project['DefaultImage']) && !empty($project['DefaultImage']) ? 'images/project_preview_' . $project['DefaultImage'][0]['file'] : '/assets/portal/img/preview-holdit.png';
							echo $this->Html->image($image);
							echo $this->Html->link('<i class="fa fa-link icon-hover icon-hover-1"></i>', array('controller' => 'projects', 'action' => 'view', $project['Project']['id'], Format::clean($project['Project']['title'])), array('escape' => false));
							if (isset($project['DefaultImage']) && !empty($project['DefaultImage'])) {
								echo $this->Html->link('<i class="fa fa-search icon-hover icon-hover-2"></i>', '/img/images/project_' . $project['DefaultImage'][0]['file'], array('escape' => false, 'class' => 'fancybox-button', 'title' => $project['Project']['title'], 'data-rel' => 'fancybox-button'));
							}
						?>
					</em>
					<?php
						echo $this->Html->link('<strong>'.$project['Project']['title'].'</strong><b>'.$project['User']['title'].' '.$project['User']['fullname'].'</b>', array('controller' => 'projects', 'action' => 'view', 'admin' => false, $project['Project']['id'], Format::clean($project['Project']['title'])), array('class' => 'bxslider-block', 'escape' => false));
					?>
				</li>
				<?php endforeach; ?>
			</ul>
			<?php else: ?>
			<p><i>No se encuentran proyectos destacados</i></p>
			<?php endif; ?>
	   </div>
	</div>
	<!-- END RECENT PROJECTS -->

	<!-- BEGIN RECENT PROJECTS -->
	<div class="row recent-work margin-bottom-40">
		<div class="col-md-3">
			<h2>Publicaciones destacadas</h2>
			<p>Descripción de proyectos destacados</p>
		</div>
		<div class="col-md-9">
			<div id="accordion1" class="panel-group">
				<?php if (isset($publications) && !empty($publications)) : ?>
				<?php foreach ($publications as $key => $publication) : ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_<?php echo $key; ?>">
								<?php echo $publication['Publication']['title']; ?>
							</a>
						</h4>
					</div>
					<div id="accordion1_<?php echo $key; ?>" class="panel-collapse collapse">
						<?php if (!empty($publication['Publication']['description'])) : ?>
						<div class="panel-body">
							<?php echo $publication['Publication']['description']; ?>
						</div>
						<?php endif; ?>
						<div class="panel-body">
							<i class="fa fa-download"></i>
							<?php echo $this->Html->link($publication['Publication']['publication_name'], array('controller' => 'publications', 'action' => 'download', $publication['Publication']['id'], 'admin' => false)); ?>&nbsp;
							(<?php echo $this->Number->toReadableSize($publication['Publication']['publication_size']); ?>)
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				<?php else: ?>
				<p><i>No se encuentran publicaciones destacadas</i></p>
				<?php endif; ?>
			</div>
	   </div>
	</div>
	<!-- END RECENT PROJECTS -->

	<div class="clearfix"></div>

</div>
<!-- END CONTAINER -->
