<!-- BEGIN DASHBOARD STATS -->
<div class="row">
	<?php
		echo $this->element('admin/dashboard-stat',
			array(
				'color' => 'blue',
				'icon' => 'picture-o',
				'number' => $totalImages,
				'title' => 'Portadas',
				'ctrlr' => 'main_images'
				)
			);

		echo $this->element('admin/dashboard-stat',
			array(
				'color' => 'red',
				'icon' => 'comments',
				'number' => $totalPosts,
				'title' => 'Blog',
				'ctrlr' => 'posts'
				)
			);
		echo $this->element('admin/dashboard-stat',
			array(
				'color' => 'green',
				'icon' => 'flask',
				'number' => $totalProjects,
				'title' => 'Proyectos',
				'ctrlr' => 'projects'
				)
			);
		echo $this->element('admin/dashboard-stat',
			array(
				'color' => 'yellow',
				'icon' => 'download',
				'number' => '0',//$totalPublications,
				'title' => 'Publicaciones',
				'ctrlr' => 'publications'
				)
			);
	?>
</div>
<div class="clearfix"></div>
<div class="row">
	<?php
		echo $this->element('admin/dashboard-stat',
			array(
				'color' => 'purple',
				'icon' => 'suitcase',
				'number' => $totalProfiles,
				'title' => 'Perfiles',
				'ctrlr' => 'profiles'
				)
			);
		if(AppController::isSuperUser()){
			echo $this->element('admin/dashboard-stat',
				array(
					'color' => 'blue',
					'icon' => 'group',
					'number' => $totalUsers,
					'title' => 'Usuarios',
					'ctrlr' => 'users'
					)
				);
			echo $this->element('admin/dashboard-stat',
				array(
					'color' => 'gray',
					'icon' => 'cogs',
					'number' => false,
					'title' => 'Configuraciones',
					'ctrlr' => 'configurations'
					)
				);
		}
	?>
</div>
<!-- END DASHBOARD STATS -->