<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php echo $this->Html->charset(); ?>
	<title>Panel Administrador - <?php echo isset($config['App']['configurations']['title']) ? $config['App']['configurations']['title'] : ''; ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php echo $this->Html->meta('viewport', 'width=device-width, initial-scale=1.0') ?>
	<meta name="MobileOptimized" content="320">
	<?php echo $this->Html->meta('author', 'Erick Peniche Castro') ?>
	<?php echo $this->Html->meta('keywords', '') ?>
	<?php echo $this->Html->meta('description', '') ?>
	<?php echo $this->fetch('meta'); ?>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<?php
		echo $this->Html->css(
			array(
				'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
				'plugins/font-awesome/css/font-awesome.min',
				'plugins/bootstrap/css/bootstrap.min',
				'plugins/uniform/css/uniform.default'
			)
		);
	?>
	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN PAGE LEVEL STYLES -->
	<?php
		echo $this->Html->css(array(
			'plugins/bootstrap-toastr/toastr.min',
			)
		);
	?>
	<!-- END PAGE LEVEL STYLES -->

	<!-- BEGIN THEME STYLES -->
	<?php
		echo $this->Html->css(
			array(
				'css/style-metronic',
				'css/style',
				'css/style-responsive',
				'css/plugins'
			)
		);
		echo $this->Html->css(array('css/themes/default'), array('id'=>'style_color'));
		echo $this->Html->css(
			array(
				'css/pages/login-soft',
				'css/custom'
			)
		);
	?>
	<!-- END THEME STYLES -->

	<!-- VIEWS STYLES -->
	<?php echo $this->fetch('css'); ?>
	<!-- END VIEWS STYLES -->

	<?php echo $this->Html->meta('favicon.ico', '/favicon.ico', array('type' => 'icon', 'rel' => 'shortcut icon')); ?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	<?php echo $this->Html->image('/assets/img/logo-mayab.png', array('alt'=>isset($config['App']['configurations']['title']) ? $config['App']['configurations']['title'] : '')); ?>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
	<?php echo $this->Session->flash(); ?>
	<?php echo $this->Session->flash('auth', array('element' => 'admin/flash/toastr', 'params' => array('title'=>'Acceso denegado!', 'type'=>'warning'))); ?>
	<?php echo $this->fetch('content'); ?>
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 <?php echo date('Y') ?> &copy; <?php echo isset($config['App']['configurations']['title']) ? $config['App']['configurations']['title'] : ''; ?>
</div>
<!-- END COPYRIGHT -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<?php
	echo $this->Html->script(
		array(
			'plugins/respond.min',
			'plugins/excanvas.min',
		)
	);
?>
<![endif]-->
<?php
	echo $this->Html->script(
		array(
			'plugins/jquery-1.10.2.min',
			'plugins/jquery-migrate-1.2.1.min',
			'plugins/bootstrap/js/bootstrap.min',
			//'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min',
			'plugins/jquery-slimscroll/jquery.slimscroll.min',
			'plugins/jquery.blockui.min',
			'plugins/jquery.cokie.min',
			'plugins/uniform/jquery.uniform.min'
		)
	);
?>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<?php
	echo $this->Html->script(
		array(
			'plugins/jquery-validation/dist/jquery.validate.min',
			'plugins/jquery-validation/localization/messages_es',
			'plugins/backstretch/jquery.backstretch.min',
			'plugins/bootstrap-toastr/toastr.min'
		)
	);
?>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<?php
	echo $this->Html->script(
		array(
			'scripts/core/app',
			'scripts/custom/login-soft'
		)
	);
	echo $this->fetch('script');
	echo $this->Js->writeBuffer();
?>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
	jQuery(document).ready(function() {
		App.init();
		Login.init();
	});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>