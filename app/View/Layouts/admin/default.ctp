<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php echo $this->Html->charset(); ?>
	<title>Panel Administrador - <?php echo isset($config['App']['configurations']['title']) ? $config['App']['configurations']['title'] : ''; ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php echo $this->Html->meta('viewport', 'width=device-width, initial-scale=1.0') ?>
	<?php echo $this->Html->meta('author', 'Erick Peniche Castro') ?>
	<?php echo $this->Html->meta('keywords', '') ?>
	<?php echo $this->Html->meta('description', '') ?>
	<?php echo $this->fetch('meta'); ?>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<?php
		echo $this->Html->css(
			array(
				'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
				'plugins/font-awesome/css/font-awesome.min',
				'plugins/bootstrap/css/bootstrap.min',
				'plugins/uniform/css/uniform.default'
			)
		);
	?>
	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<?php
		echo $this->Html->css(array(
			'plugins/bootstrap-toastr/toastr.min',
			)
		);
	?>
	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->


	<!-- BEGIN THEME STYLES -->
	<?php
		echo $this->Html->css(
			array(
				'css/style-metronic',
				'css/style',
				'css/style-responsive',
				'css/plugins'
			)
		);
		echo $this->Html->css(array('css/themes/default'), array('id'=>'default'));
		echo $this->Html->css(array('css/custom'));
	?>
	<!-- END THEME STYLES -->

	<!-- BEGIN USER CUSTOM STYLES -->
	<?php echo $this->fetch('css'); ?>
	<!-- END USER CUSTOM STYLES -->
	<?php
		echo $this->Html->meta('favicon.ico', '/favicon.ico', array('type' => 'icon', 'rel' => 'shortcut icon'));
	?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-sidebar-fixed page-footer-fixed">
<!-- BEGIN HEADER -->
	<div class="header navbar navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="header-inner">
			<!-- BEGIN LOGO -->
			<?php
				$img = $this->Html->image('../assets/img/logo.png', array('class'=>'img-responsive', 'alt'=>'logo'));
				echo $this->Html->link($img, array('controller'=>'pages', 'action'=>'home', 'admin'=>true), array('escape'=>false, 'class'=>'navbar-brand'))
			?>
			</a>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<?php echo $this->Html->image('/assets/img/menu-toggler.png'); ?>
			</a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN TOP NAVIGATION MENU -->
			<?php echo $this->element('admin/top-menu', array('userFullName'=>$sessionUser['User']['fullname'], 'user'=>$sessionUser));?>
			<!-- END TOP NAVIGATION MENU -->
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<div class="clearfix"></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<?php echo $this->element('admin/main-menu', array('activeMenu' => empty($activeMenu) ? NULL : $activeMenu));?>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<?php echo $this->element('admin/page-header');?>
				<!-- END PAGE HEADER-->

				<?php echo $this->Session->flash(); ?>
				<?php echo $this->Session->flash('auth'); ?>

				<!-- BEGIN PAGE CONTENT-->
				<?php echo $this->fetch('content'); ?>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<?php echo $this->element('admin/footer');?>
	<!-- END FOOTER -->

	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<!--[if lt IE 9]>
		<?php
			echo $this->Html->script(
				array(
					'plugins/respond.min',
					'excanvas.min',
				)
			);
		?>
	<![endif]-->
	<?php
		echo $this->Html->script(
			array(
				'plugins/jquery-1.10.2.min',
				'plugins/jquery-migrate-1.2.1.min',
	//IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip
				'plugins/jquery-ui/jquery-ui-1.10.3.custom.min',
				'plugins/bootstrap/js/bootstrap.min',
				'plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min',
				'plugins/jquery-slimscroll/jquery.slimscroll.min',
				'plugins/jquery.blockui.min',
				'plugins/jquery.cokie.min',
				'plugins/uniform/jquery.uniform.min',
				'plugins/bootbox/bootbox.min'
			)
		);
	?>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<?php
		echo $this->Html->script(array(
			'plugins/bootstrap-toastr/toastr.min'
			)
		);
	?>
	<!-- END PAGE LEVEL SCRIPTS -->
	<?php
		echo $this->Html->script(array('scripts/core/app'));
	?>
	<script>
		jQuery(document).ready(function() {
			App.init();
		});
	</script>
	<?php
		echo $this->fetch('script');
		echo $this->Js->writeBuffer();
	?>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>