<!DOCTYPE html>
<!--[if IE 8]> <html lang="es" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="es" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="es">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo isset($config['App']['configurations']['title']) ? $config['App']['configurations']['title'] : ''; ?> - <?php echo isset($viewTitle) && !empty($viewTitle) ? $viewTitle : ''; ?></title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<?php echo $this->Html->meta('author', 'Erick Peniche Castro') ?>
	<?php echo $this->Html->meta('keywords', '') ?>
	<?php echo $this->Html->meta('description', '') ?>
	<?php echo $this->fetch('meta'); ?>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<?php
	  echo $this->Html->css(
		array(
		  'portal/plugins/font-awesome/css/font-awesome.min',
		  'portal/plugins/bootstrap/css/bootstrap.min'
		)
	  );
	?>
	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<?php
		echo $this->Html->css(array('portal/plugins/fancybox/source/jquery.fancybox'));
		echo $this->Html->css(array('portal/plugins/revolution_slider/css/rs-style'), array('media'=>'screen'));
		echo $this->Html->css(array('portal/plugins/revolution_slider/rs-plugin/css/settings'), array('media'=>'screen'));
		echo $this->Html->css(array('portal/plugins/bxslider/jquery.bxslider'));
	?>
	<!-- END PAGE LEVEL PLUGIN STYLES -->

	<!-- BEGIN THEME STYLES -->
	<?php
		echo $this->Html->css(
			array(
				'portal/css/style-metronic',
				'portal/css/style'
			)
		);
		echo $this->Html->css(array('portal/css/themes/orange'), array('id'=>'style_color'));
		echo $this->Html->css(
			array(
				'portal/css/style-responsive',
				'portal/css/custom'
			)
		);
	?>
	<!-- END THEME STYLES -->

	<!-- BEGIN USER CUSTOM STYLES -->
	<?php echo $this->fetch('css'); ?>
	<!-- END USER CUSTOM STYLES -->
	<?php
		echo $this->Html->meta('favicon.ico', '/favicon.ico', array('type' => 'icon', 'rel' => 'shortcut icon'));
	?>
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-default navbar-static-top">
		<!-- BEGIN TOP BAR -->
		<div class="front-topbar">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-9">
						<ul class="list-unstyled inline">
							<li>
								<?php if (isset($config['App']) && !empty($config['App']['configurations']['email'])) : ?>
								<i class="fa fa-envelope-o topbar-info-icon top-2"></i>Email: <span><?php echo $this->Text->autoLinkEmails($config['App']['configurations']['email']); ?></span>
								<?php endif; ?>
							</li>
						</ul>
					</div>
					<div class="col-md-3 col-sm-3 login-reg-links">
						<ul class="list-unstyled inline">
							<li><?php echo $this->Html->link('Admin', array('controller' => 'users', 'action' => 'login', 'admin' =>true)) ?></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- END TOP BAR -->
		<div class="container">
			<div class="navbar-header">
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<button class="navbar-toggle btn navbar-btn" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN LOGO (you can use logo image instead of text)-->
				<?php
					$logo = $this->Html->image('/assets/portal/img/logo_orange.png');
					echo $this->Html->link($logo, array('controller' => 'pages', 'action' => 'home', 'admin' => false), array('escape' => false, 'class' => 'navbar-brand logo-v1'));
				?>
				<!-- END LOGO -->
			</div>

			<!-- BEGIN TOP NAVIGATION MENU -->
			<?php echo $this->element('portal/top-navigation', array('activeMenu' => empty($activeMenu) ? NULL : $activeMenu));?>
			<!-- BEGIN TOP NAVIGATION MENU -->
		</div>
	</div>
	<!-- END HEADER -->

	<!-- BEGIN PAGE CONTAINER -->
	<div class="page-container">
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>
	<!-- END PAGE CONTAINER -->

	<!-- BEGIN FOOTER -->
	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 space-mobile">
					<!-- BEGIN ABOUT -->
					<h2>Acerca de</h2>
					<p class="margin-bottom-30">Investigadores Anáhuac Mayab es un sitio educacional con motivo de promover y dar a conocer los proyectos y trabajos de los investigadores que laboran en esta institución</p>
					<div class="clearfix"></div>
					<!-- END ABOUT -->
				</div>
				<div class="col-md-4 col-sm-4 space-mobile">
					<!-- BEGIN CONTACTS -->
					<h2>Contacto</h2>
					<address class="margin-bottom-40">
						Universidad Anáhuac Mayab <br />
						Carretera Merida Progreso <br />
						Km. 15.5 AP. 96 <br />
						 (999) 942 4800 con 5 líneas <br />
						 <?php if (isset($config) && !empty($config['App']['configurations']['contact_email'])) : ?>
						 Email: <?php echo $this->Text->autoLinkEmails($config['App']['configurations']['contact_email']); ?>
						 <?php endif; ?>
					</address>
					<!-- END CONTACTS -->
				</div>
				<div class="col-md-4 col-sm-4">
				</div>
			</div>
		</div>
	</div>
	<!-- END FOOTER -->

	<!-- BEGIN COPYRIGHT -->
	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-8">
					<p>
						<span class="margin-right-10"><?php echo date('Y'); ?> ©</span>
					</p>
				</div>
				<div class="col-md-4 col-sm-4">
					<ul class="social-footer">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-skype"></i></a></li>
						<li><a href="#"><i class="fa fa-github"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
						<li><a href="#"><i class="fa fa-dropbox"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- END COPYRIGHT -->

	<!-- Load javascripts at bottom, this will reduce page load time -->
	<!-- BEGIN CORE PLUGINS(REQUIRED FOR ALL PAGES) -->
	<!--[if lt IE 9]>
		<?php echo $this->Html->script(array('portal/plugins/respond.min')); ?>
	<![endif]-->

	<?php
		echo $this->Html->script(
			array(
				'portal/plugins/jquery-1.10.2.min',
				'portal/plugins/jquery-migrate-1.2.1.min',
				'portal/plugins/bootstrap/js/bootstrap.min',
				'portal/plugins/back-to-top'
			)
		);
	?>
	<!-- END CORE PLUGINS -->

	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<?php
		echo $this->Html->script(array(
			'portal/plugins/fancybox/source/jquery.fancybox.pack',
			'portal/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min',
			'portal/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min',
			'portal/plugins/bxslider/jquery.bxslider.min'
			)
		);
	?>

	<?php
		echo $this->Html->script(
			array(
				'portal/scripts/app',
				'portal/scripts/index'
			)
		);
	?>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			App.init();
			App.initBxSlider();
			Index.initRevolutionSlider();
		});
	</script>
	<?php
		echo $this->fetch('script');
		echo $this->Js->writeBuffer();
	?>
	<!-- END PAGE LEVEL SCRIPTS -->
</body>
<!-- END BODY -->
</html>