<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>System Email</title>
		<meta name="viewport" content="width=device-width">
	</head>
	<body style="min-width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;text-align: left;line-height: 19px;font-size: 14px;direction: ltr;background: #f6f8f1;width: 100%;">
		<table class="body" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;height: 100%;width: 100%;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;">
			<tr style="padding: 0;vertical-align: top;text-align: left;">
				<td class="center" align="center" valign="top" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: top;text-align: center;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;border-collapse: collapse;">
					<!-- BEGIN: Header -->
					<table class="header" align="center" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;background: #1f1f1f;">
						<tr style="padding: 0;vertical-align: top;text-align: left;">
							<td class="center" align="center" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: top;text-align: center;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;border-collapse: collapse;">
								<!-- BEGIN: Header Container -->
								<table class="container" align="center" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: inherit;width: 580px;margin: 0 auto;">
									<tr style="padding: 0;vertical-align: top;text-align: left;">
										<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: top;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;border-collapse: collapse;">
											<table class="row " style="border-spacing: 0;border-collapse: collapse;padding: 0px;vertical-align: top;text-align: left;width: 100%;position: relative;display: block;">
												<tr style="padding: 0;vertical-align: top;text-align: left;">
													<td class="wrapper vertical-middle" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 10px 20px 0px 0px;vertical-align: middle;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;padding-top: 0;padding-bottom: 0;position: relative;border-collapse: collapse;">
														<!-- BEGIN: Logo -->
														<table class="six columns" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;margin: 0 auto;width: 280px;">
															<tr style="padding: 0;vertical-align: top;text-align: left;">
																<td class="vertical-middle" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0px 0px 10px;vertical-align: middle;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;padding-top: 0;padding-bottom: 0;border-collapse: collapse;">
																	<a href="<?php echo $this->Html->url(array('controller'=>'pages', 'action'=>'home'), true); ?>" style="color: #2ba6cb;text-decoration: none;">
																		<img src="http://yinkapublicidad.com/images/email/logo.png" width="86" height="14" border="0" alt="" style="outline: none;text-decoration: none;-ms-interpolation-mode1: bicubic;width: auto;max-width: 100%;float: left;clear: both;display: block;border: none;">
																	</a>
																</td>
															</tr>
														</table>
														<!-- END: Logo -->
													</td>
													<td class="wrapper vertical-middle last" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 10px 20px 0px 0px;vertical-align: middle;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;padding-top: 0;padding-bottom: 0;position: relative;padding-right: 0px;border-collapse: collapse;">
														<!-- BEGIN: Social Icons -->
														<table class="six columns" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;margin: 0 auto;width: 280px;">
															<tr style="padding: 0;vertical-align: top;text-align: left;">
																<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0px 0px 10px;vertical-align: top;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;border-collapse: collapse;">
																	<table class="wrapper social-icons" align="right" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;float: right;">
																		<tr style="padding: 0;vertical-align: top;text-align: left;">
																			<td class="vertical-middle" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0 2px;vertical-align: middle;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;padding-top: 0;padding-bottom: 0;border-collapse: collapse;width: auto;">
																				<a href="#" style="color: #2ba6cb;text-decoration: none;">
																					<img src="http://yinkapublicidad.com/images/email/social_facebook.png" alt="social icon" style="outline: none;text-decoration: none;-ms-interpolation-mode1: bicubic;width: auto;max-width: none;float: left;clear: both;display: block;border: none;">
																				</a>
																			</td>
																			<td class="vertical-middle" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0 2px;vertical-align: middle;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;padding-top: 0;padding-bottom: 0;border-collapse: collapse;width: auto;">
																				<a href="#" style="color: #2ba6cb;text-decoration: none;">
																					<img src="http://yinkapublicidad.com/images/email/social_twitter.png" alt="social icon" style="outline: none;text-decoration: none;-ms-interpolation-mode1: bicubic;width: auto;max-width: none;float: left;clear: both;display: block;border: none;">
																				</a>
																			</td>
																			<td class="vertical-middle" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0 2px;vertical-align: middle;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;padding-top: 0;padding-bottom: 0;border-collapse: collapse;width: auto;">
																				<a href="#" style="color: #2ba6cb;text-decoration: none;">
																					<img src="http://yinkapublicidad.com/images/email/social_googleplus.png" alt="social icon" style="outline: none;text-decoration: none;-ms-interpolation-mode1: bicubic;width: auto;max-width: none;float: left;clear: both;display: block;border: none;">
																				</a>
																			</td>
																			<td class="vertical-middle" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0 2px;vertical-align: middle;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;padding-top: 0;padding-bottom: 0;border-collapse: collapse;width: auto;">
																				<a href="#" style="color: #2ba6cb;text-decoration: none;">
																					<img src="http://yinkapublicidad.com/images/email/social_linkedin.png" alt="social icon" style="outline: none;text-decoration: none;-ms-interpolation-mode1: bicubic;width: auto;max-width: none;float: left;clear: both;display: block;border: none;">
																				</a>
																			</td>
																			<td class="vertical-middle" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0 2px;vertical-align: middle;text-align: left;color: #222222;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;padding-top: 0;padding-bottom: 0;border-collapse: collapse;width: auto;">
																				<a href="#" style="color: #2ba6cb;text-decoration: none;">
																					<img src="http://yinkapublicidad.com/images/email/social_rss.png" alt="social icon" style="outline: none;text-decoration: none;-ms-interpolation-mode1: bicubic;width: auto;max-width: none;float: left;clear: both;display: block;border: none;">
																				</a>
																			</td>
																		</tr>
																	</table>
																</td>
															</tr>
														</table>
														<!-- END: Social Icons -->
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<!-- END: Header Container -->
							</td>
						</tr>
					</table>
					<!-- END: Header -->
					<!-- BEGIN: Content -->
					<?php echo $this->fetch('content'); ?>
					<!-- END: Content -->
					<!-- BEGIN: Footer -->
					<table class="footer" align="center" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;background: #2f2f2f;">
						<tr style="padding: 0;vertical-align: top;text-align: left;">
							<td class="center" align="center" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: middle;text-align: center;color: #fff;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;border-collapse: collapse;">
								<table class="container" align="center" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: inherit;width: 580px;margin: 0 auto;">
									<tr style="padding: 0;vertical-align: top;text-align: left;">
										<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: middle;text-align: left;color: #fff;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;border-collapse: collapse;">
											<!-- BEGIN: Unsubscribet -->
											<table class="row" style="border-spacing: 0;border-collapse: collapse;padding: 0px;vertical-align: top;text-align: left;width: 100%;position: relative;display: block;">
												<tr style="padding: 0;vertical-align: top;text-align: left;">
													<td class="wrapper last" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 10px 20px 0px 0px;vertical-align: middle;text-align: left;color: #fff;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;position: relative;padding-right: 0px;border-collapse: collapse;">
														<span style="font-size:12px;"><i>Este es un mensaje generado por el sistema y no se requiere una respuesta.</i></span>
													</td>
												</tr>
											</table>
											<!-- END: Unsubscribe -->
											<!-- BEGIN: Footer Panel -->
											<table class="row" style="border-spacing: 0;border-collapse: collapse;padding: 0px;vertical-align: top;text-align: left;width: 100%;position: relative;display: block;">
												<tr style="padding: 0;vertical-align: top;text-align: left;">
													<td class="wrapper" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 10px 20px 0px 0px;vertical-align: middle;text-align: left;color: #fff;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;position: relative;border-collapse: collapse;">
														<table class="four columns" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;margin: 0 auto;width: 180px;">
															<tr style="padding: 0;vertical-align: top;text-align: left;">
																<td class="vertical-middle" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0px 0px 10px;vertical-align: middle;text-align: left;color: #fff;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;padding-top: 0;padding-bottom: 0;border-collapse: collapse;">
																	<?php echo date('Y') ?> &copy; <?php echo $config['title']; ?>
																</td>
															</tr>
														</table>
													</td>
													<td class="wrapper last" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 10px 20px 0px 0px;vertical-align: middle;text-align: left;color: #fff;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;position: relative;padding-right: 0px;border-collapse: collapse;">
														<table class="eight columns" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;margin: 0 auto;width: 380px;">
															<tr style="padding: 0;vertical-align: top;text-align: left;">
																<td class="vertical-middle align-reverse" style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0px 0px 10px;vertical-align: middle;text-align: right;color: #fff;font-family: &quot;Helvetica&quot;, &quot;Arial&quot;, sans-serif;font-weight: normal;margin: 0;line-height: 19px;font-size: 14px;padding-top: 0;padding-bottom: 0;border-collapse: collapse;">
																	<a href="#" style="color: #2ba6cb;text-decoration: none;">About Us</a>&nbsp;
																	<a href="#" style="color: #2ba6cb;text-decoration: none;">Privacy Policy</a>&nbsp;
																	<a href="#" style="color: #2ba6cb;text-decoration: none;">Terms of Use</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
											<!-- END: Footer Panel List -->
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<!-- END: Footer -->
				</td>
			</tr>
		</table>
	</body>
</html>