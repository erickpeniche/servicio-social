<div class="mainImages view">
<h2><?php echo __('Main Image'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($mainImage['MainImage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($mainImage['MainImage']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($mainImage['MainImage']['image']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Link'); ?></dt>
		<dd>
			<?php echo h($mainImage['MainImage']['link']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($mainImage['MainImage']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($mainImage['MainImage']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Main Image'), array('action' => 'edit', $mainImage['MainImage']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Main Image'), array('action' => 'delete', $mainImage['MainImage']['id']), null, __('Are you sure you want to delete # %s?', $mainImage['MainImage']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Main Images'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Main Image'), array('action' => 'add')); ?> </li>
	</ul>
</div>
