<?php
	echo $this->Html->css(array(
		'plugins/bootstrap-datepicker/css/datepicker',
		'plugins/bootstrap-switch/css/bootstrap-switch.min',
		'plugins/select2/select2',
		'plugins/select2/select2-metronic'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/bootstrap-datepicker/js/bootstrap-datepicker',
		'plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.es',
		'plugins/ckeditor/ckeditor',
		'plugins/bootstrap-switch/js/bootstrap-switch.min',
		'plugins/jquery-validation/dist/jquery.validate.min',
		'plugins/jquery-validation/localization/messages_es',
		'plugins/jquery-validation/dist/additional-methods.min',
		'plugins/select2/select2.min',
		'/js/mainImages/admin_add'
		), array('inline' => false)
	);

	$this->Html->addCrumb('mainImages',
		array(
			'action' => 'index',
			'controller' => 'mainImages',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Agregar',
		array(
			'action' => 'add',
			'controller' => 'mainImages',
			'admin' => true
			)
		);
?>
<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-plus"></i>Nuevo mainImages</div>
				<div class="actions">
					<?php
						echo $this->Html->link('<i class="m-icon-swapleft"></i> mainImages', array('controller'=>'mainImages', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon green-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<?php
				echo $this->Form->create('MainImage',
					array(
						'inputDefaults' => array(
							'div' => false,
							'label' => false,
							'class' => 'form-control',
							'error' => false
						),
						'class' => 'form-horizontal form-row-seperated',
						'novalidate' => true
					)
				);
			?>
				<div class="form-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						Existen errores en el formulario. Por favor verifica tus datos.
					</div>

				</div>
				<div class="form-actions fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-offset-3 col-md-9">
								<?php
									echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
										array('type' => 'submit', 'class' => 'btn green')
										).'&nbsp;';

									$cancelOptions = array('action' => 'index', 'controller' => 'mainImages', 'admin' => true);
									echo $this->Form->button('Cancelar',
										array(
											'type' => 'button',
											'class' => 'btn red',
											'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
											)
										);
								?>
							</div>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'FormValidation.init();',
		array('inline' => false)
	);
?>
