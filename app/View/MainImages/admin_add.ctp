<?php
	echo $this->Html->css(array(
		'plugins/bootstrap-fileinput/bootstrap-fileinput'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/bootstrap-fileinput/bootstrap-fileinput',
		'plugins/jquery-validation/dist/jquery.validate.min',
		'plugins/jquery-validation/localization/messages_es',
		'plugins/jquery-validation/dist/additional-methods.min',
		'/js/main_images/admin_add'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Imágenes de portada',
		array(
			'action' => 'index',
			'controller' => 'main_images',
			'admin' => true
		)
	);

	$this->Html->addCrumb('Agregar',
		array(
			'action' => 'add',
			'controller' => 'main_images',
			'admin' => true
			)
		);
?>
<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-plus"></i>Nueva Portada</div>
				<div class="actions">
					<?php
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Portadas', array('controller' => 'main_images', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon green-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<?php
				echo $this->Form->create('MainImage',
					array(
						'inputDefaults' => array(
							'div' => false,
							'label' => false,
							'class' => 'form-control',
							'error' => false
						),
						'class' => 'form-horizontal form-row-seperated',
						'enctype' => 'multipart/form-data',
						'novalidate' => true
					)
				);
			?>
				<div class="form-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						Existen errores en el formulario. Por favor verifica tus datos.
					</div>

					<!-- Description -->
					<div class="form-group">
						<label class="control-label col-md-3">Descripción</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
								<?php echo $this->Form->input('description'); ?>
							</div>
						</div>
					</div>
					<!-- END Description -->

					<!-- Description -->
					<div class="form-group">
						<label class="control-label col-md-3">Enlace</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-link"></i></span>
								<?php echo $this->Form->input('link'); ?>
							</div>
						</div>
					</div>
					<!-- END Description -->

					<!-- Image upload -->
					<div class="form-group last <?php echo $this->Form->isFieldError('image') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Imagen<span class="required">*</span></label>
						<div class="col-md-9">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
									<?php echo $this->Html->image('/assets/img/img-holdit.png'); ?>
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
								<div>
									<?php echo $this->Form->isFieldError('image') ? $this->Form->error('image', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
									<span class="btn default btn-file">
										<span class="fileinput-new"> Seleccionar</span>
										<span class="fileinput-exists"> Cambiar</span>
										<?php echo $this->Form->file('image'); ?>
									</span>
									<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> Eliminar</a>
								</div>
							</div>
							<div class="clearfix margin-top-10">
								<span class="label label-danger"> NOTA!</span>
								<span>El archivo no debe ser mayor a <strong>20 MB</strong> y debe estar en formato <strong>JPG, PNG o GIF</strong>.</span>
							</div>
						</div>
					</div>
					<!-- END Image upload -->

				</div>
				<div class="form-actions fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-offset-3 col-md-9">
								<?php
									echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
										array('type' => 'submit', 'class' => 'btn green')
										).'&nbsp;';

									$cancelOptions = array('action' => 'index', 'controller' => 'main_images', 'admin' => true);
									echo $this->Form->button('Cancelar',
										array(
											'type' => 'button',
											'class' => 'btn red',
											'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
											)
										);
								?>
							</div>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'FormValidation.init();',
		array('inline' => false)
	);
?>
