<?php
	echo $this->Html->css(array(
		'plugins/fancybox/source/jquery.fancybox',
		'css/pages/portfolio',
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/jquery-mixitup/jquery.mixitup.min',
		'plugins/fancybox/source/jquery.fancybox.pack',
		'/js/main_images/admin_index'
		), array('inline' => false)
	);


	$this->Html->addCrumb('Imágenes de portada',
		array(
			'action' => 'index',
			'controller' => 'main_images',
			'admin' => true
		)
	);
?>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box grey">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-picture-o"></i>Imágenes de portada</div>
				<div class="actions">
				</div>
			</div>
			<div class="portlet-body">
				<div class="margin-top-10">
					<div class="mix-filter">
						<blockquote>
							<p style="font-size:16px">
								 Haz click en <?php echo $this->Html->link('<i class="fa fa-plus"></i> Agregar',
									array(
										'action' => 'add',
										'controller' => 'main_images'
									),
									array(
										'class' => 'btn green',
										'escape' => false)
									);
								?> para añadir una imagen de portada
							</p>
						</blockquote>
					</div>
					<div class="row mix-grid">
					<?php if (isset($mainImages) && !empty($mainImages)) : ?>
						<?php foreach ($mainImages as $image) : ?>
						<div class="col-md-3 col-sm-4 mix">
							<div class="mix-inner">
								<?php
									echo $this->Html->image('images/main_index_'.$image['MainImage']['image'], array('class' => 'img-responsive'));
								?>
								<div class="mix-details">
									<?php if (isset($image) && !empty($image['MainImage']['description'])) : ?>
									<h4><?php echo $image['MainImage']['description']; ?></h4>
									<?php endif; ?>
									<?php
										if (isset($image) && !empty($image['MainImage']['link'])) {
											echo $this->Html->link('<i class="fa fa-link"></i>', $image['MainImage']['link'], array('class' => 'mix-link', 'escape' => false, 'target' => '_blank')).'<br><br><br>';
										}

										echo $this->Html->link('<i class="fa fa-trash-o"></i>', array('controller' => 'main_images', 'action' => 'delete', $image['MainImage']['id'], 'admin' => true), array('id' => 'delete-btn', 'class'=>'mix-link', 'escape' => false, 'data-id' => $image['MainImage']['id'], 'data-name' => '', 'escape' => false));

										echo $this->Html->link('<i class="fa fa-search"></i>', '/img/images/main_image_'.$image['MainImage']['image'], array('class' => 'mix-preview fancybox-button', 'escape' => false, 'title' => !empty($image['MainImage']['description']) ? $image['MainImage']['description'] : '', 'data-rel' => 'fancybox-button'));
									?>
								</div>
							</div>
						</div>
						<?php endforeach; ?>
					<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'AdminIndex.init();',
		array('inline' => false)
	);
?>