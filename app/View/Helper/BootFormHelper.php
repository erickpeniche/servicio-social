<?php
App::uses('FormHelper', 'View/Helper');
class BootFormHelper extends FormHelper {
	private $sizes = array('mini', 'small', 'medium', 'large', 'xlarge', 'xxlarge');
	private $horizontal = FALSE;
	public $helpers = array('Html');

	public function create($model = null, $options = array()) {

		// $options['class'] = empty($options['class']) ? 'form' : $options['class'] . ' form';
		$options['class'] = empty($options['class']) ? '' : $options['class'];

		if (empty($options['vertical'])) {
			$options['class'] .= ' form-horizontal form-row-seperated';
			$this->horizontal = TRUE;
		}
		else {
			unset($options['vertical']);
		}

		if (isset($options['async'])) {
			unset($options['async']);
		}

		$options['novalidate'] = TRUE;

		return parent::create($model, $options);
	}

	public function input($fieldName, $options = array()) {
		$c = '';
		$optionsLegend = array();

		$this->setEntity($fieldName);
		$options = $this->_parseOptions($options);
		$this->options = $options;

		if (!empty($options['div']) && is_string($options['div'])) {
			unset($options['div']);
		} else if (!empty($options['div']['class'])) {
			unset($options['div']['class']);
		}

		$beforeContent = '';
		$afterContent = '';

		if (
			(!empty($options['type']) && in_array($options['type'], array('radio', 'checkbox'))) ||
			(!empty($options['multiple']) && $options['multiple'] == 'checkbox')
		) {
			unset($options['prepend']);
			unset($options['append']);
			unset($options['size']);
			$options['class'] = (isset($options['class']) && $options['class'] !== FALSE) ? $options['class'] : '';
		}
		else {
			$options['class'] = (isset($options['class']) && $options['class'] !== FALSE) ? 'form-control ' . $options['class'] : 'form-control';
		}

		if ($this->horizontal && (!isset($options['div']) || $options['div'] !== FALSE)) {
			$divClass = 'col-md-9';

			if (!empty($options['inputDiv']) && is_string($options['inputDiv'])) {
				$divClass = $options['inputDiv'];
			}
			elseif (!empty($options['inputDiv']['class'])) {
				$divClass = $options['inputDiv']['class'];
			}

			$divClass .= (empty($options['prepend']) && empty($options['append'])) ? '' : ' input-group';

			$beforeContent 	= '<div class="' . $divClass . '">';
			$afterContent 	= '</div>';
		}

		unset($options['inputDiv']);

		if (isset($options['legend']) && $options['legend'] !== false) {
			if (!empty($options['legend'])) {
				if (is_string($options['legend'])) {
					$legend = $options['legend'];
				}
				else {
					$legend = isset($options['legend']['text']) ? $options['legend']['text'] : __(Inflector::humanize($this->field()));
					$optionsLegend['class'] = isset($options['legend']['class']) ? $options['legend']['class'] : '';
				}
			} else {
				$legend = __(Inflector::humanize($this->field()));
			}

			if (isset($options['required']) && $options['required'] === true) {
				$legend .= '<span class="required">*</span>';
			}

			$optionsLegend['class'] = isset($optionsLegend['class']) && !empty($optionsLegend['class']) ? 'control-label ' . $optionsLegend['class'] : 'control-label col-md-3';

			$options['before'] = $this->label($fieldName, $legend, $optionsLegend) . $options['before'] . $beforeContent;
			$options['legend'] = false;
			$options['label'] = false;
		} else {
			$options['before'] = empty($options['before']) ? $beforeContent : ($options['before'] . $beforeContent);
		}
		$options['after'] = empty($options['after']) ? $afterContent : ($options['after'] . $afterContent);

		$options = $this->_inputSize($options);
		$options = $this->_inputAddOn($options);
		$options = $this->_setHint($options);

		if (isset($options['icon']) && !empty($options['icon'])) {
			$options['before'] .= '<div class="input-group"><span class="input-group-addon"><i class="fa fa-' . $options['icon'] . '"></i></span>';
			$options['after'] = '</div></div>';
		}

		unset($options['icon']);

		if (!empty($options['type']) && $options['type'] == 'date') {
			$options = $this->_setCalendar($options);
		}
		elseif (!empty($options['type']) && in_array($options['type'], array('radio', 'checkbox'))) {
			//if (!isset($options['legend'])) {

				if (isset($options['legend']) && $options['legend'] !== false) {
					//$legend = empty($options['legend']) ? __(Inflector::humanize($this->field())) : $options['legend'];

					if (!empty($options['legend'])) {
						if (is_string($options['legend'])) {
							$legend = $options['legend'];
						}
						else {
							$legend = isset($options['legend']['text']) ? $options['legend']['text'] : __(Inflector::humanize($this->field()));
							$optionsLegend['class'] = isset($options['legend']['class']) ? $options['legend']['class'] : '';
						}
					}
					else {
						$legend = __(Inflector::humanize($this->field()));
					}

					$optionsLegend['for'] = $this->field() . '-legend';
					$optionsLegend['class'] = !isset($optionsLegend['class']) ? 'control-label' : 'control-label ' . $optionsLegend['class'];

					$options['before'] = $this->label($fieldName, $legend, $optionsLegend) . $options['before'];

					$options['legend'] = FALSE;
				}

				// Wrapper divs for every option
				if (isset($options['inline']) && $options['inline'] !== false) {
					$c = $options['type'] . '-inline';
				}

				$options['separator'] = "</div><div class='" . $c . "'>";
				$options['before'] = $options['before'] . "<div class='" . $c . "'>";
				$options['after'] = $options['after'] . '</div>';

				// pr($options);
			//}
		}

		if (!empty($options['prepend'])) {
			$options['before'] .= $this->_getAddOn($options['prepend']);
			unset($options['prepend']);
		}

		if (!empty($options['append'])) {
			$options['after'] = $this->_getAddOn($options['append']) . $options['after'];
			unset($options['append']);
		}

		if (isset($options['help']) && !empty($options['help'])) {
			$options['after'] = '<span class="help-block">' . $options['help'] . '</span>' . $options['after'];
			unset($options['help']);
		}

		return parent::input($fieldName, $options);
	}

	public function submit($caption = null, $options = array()) {
		$c = 'col-md-offset-3 col-md-9';

		if (!isset($options['div']) || $options['div'] !== FALSE) {
			if (empty($options['div'])) {
				$options['div'] = 'form-actions fluid';
			} else {
				$options['div'] = is_string($options['div']) ? ($options['div'] . ' form-actions fluid') : (empty($options['div']['class']) ? 'form-actions fluid' : ($options['div']['class'] . ' submit form-group'));
			}
		}

		$options['before'] = (isset($options['before']) && $options['before'] !== FALSE) ? $options['before'] : '';
		$options['after'] = (isset($options['after']) && $options['after'] !== FALSE) ? $options['after'] : '';

		if (is_array($options['before'])) {
			$c = (isset($options['before']['class']) && $options['before']['class'] !== false) ? $options['before']['class'] : '';
			$options['before'] = '';
		}

		if ($this->horizontal) {
			if (empty($options['before']) && $options['before'] !== false && empty($options['after']) && $options['after'] !== false) {
				$options['before'] = "<div class='form-actions fluid'><div class='row'><div class='col-md-12'><div class='" . $c . "'>";
				$options['after'] = '</div></div></div></div>';
			}
		}

		$options['class'] = empty($options['class']) ? 'btn green' : ($options['class'] . ' btn green');

		if (!empty($options['color'])) {
			$options['class'] .= ' btn ' . $options['color'];
		}

		echo $options['before'];
		echo FormHelper::button('<i class="fa fa-check"></i> ' . $caption,
			array('type' => 'submit', 'class' => $options['class'])
			).'&nbsp;';

		$cancelOptions = array('action' => 'index', 'admin' => true);
		echo FormHelper::button('Cancelar',
			array(
				'type' => 'button',
				'class' => 'btn red',
				'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
				)
			);
		echo $options['after'];
	}

	public function label($fieldName = NULL, $text = NULL, $options = array()) {
		if (
			((empty($this->options['multiple']) || $this->options['multiple'] !== 'checkbox') &&
			(empty($this->options['type']) || !in_array($this->options['type'], array('checkbox', 'radio')))) ||
			(!empty($this->options['multiple']) && !empty($options['for']) && $options['for'] == $this->field())
		) {
			if (empty($options)){
				$options['class'] = 'control-label col-md-3';
			} else {
				$options['class'] = is_string($options) ? ($options . ' control-label') : (empty($options['class']) ? 'control-label col-md-3' : $options['class']);
			}
		}

		$modelKey = Inflector::underscore($this->model());
		$labelKey = empty($text) ? Inflector::slug($this->field(), '-') : $text;
		$text = !empty($text) ? $text : 'null';

		return parent::label($fieldName, $text, $options);
	}

	public function hint($options) {
		$hintAttributes = array(
			'escape' => false,
			'data-toggle' => 'popover',
			'data-content' => is_string($options['hint']) ? $options['hint'] : (empty($options['hint']['message']) ? ' ' : $options['hint']['message']),
			'class' => 'toggle-popover help-inline'
		);

		if (is_array($options['hint'])) {
			if (!empty($options['hint']['title'])) {
				$hintAttributes['title'] = $options['hint']['title'];
				$hintAttributes['data-original-title'] = $options['hint']['title'];
			}

			if (!empty($options['hint']['placement'])) {
				$hintAttributes['data-placement'] = $options['hint']['placement'];
			}
		}

		$hint = $this->Html->link(
			'<i class="icon-question-sign"></i>',
			'#',
			$hintAttributes
		);

		return $hint;
	}

	protected function _divOptions($options) {
		$divOptions = parent::_divOptions($options);
		$divOptions['class'] = '';

		if (!empty($divOptions)) {
			$divOptions = $this->addClass($divOptions, 'form-group');
			$divOptions['class'] = isset($this->options['last']) && $this->options['last'] == true ? $divOptions['class'] . ' last' : $divOptions['class'];
		}

		if (!empty($options['type']) && $options['type'] == 'password') {
			$divOptions = $this->addClass($divOptions, 'text');
		}

		if (!empty($this->options['div']) && is_string($this->options['div'])) {
			$divOptions = $this->addClass($divOptions, $this->options['div']);
		} else if (!empty($this->options['div']['class'])) {
			$divOptions = $this->addClass($divOptions, $this->options['div']['class']);
		}

		return $divOptions;
	}

	protected function _inputSize($options) {
		if (!empty($options['size']) && in_array($options['size'], $this->sizes)) {
			$sizeClass = 'input-' . $options['size'];
			$options['class'] = empty($options['class']) ? $sizeClass : ($options['class'] . ' ' . $sizeClass);
		}
		unset($options['size']);

		return $options;
	}

	protected function _inputAddOn($options) {
		if (!empty($options['append']) || !empty($options['prepend'])) {
			// $options['class'] = empty($options['class']) ? ' unit' : ($options['class'] . ' unit');
			$options['before'] = "<div class='input-group'>";
			$options['after'] = '</div>';
		}

		return $options;
	}

	protected function _setHint($options) {
		if (!empty($options['hint']) && (is_string($options['hint']) || !empty($options['hint']['message']))) {
			$hint = $this->hint($options);
			$options['after'] = empty($options['after']) ? $hint : ($hint . $options['after']);
		}

		unset($options['hint']);

		return $options;

	}

	protected function _setCalendar($options) {
		$attributes = array();
		$phpFormat = empty($options['format']) ? 'd/m/Y' : $options['format'];
		$autoclose = empty($options['autoclose']) ? 'true' : $options['autoclose'];
		$startDate = empty($options['startDate']) ? null : $options['startDate'];
		$language = empty($options['language']) ? 'en' : $options['language'];

		$format = str_replace(
			array('d', 'j', 'm', 'n', 'y', 'Y'),
			array('dd', 'd', 'mm', 'm', 'yy', 'yyyy'),
			$phpFormat
		);

		$minViewMode = 'years';
		$startView = 'years';

		$date = empty($options['date']) ? date($phpFormat) : $options['date'];

		if (in_array('d', str_split($format))) {
			$minViewMode = 'days';
			$startView = empty($options['mode']) || !in_array($options['mode'], array('day', 'month', 'year', 'decade')) ? 'days' : $options['mode'];
		} else if (in_array('m', str_split($format))) {
			$minViewMode = 'months';
			$startView = empty($options['mode']) || !in_array($options['mode'], array('month', 'year', 'decade')) ? 'months' : $options['mode'];
		}

		if (!empty($options['disabled'])) {
			$attributes[] = "class='input-group'";
		} else {
			$attributes[] = "class='input-group date datepicker'";
			$attributes[] = "data-date='" . $date . "'";
			$attributes[] = "data-date-format='" . $format . "'";
			$attributes[] = "data-date-start-view='" . $startView . "'";
			$attributes[] = "data-date-min-view-mode='" . $minViewMode . "'";
			$attributes[] = "data-date-autoclose='" . $autoclose . "'";
			$attributes[] = "data-date-language='" . $language . "'";

			if(!is_null($startDate)) {
				$attributes[] = "data-date-start-date='" . $startDate . "'";
			}
		}

		$calendarDiv = "<div " . implode($attributes, ' ')  . ">";
		$calendarButton = "<span class='input-group-addon'><i class='icon-calendar'></i></span></div>";

		$options['before'] = empty($options['before']) ? $calendarDiv : ($options['before'] . $calendarDiv);
		$options['after'] = empty($options['after']) ? $calendarButton : ($calendarButton . $options['after']);


		$options['type'] = 'text';
		$options['readOnly'] = true;
		unset($options['date']);
		unset($options['format']);
		unset($options['mode']);
		unset($options['autoclose']);
		unset($options['startDate']);
		unset($options['language']);

		return $options;
	}

	protected function _getAddOn($html) {
		return "<span class='input-group-addon'>" . $html . "</span>";
	}

	protected function _getFormat($options) {
		if ($options['type'] === 'hidden') {
			return array('input');
		}
		if (is_array($options['format']) && in_array('input', $options['format'])) {
			return $options['format'];
		}
		if ($options['type'] === 'checkbox') {
			return array('before', 'input', 'between', 'label', 'after', 'error');
		}

		return array('label', 'before', 'between', 'input', 'error' , 'after');
	}
}
