<?php
	echo $this->Html->css(array(
		'css/pages/profile'
		), null, array('inline' => false)
	);

	$this->Html->addCrumb('Perfiles',
		array(
			'action' => 'index',
			'controller' => 'profiles',
			'admin' => true
			)
		);

	$this->Html->addCrumb($profile['User']['fullname'],
		array(
			'action' => 'view',
			'controller' => 'profiles',
			'admin' => true,
			$profile['Profile']['id'],
			Format::clean($profile['User']['fullname'])
			)
		);
?>

<div class="row profile">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">&nbsp;</div>
				<div class="actions">
					<?php
						echo $ownAccount ? $this->Html->link('<i class="fa fa-pencil"></i> Editar', array('controller'=>'profiles', 'action' => 'edit', 'admin'=>true, $profile['Profile']['id'], Format::clean($profile['User']['fullname'])), array('class'=>'btn default yellow-stripe', 'escape'=>false)).'&nbsp;' : '';
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Perfiles', array('controller'=>'profiles', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon blue-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row profile">
					<div class="col-md-12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom tabbable-full-width">
							<ul class="nav nav-tabs">
								<li class="active">
									<a href="#biography" data-toggle="tab">
										 Biografía
									</a>
								</li>
								<li>
									<a href="#education" data-toggle="tab">
										 Formación Académica
									</a>
								</li>
								<li>
									<a href="#profesion" data-toggle="tab">
										 Profesión
									</a>
								</li>
								<li>
									<a href="#projects" data-toggle="tab">
										 Proyectos
									</a>
								</li>
								<li>
									<a href="#researches" data-toggle="tab">
										 Investigaciones
									</a>
								</li>
								<li>
									<a href="#contact" data-toggle="tab">
										 Contacto
									</a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="biography">
									<div class="row">
										<div class="col-md-3">
											<ul class="list-unstyled profile-nav">
												<li>
													<?php
														$img = !empty($profile['User']['image']) ? 'images/user_img_'.$profile['User']['image'] : '/assets/img/img-holdit-edit-profile.png';
														echo $this->Html->image($img, array('class'=>'img-responsive'));
														echo $ownAccount ? $this->Html->link('editar', array('controller' => 'users', 'action'=>'edit', 'admin'=>true, $profile['User']['id'], Format::clean($profile['User']['fullname']), '#' => 'avatar'), array('class'=>'profile-edit')) : '';
													?>
												</li>
											</ul>
										</div>
										<div class="col-md-9">
											<div class="row">
												<div class="col-md-8 profile-info">
													<h1><?php echo isset($profile['User']['title']) && !empty($profile['User']['title']) ? $profile['User']['title']. ' ' .$profile['User']['fullname'] : $profile['User']['fullname']; ?></h1>
													<ul class="list-inline">
														<?php if (isset($profile['Profile']['birth_place']) && !empty($profile['Profile']['birth_place'])) : ?>
														<li>
															<i class="fa fa-map-marker"></i> <?php echo $profile['Profile']['birth_place']; ?>
														</li>
														<?php endif; ?>
														<?php if (isset($profile['User']['birthday']) && !empty($profile['User']['birthday'])) : ?>
														<li>
															<i class="fa fa-calendar"></i> <?php echo $this->Time->format($profile['User']['birthday'], '%d de %B de %Y'); ?>
														</li>
														<?php endif; ?>
													</ul>
												</div>
												<!--end col-md-8-->
											</div>
											<!--end row-->
											<div class="row">
												<div class="col-md-12">
												<?php if (isset($profile['Profile']['biography']) && !empty($profile['Profile']['biography'])) : ?>
												<h3 class="form-section">Biografía</h3>
												<?php echo $profile['Profile']['biography']; ?>
												<?php endif; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--tab_1_2-->
								<div class="tab-pane" id="education">
									<div class="row">
										<div class="col-md-3">
											<ul class="list-unstyled profile-nav">
												<li>
													<?php
														$img = !empty($profile['User']['image']) ? 'images/user_img_'.$profile['User']['image'] : '/assets/img/img-holdit-edit-profile.png';
														echo $this->Html->image($img, array('class'=>'img-responsive'));
														echo $ownAccount ? $this->Html->link('editar', array('controller' => 'users', 'action'=>'edit', 'admin'=>true, $profile['User']['id'], Format::clean($profile['User']['fullname']), '#' => 'avatar'), array('class'=>'profile-edit')) : '';
													?>
												</li>
											</ul>
										</div>
										<div class="col-md-9">
											<div class="row">
												<div class="col-md-12">
												<?php if (isset($profile['Profile']['education']) && !empty($profile['Profile']['education'])) : ?>
												<h2>Formación Académica</h2><hr>
												<?php echo $profile['Profile']['education']; ?>
												<?php endif; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end tab-pane-->
								<div class="tab-pane" id="profesion">
									<div class="row">
										<div class="col-md-3">
											<ul class="list-unstyled profile-nav">
												<li>
													<?php
														$img = !empty($profile['User']['image']) ? 'images/user_img_'.$profile['User']['image'] : '/assets/img/img-holdit-edit-profile.png';
														echo $this->Html->image($img, array('class'=>'img-responsive'));
														echo $ownAccount ? $this->Html->link('editar', array('controller' => 'users', 'action'=>'edit', 'admin'=>true, $profile['User']['id'], Format::clean($profile['User']['fullname']), '#' => 'avatar'), array('class'=>'profile-edit')) : '';
													?>
												</li>
											</ul>
										</div>
										<div class="col-md-9">
											<form class="form-horizontal" role="form">
												<h2 class="margin-bottom-20"> Información profesional</h2>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label col-md-3">Profesión:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	<?php echo !empty($profile['Profile']['profession']) ? $profile['Profile']['profession'] : '-'; ?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label col-md-3">Ocupación actual:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	<?php echo !empty($profile['Profile']['occupation']) ? $profile['Profile']['occupation'] : '-'; ?>
																</p>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label col-md-3">Lugar de trabajo:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	<?php echo !empty($profile['Profile']['workplace']) ? $profile['Profile']['workplace'] : '-'; ?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->

												<h3 class="form-section">Experiencia</h3>
												<div class="row">
													<div class="col-md-12">
														<?php if (isset($profile['Profile']['experience']) && !empty($profile['Profile']['experience'])) : ?>
														<?php echo $profile['Profile']['experience']; ?>
														<?php endif; ?>
													</div>
												</div>

												<h3 class="form-section">Areas laborales</h3>
												<div class="row">
													<div class="col-md-12">
														<?php if (isset($profile['Profile']['laboral_areas']) && !empty($profile['Profile']['laboral_areas'])) : ?>
														<?php echo $profile['Profile']['laboral_areas']; ?>
														<?php endif; ?>
													</div>
												</div>

												<h3 class="form-section">Cursos, Diplomados y Talleres</h3>
												<div class="row">
													<div class="col-md-12">
														<?php if (isset($profile['Profile']['courses']) && !empty($profile['Profile']['courses'])) : ?>
														<?php echo $profile['Profile']['courses']; ?>
														<?php endif; ?>
													</div>
												</div>

												<h3 class="form-section">Idiomas</h3>
												<div class="row">
													<div class="col-md-12">
														<?php if (isset($profile['Profile']['languages']) && !empty($profile['Profile']['languages'])) : ?>
														<?php echo $profile['Profile']['languages']; ?>
														<?php endif; ?>
													</div>
												</div>

												<h3 class="form-section">Intereses</h3>
												<div class="row">
													<div class="col-md-12">
														<?php if (isset($profile['Profile']['interests']) && !empty($profile['Profile']['interests'])) : ?>
														<?php echo $profile['Profile']['interests']; ?>
														<?php endif; ?>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
								<!--end tab-pane-->
								<div class="tab-pane" id="projects">
									<div class="row">
										<div class="col-md-12">
											<div class="add-portfolio">
												<span>
													 <?php echo count($projects); ?> Proyectos
												</span>
												<?php
													echo $this->Html->link('Agregar Proyecto <i class="m-icon-swapright m-icon-white"></i>', array('controller' => 'projects', 'action' => 'add', 'admin' => true), array('class' => 'btn icn-only green', 'escape' => false));
												?>
											</div>
										</div>
									</div>
									<!--end add-portfolio-->
									<?php foreach ($projects as $project) : ?>
										<div class="row portfolio-block">
											<div class="col-md-5">
												<div class="portfolio-text">
													<?php
														echo $this->Html->image('/../assets/img/profile/portfolio/logo_metronic.jpg')
													?>
													<div class="portfolio-text-info">
														<h4><?php echo $project['Project']['title']; ?></h4>
														<?php if (isset($project['Project']) && $project['Project']['working'] == 1) : ?>
														<p>
															*Actualmente trabajando en este proyecto
														</p>
														<?php endif; ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 portfolio-stat">
											</div>
											<div class="col-md-2">
												<div class="portfolio-btn">
													<?php
														echo $this->Html->link('<span>Ver</span>', array('controller' => 'projects', 'action' => 'view', $project['Project']['id'], Format::clean($project['Project']['title'])), array('class' => 'btn bigicn-only', 'escape' => false));
													?>
												</div>
											</div>
											<div class="col-md-2">
												<div class="portfolio-btn">
													<?php
														echo $this->Html->link('<span>Editar</span>', array('controller' => 'projects', 'action' => 'edit', $project['Project']['id'], Format::clean($project['Project']['title'])), array('class' => 'btn bigicn-only', 'escape' => false));
													?>
												</div>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
								<!--end tab-pane-->
								<div class="tab-pane" id="researches">
									<div class="row">
										<div class="col-md-12">
											<div class="add-portfolio">
												<span>
													 <?php echo count($researches); ?> Investigaciones
												</span>
												<?php
													echo $this->Html->link('Agregar Investigación <i class="m-icon-swapright m-icon-white"></i>', array('controller' => 'researches', 'action' => 'add', 'admin' => true), array('class' => 'btn icn-only green', 'escape' => false));
												?>
											</div>
										</div>
									</div>
									<!--end add-portfolio-->
									<?php foreach ($researches as $research) : ?>
										<div class="row portfolio-block">
											<div class="col-md-5">
												<div class="portfolio-text">
													<?php
														echo $this->Html->image('/../assets/img/profile/portfolio/logo_metronic.jpg')
													?>
													<div class="portfolio-text-info">
														<h4><?php echo $research['Research']['title']; ?></h4>
														<?php if (isset($research['Research']) && $research['Research']['working'] == 1) : ?>
														<p>
															*Actualmente trabajando en esta investigación
														</p>
														<?php endif; ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 portfolio-stat">
											</div>
											<div class="col-md-2">
												<div class="portfolio-btn">
													<?php
														echo $this->Html->link('<span>Ver</span>', array('controller' => 'researches', 'action' => 'view', $research['Research']['id'], Format::clean($research['Research']['title'])), array('class' => 'btn bigicn-only', 'escape' => false));
													?>
												</div>
											</div>
											<div class="col-md-2">
												<div class="portfolio-btn">
													<?php
														echo $this->Html->link('<span>Editar</span>', array('controller' => 'researches', 'action' => 'edit', $research['Research']['id'], Format::clean($research['Research']['title'])), array('class' => 'btn bigicn-only', 'escape' => false));
													?>
												</div>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
								<!--end tab-pane-->
								<div class="tab-pane" id="contact">
									<div class="row">
										<div class="col-md-3">
											<ul class="list-unstyled profile-nav">
												<li>
													<?php
														$img = !empty($profile['User']['image']) ? 'images/user_img_'.$profile['User']['image'] : '/assets/img/img-holdit-edit-profile.png';
														echo $this->Html->image($img, array('class'=>'img-responsive'));
														echo $ownAccount ? $this->Html->link('editar', array('controller' => 'users', 'action'=>'edit', 'admin'=>true, $profile['User']['id'], Format::clean($profile['User']['fullname']), '#' => 'avatar'), array('class'=>'profile-edit')) : '';
													?>
												</li>
											</ul>
										</div>
										<div class="col-md-9">
											<form class="form-horizontal" role="form">
												<h2 class="margin-bottom-20"> Información de contacto</h2>
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label col-md-3">Correo electrónico:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	<?php echo !empty($profile['Profile']['contact_email']) ? $this->Text->autoLinkEmails($profile['Profile']['contact_email']) : '-'; ?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label class="control-label col-md-3">Teléfono(s):</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	<?php echo !empty($profile['Profile']['contact_phones']) ? $profile['Profile']['contact_phones'] : '-'; ?>
																</p>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->

												<?php if (isset($profile['Profile']) && !empty($profile['Profile']['curriculum_name'])) : ?>
												<h3 class="form-section"><i class="fa fa-download"></i> Descargar Curriculum Vitae</h3>
												<div class="row">
													<div class="col-md-12">
														<div class="well">
															<i class="fa fa-file-o"></i> <?php echo $this->Html->link($profile['Profile']['curriculum_name'], array('controller' => 'profiles', 'action' => 'downloadCV', $profile['Profile']['id'], 'admin' => false)) ?>
														</div>
													</div>
												</div>
												<?php endif; ?>

											</form>
										</div>
									</div>
								</div>
								<!--end tab-pane-->
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>