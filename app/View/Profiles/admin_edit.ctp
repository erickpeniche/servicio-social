<?php
	echo $this->Html->css(array(
		'plugins/bootstrap-fileinput/bootstrap-fileinput',
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/bootstrap-fileinput/bootstrap-fileinput',
		'plugins/ckeditor/ckeditor',
		'plugins/jquery-validation/dist/jquery.validate.min',
		'plugins/jquery-validation/localization/messages_es',
		'plugins/jquery-validation/dist/additional-methods.min'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Perfiles',
		array(
			'action' => 'index',
			'controller' => 'profiles',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Editar',
		array(
			'action' => 'edit',
			'controller' => 'profiles',
			'admin' => true,
			$profile['Profile']['id'],
			Format::clean($profile['User']['fullname'])
			)
		);

	$this->Html->addCrumb($profile['User']['fullname'], null);
?>

<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-pencil"></i>Editar Perfil</div>
				<div class="actions">
					<?php
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Perfiles', array('controller'=>'profiles', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon dark-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<?php
				echo $this->Form->create('Profile',
					array(
						'inputDefaults' => array(
							'div' => false,
							'label' => false,
							'class' => 'form-control',
							'error' => false
						),
						'class' => 'form-horizontal form-row-seperated',
						'enctype' => 'multipart/form-data',
						'novalidate' => true
					)
				);
			?>
				<div class="form-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						Existen errores en el formulario. Por favor verifica tus datos.
					</div>

					<?php echo $this->Form->hidden('id'); ?>

					<!-- Birth place -->
					<div class="form-group">
						<label class="control-label col-md-3">Lugar de nacimiento</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-flag"></i></span>
								<?php echo $this->Form->input('birth_place'); ?>
							</div>
						</div>
					</div>
					<!-- END Birth place -->

					<!-- Biography -->
					<div class="form-group">
						<label class="control-label col-md-3">Biografía</label>
						<div class="col-md-9">
							<?php echo $this->Form->textarea('biography', array('class' => 'ckeditor form-control', 'rows' => 6)); ?>
						</div>
					</div>
					<!-- END Biography -->

					<!-- Profession -->
					<div class="form-group">
						<label class="control-label col-md-3">Profesión</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-certificate"></i></span>
								<?php echo $this->Form->input('profession'); ?>
							</div>
						</div>
					</div>
					<!-- END Profession -->

					<!-- Ocupation -->
					<div class="form-group">
						<label class="control-label col-md-3">Ocupación actual</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-briefcase"></i></span>
								<?php echo $this->Form->input('occupation'); ?>
							</div>
						</div>
					</div>
					<!-- END Ocupation -->

					<!-- Work place -->
					<div class="form-group">
						<label class="control-label col-md-3">Lugar de trabajo actual</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-globe"></i></span>
								<?php echo $this->Form->input('workplace'); ?>
							</div>
						</div>
					</div>
					<!-- END Work place -->

					<!-- Education -->
					<div class="form-group">
						<label class="control-label col-md-3">Formación Académica</label>
						<div class="col-md-9">
							<?php echo $this->Form->textarea('education', array('class' => 'ckeditor form-control', 'rows' => 6)); ?>
						</div>
					</div>
					<!-- END Education -->

					<!-- Experience -->
					<div class="form-group">
						<label class="control-label col-md-3">Experiencia</label>
						<div class="col-md-9">
							<?php echo $this->Form->textarea('experience', array('class' => 'ckeditor form-control', 'rows' => 6)); ?>
						</div>
					</div>
					<!-- END Experience -->

					<!-- Laboral Areas -->
					<div class="form-group">
						<label class="control-label col-md-3">Áreas Laborales</label>
						<div class="col-md-9">
							<?php echo $this->Form->textarea('laboral_areas', array('class' => 'ckeditor form-control', 'rows' => 6)); ?>
						</div>
					</div>
					<!-- END Laboral Areas -->

					<!-- Courses -->
					<div class="form-group">
						<label class="control-label col-md-3">Cursos, diplomados y/o talleres</label>
						<div class="col-md-9">
							<?php echo $this->Form->textarea('courses', array('class' => 'ckeditor form-control', 'rows' => 6)); ?>
						</div>
					</div>
					<!-- END Courses -->

					<!-- Languages -->
					<div class="form-group">
						<label class="control-label col-md-3">Idiomas</label>
						<div class="col-md-9">
							<?php echo $this->Form->textarea('languages', array('class' => 'ckeditor form-control', 'rows' => 6)); ?>
						</div>
					</div>
					<!-- END Languages -->

					<!-- Interests -->
					<div class="form-group">
						<label class="control-label col-md-3">Intereses</label>
						<div class="col-md-9">
							<?php echo $this->Form->textarea('interests', array('class' => 'ckeditor form-control', 'rows' => 6)); ?>
						</div>
					</div>
					<!-- END Interests -->

					<!-- Contact email -->
					<div class="form-group">
						<label class="control-label col-md-3">Correo Electrónico de contacto</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<?php echo $this->Form->input('contact_email'); ?>
							</div>
						</div>
					</div>
					<!-- END Contact email -->

					<!-- Contact email -->
					<div class="form-group">
						<label class="control-label col-md-3">Teléfono(s) de contacto</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-phone"></i></span>
								<?php echo $this->Form->input('contact_phones'); ?>
							</div>
						</div>
					</div>
					<!-- END Contact email -->

					<!-- Curriculum upload -->
					<div class="form-group last">
						<label class="control-label col-md-3">Currículum Vitae</label>
						<div class="col-md-9">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="input-group input-large">
									<div class="form-control uneditable-input span3" data-trigger="fileinput">
										<i class="fa fa-file fileinput-exists"></i>&nbsp;
										<span class="fileinput-filename">
											<?php
												echo isset($profile['Profile']) && !empty($profile['Profile']['curriculum_name']) ? $profile['Profile']['curriculum_name'] : '';
											?>
										</span>
									</div>
									<span class="input-group-addon btn default btn-file">
										<span class="fileinput-new">
											 Seleccionar
										</span>
										<span class="fileinput-exists">
											 Cambiar
										</span>
										<?php
											echo $this->Form->file('curriculum');
										?>
									</span>
									<a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
										 Eliminar
									</a>
								</div>
							</div>
							<div class="clearfix margin-top-10">
								<span class="label label-danger"> NOTA!</span>
								<span>El archivo no debe ser mayor a <strong>10 MB</strong> y debe estar en formato <strong>DOC, PDF o TXT</strong>.</span>
							</div>
						</div>
					</div>
					<!-- END Curriculum upload -->
				</div>

				<div class="form-actions fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-offset-3 col-md-9">
								<?php
									echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
										array('type' => 'submit', 'class' => 'btn green')
										).'&nbsp;';

									$cancelOptions = array('action' => 'index', 'controller' => 'profiles', 'admin' => true);
									echo $this->Form->button('Cancelar',
										array(
											'type' => 'button',
											'class' => 'btn red',
											'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
											)
										);
								?>
							</div>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'FormValidation.init();',
		array('inline' => false)
	);
?>