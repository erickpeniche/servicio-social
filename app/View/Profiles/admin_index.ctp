<?php
	echo $this->Html->css(array(
		'plugins/select2/select2',
		'plugins/select2/select2-metronic',
		'plugins/data-tables/DT_bootstrap'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/select2/select2.min',
		'plugins/data-tables/jquery.dataTables',
		'plugins/data-tables/DT_bootstrap',
		'/js/profiles/admin_index'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Perfiles',
		array(
			'action' => 'index',
			'controller' => 'profiles',
			'admin' => true
			)
		);
?>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-suitcase"></i>Perfiles</div>
			</div>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="profiles_table">
						<thead>
							<tr>
								<th>Docente</th>
								<th>Última modificación</th>
								<th class="actions">Acciones</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($profiles as $profile): ?>
							<tr>
								<td><?php echo $profile['User']['fullname']; ?></td>
								<td><?php echo $this->Time->format($profile['Profile']['modified'], '%d/%m/%Y - %H:%M'); ?></td>
								<td class="actions">
									<div class="btn-group btn-group btn-group-justified">
										<?php
											echo $this->Html->link('<i class="fa fa-info-circle"></i> Ver', array('action' => 'view', $profile['Profile']['id'], Format::clean($profile['User']['fullname'])), array('class'=>'btn btn-sm btn-info', 'escape'=>false));
											echo $profile['Profile']['user_id'] == $loggedUser['id'] ? $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $profile['Profile']['id'], Format::clean($profile['User']['fullname'])), array('class'=>'btn btn-sm yellow', 'escape'=>false)) : '';
										?>
									</div>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'AdminIndex.init();',
		array('inline' => false)
	);
?>