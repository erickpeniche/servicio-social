<?php
echo $this->Html->css(array(
	'portal/css/pages/prices'
	), null, array('inline' => false)
);
?>
<!-- BEGIN BREADCRUMBS -->
<div class="row breadcrumbs margin-bottom-40">
	<div class="container">
		<div class="col-md-4 col-sm-4">
			<h1>Perfil de Investigador</h1>
		</div>
		<div class="col-md-8 col-sm-8">
			<ul class="pull-right breadcrumb">
				<li>
					<?php
						echo $this->Html->link('Inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => false));
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link('Investigadores', array('controller' => 'users', 'action' => 'index', 'admin' => false));
					?>
				</li>
				<li class="active"><?php echo $profile['User']['fullname']; ?></li>
			</ul>
		</div>
	</div>
</div>
<!-- END BREADCRUMBS -->

<!-- BEGIN CONTAINER -->
<div class="container min-hight">
	<!-- BEGIN SERVICE INFO -->
	<div class="row margin-bottom-40">
		<!-- TABS -->
		<div class="col-md-12 tab-style-1 margin-bottom-20">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#biography" data-toggle="tab">
						 Biografía
					</a>
				</li>
				<li>
					<a href="#education" data-toggle="tab">
						 Formación Académica
					</a>
				</li>
				<li>
					<a href="#profesion" data-toggle="tab">
						 Profesión
					</a>
				</li>
				<li>
					<a href="#projects" data-toggle="tab">
						 Proyectos
					</a>
				</li>
				<li>
					<a href="#publications" data-toggle="tab">
						 Publicaciones
					</a>
				</li>
				<li>
					<a href="#contact" data-toggle="tab">
						 Contacto
					</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane row fade in active" id="biography">
					<div class="col-md-3">
						<?php
							$img = !empty($profile['User']['image']) ? 'images/user_img_'.$profile['User']['image'] : '/assets/img/img-holdit-edit-profile.png';
							echo $this->Html->link($this->Html->image($img, array('class'=>'img-responsive')), $img, array('class'=>'fancybox-button', 'title' => $profile['User']['fullname'], 'data-rel' => 'fancybox-button', 'escape' => false));
						?>
					</div>
					<div class="col-md-9">
						<h3><?php echo $profile['User']['title'].' '.$profile['User']['fullname']; ?></h3>
						<ul class="list-inline">
							<?php if (isset($profile['Profile']['birth_place']) && !empty($profile['Profile']['birth_place'])) : ?>
							<li>
								<i class="fa fa-map-marker"></i> <?php echo $profile['Profile']['birth_place']; ?>
							</li>
							<?php endif; ?>
							<?php if (isset($profile['User']['birthday']) && !empty($profile['User']['birthday'])) : ?>
							<li>
								<i class="fa fa-calendar"></i> <?php echo $this->Time->format($profile['User']['birthday'], '%d de %B de %Y'); ?>
							</li>
							<?php endif; ?>
						</ul>
						<hr>
						<?php if (isset($profile['Profile']['biography']) && !empty($profile['Profile']['biography'])) : ?>
						<?php echo $profile['Profile']['biography']; ?>
						<?php endif; ?>
					</div>
				</div>
				<div class="tab-pane row fade" id="education">
					<div class="col-md-12">
						<?php if (isset($profile['Profile']['education']) && !empty($profile['Profile']['education'])) : ?>
						<?php echo $profile['Profile']['education']; ?>
						<?php endif; ?>
					</div>
				</div>
				<div class="tab-pane fade" id="profesion">
					<dl>
					<?php if (isset($profile['Profile']) && !empty($profile['Profile']['profession'])) : ?>
						<dt>Profesión</dt>
						<dd><?php echo $profile['Profile']['profession']; ?></dd>
					<?php endif; ?>
					<?php if (isset($profile['Profile']) && !empty($profile['Profile']['occupation'])) : ?>
						<dt>Ocupación</dt>
						<dd><?php echo $profile['Profile']['occupation']; ?></dd>
					<?php endif; ?>
					<?php if (isset($profile['Profile']) && !empty($profile['Profile']['workplace'])) : ?>
						<dt>Lugar de trabajo</dt>
						<dd><?php echo $profile['Profile']['workplace']; ?></dd>
					<?php endif; ?>
					</dl><br>

					<?php if (isset($profile['Profile']) && !empty($profile['Profile']['experience'])) : ?>
						<h4>Experiencia</h4><hr>
						<?php echo $profile['Profile']['experience']; ?><br>
					<?php endif; ?>

					<?php if (isset($profile['Profile']) && !empty($profile['Profile']['laboral_areas'])) : ?>
						<h4>Áreas laborales</h4><hr>
						<?php echo $profile['Profile']['laboral_areas']; ?><br>
					<?php endif; ?>

					<?php if (isset($profile['Profile']) && !empty($profile['Profile']['courses'])) : ?>
						<h4>Cursos, Diplomados y Talleres</h4><hr>
						<?php echo $profile['Profile']['courses']; ?><br>
					<?php endif; ?>

					<?php if (isset($profile['Profile']) && !empty($profile['Profile']['languages'])) : ?>
						<h4>Idiomas</h4><hr>
						<?php echo $profile['Profile']['languages']; ?><br>
					<?php endif; ?>

					<?php if (isset($profile['Profile']) && !empty($profile['Profile']['interests'])) : ?>
						<h4>Intereses</h4><hr>
						<?php echo $profile['Profile']['interests']; ?><br>
					<?php endif; ?>
				</div>
				<div class="tab-pane row fade" id="projects">
					<div class="col-md-12">
						<?php echo $this->element('portal/profile-projects'); ?>
					</div>
				</div>
				<div class="tab-pane row fade" id="publications">
					<div class="col-md-12">
						<?php if (isset($profile['User']['Publication']) && !empty($profile['User']['Publication'])) : ?>
							<ul class="nav sidebar-categories margin-bottom-40">
								<?php foreach ($profile['User']['Publication'] as $pub) : ?>
								<li><?php echo $this->Html->link($pub['publication_name'], array('controller' => 'publications', 'action' => 'view', $pub['id'], Format::clean($pub['title'])), array('escape' => false)); ?></li>
								<?php endforeach; ?>
							</ul>
						<?php else: ?>
							<p>No se encuentran el documento para esta publicación</p>
						<?php endif; ?>
					</div>
				</div>
				<div class="tab-pane row fade" id="contact">
					<?php if (isset($profile['Profile']) && !empty($profile['Profile']['contact_email'])) : ?>
					<address>
						<strong>Correo Electrónico</strong><br>
						<?php echo $this->Text->autoLinkEmails($profile['Profile']['contact_email']); ?>
					</address>
					<?php endif; ?>

					<?php if (isset($profile['Profile']) && !empty($profile['Profile']['contact_phones'])) : ?>
					<address>
						<strong>Teléfono(s) de Contacto</strong><br>
						<?php echo $profile['Profile']['contact_phones']; ?>
					</address>
					<?php endif; ?>

					<?php if (isset($profile['Profile']) && !empty($profile['Profile']['curriculum_name'])) : ?>
					<address>
						<i class="fa fa-download"></i> <strong>Descargar curriculum vitae</strong><br>
						<?php echo $this->Html->link($profile['Profile']['curriculum_name'], array('controller' => 'profiles', 'action' => 'downloadCV', 'admin' => false, $profile['Profile']['id'])); ?>
					</address>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<!-- END TABS -->
	</div>
	<!-- END SERVICE INFO -->
</div>
<!-- END CONTAINER -->