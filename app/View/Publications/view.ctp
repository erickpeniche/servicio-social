<?php
echo $this->Html->css(array(
	'portal/plugins/fancybox/source/jquery.fancybox',
	'portal/css/pages/portfolio',
	'portal/plugins/bxslider/jquery.bxslider',
	), null, array('inline' => false)
);

echo $this->Html->script(array(
	'portal/plugins/fancybox/source/jquery.fancybox.pack',
	'plugins/bxslider/jquery.bxslider.min',
	//'/js/projects/index'
	), array('inline' => false)
);
?>

<!-- BEGIN BREADCRUMBS -->
<div class="row breadcrumbs margin-bottom-40">
	<div class="container">
		<div class="col-md-4 col-sm-4">
			<h1>Publicación</h1>
		</div>
		<div class="col-md-8 col-sm-8">
			<ul class="pull-right breadcrumb">
				<li>
					<?php
						echo $this->Html->link('Inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => false));
					?>
				</li>
				<li>
					<?php
						echo $this->Html->link('Publicaciones', array('controller' => 'publications', 'action' => 'index', 'admin' => false));
					?>
				</li>
				<li class="active"><?php echo $publication['Publication']['title']; ?></li>
			</ul>
		</div>
	</div>
</div>
<!-- END BREADCRUMBS -->

<!-- BEGIN CONTAINER -->
<div class="container min-hight portfolio-page margin-bottom-30">
	<!-- BEGIN PORTFOLIO ITEM -->
	<div class="row">
		<!-- BEGIN PORTFOLIO DESCRIPTION -->
		<div class="col-md-12>">
			<h2><?php echo $publication['Publication']['title']; ?></h2><small>por <?php echo $publication['User']['title'].' '.$publication['User']['fullname']; ?></small><hr>
			<div class="row front-lists-v2 margin-bottom-15">
				<div class="col-md-12">
					<ul class="list-unstyled">
						<li><strong>Descripción:</strong> <?php echo !empty($publication['Publication']['description']) ? $publication['Publication']['description'] : '-'; ?></li>
						<li><strong>Fecha de publicación:</strong> <?php echo $this->Time->format($publication['Publication']['publication_date'], '%d/%m/%Y - %H:%M'); ?></li>
						<li><strong>Fecha de creación:</strong> <?php echo $this->Time->format($publication['Publication']['created'], '%d/%m/%Y - %H:%M'); ?></li>
						<li><strong>Última modificación:</strong> <?php echo $this->Time->format($publication['Publication']['modified'], '%d/%m/%Y - %H:%M'); ?></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- END PORTFOLIO DESCRIPTION -->
	</div>
	<!-- END PORTFOLIO ITEM -->

	<!-- BEGIN BLOCKQUOTE BLOCK -->
	<div class="row quote-v1">
		<div class="col-md-12 quote-v1-inner">
			<span>Documento</span>
		</div>
	</div>
	<!-- END BLOCKQUOTE BLOCK -->

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12">
			<?php if (isset($publication['Publication']) && !empty($publication['Publication']['publication_key'])) : ?>
				<ul class="nav sidebar-categories margin-bottom-40">
					<li><?php echo $this->Html->link('<i class="fa fa-download"></i> ' . $publication['Publication']['publication_name'], array('controller' => 'publications', 'action' => 'download', $publication['Publication']['id']), array('escape' => false)); ?></li>
				</ul>
			<?php else: ?>
				<p>No se encuentran el documento para esta publicación</p>
			<?php endif; ?>
		</div>
	</div>
	<!-- END RECENT WORKS -->
</div>
<!-- END CONTAINER -->