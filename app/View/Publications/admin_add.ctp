<?php
	echo $this->Html->css(array(
		'plugins/bootstrap-switch/css/bootstrap-switch.min',
		'plugins/bootstrap-fileinput/bootstrap-fileinput'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/bootstrap-fileinput/bootstrap-fileinput',
		'plugins/bootstrap-switch/js/bootstrap-switch.min',
		'plugins/jquery-validation/dist/jquery.validate.min',
		'plugins/jquery-validation/localization/messages_es',
		'plugins/jquery-validation/dist/additional-methods.min',
		'/js/publications/admin_add'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Publicaciones',
		array(
			'action' => 'index',
			'controller' => 'publications',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Agregar',
		array(
			'action' => 'add',
			'controller' => 'publications',
			'admin' => true
			)
		);
?>
<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-plus"></i>Nueva Publicación</div>
				<div class="actions">
					<?php
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Publicaciones', array('controller'=>'publications', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon green-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<?php
				echo $this->Form->create('Publication',
					array(
						'inputDefaults' => array(
							'div' => false,
							'label' => false,
							'class' => 'form-control',
							'error' => false
						),
						'class' => 'form-horizontal form-row-seperated',
						'enctype' => 'multipart/form-data',
						'novalidate' => true
					)
				);
			?>
				<div class="form-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						Existen errores en el formulario. Por favor verifica tus datos.
					</div>

					<!-- Title -->
					<div class="form-group <?php echo $this->Form->isFieldError('title') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Título<span class="required">*</span></label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-bookmark"></i></span>
								<?php echo $this->Form->input('title'); ?>
							</div>
							<?php echo $this->Form->isFieldError('title') ? $this->Form->error('title', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Title -->
					<!-- Description -->
					<div class="form-group">
						<label class="control-label col-md-3">Descripción</label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
								<?php echo $this->Form->input('description'); ?>
							</div>
						</div>
					</div>
					<!-- END Description -->
					<!-- File upload -->
					<div class="form-group <?php echo $this->Form->isFieldError('file') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Documento<span class="required">*</span></label>
						<div class="col-md-9">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="input-group input-large">
									<div class="form-control uneditable-input span3" data-trigger="fileinput">
										<i class="fa fa-file fileinput-exists"></i>&nbsp;
										<span class="fileinput-filename">
											<?php
												echo isset($publication['Publication']) && !empty($publication['Publication']['publication_name']) ? $publication['Publication']['publication_name'] : '';
											?>
										</span>
									</div>
									<span class="input-group-addon btn default btn-file">
										<span class="fileinput-new">
											 Seleccionar
										</span>
										<span class="fileinput-exists">
											 Cambiar
										</span>
										<?php
											echo $this->Form->file('file');
										?>
									</span>
									<a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
										 Eliminar
									</a>
								</div>
							</div>
							<?php echo $this->Form->isFieldError('file') ? $this->Form->error('file', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
							<div class="clearfix margin-top-10">
								<span class="label label-danger"> NOTA!</span>
								<span>El archivo no debe ser mayor a <strong>10 MB</strong> y debe estar en formato <strong>DOC, PDF o TXT</strong>.</span>
							</div>
						</div>
					</div>
					<!-- END File upload -->
					<!-- Public status -->
					<div class="form-group last">
						<label class="control-label col-md-3">Visible al público</label>
						<div class="col-md-9">
							<?php
								echo $this->Form->checkbox('public', array('type' => 'checkbox', 'class' => 'make-switch', 'checked' => true, 'data-on-label' => 'Sí', 'data-off-label' => 'No', 'data-on' => 'success', 'data-off' => 'danger'));
							?>
						</div>
					</div>
					<!-- END Public status -->
				</div>
				<div class="form-actions fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-offset-3 col-md-9">
								<?php
									echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
										array('type' => 'submit', 'class' => 'btn green')
										).'&nbsp;';

									$cancelOptions = array('action' => 'index', 'controller' => 'publications', 'admin' => true);
									echo $this->Form->button('Cancelar',
										array(
											'type' => 'button',
											'class' => 'btn red',
											'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
											)
										);
								?>
							</div>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'FormValidation.init();',
		array('inline' => false)
	);
?>
