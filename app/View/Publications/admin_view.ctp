<?php
	echo $this->Html->css(array(
		'plugins/fancybox/source/jquery.fancybox',
		'css/pages/profile',
		'css/pages/portfolio'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/jquery-mixitup/jquery.mixitup.min',
		'plugins/fancybox/source/jquery.fancybox.pack',
		'/js/publications/admin_view'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Publicaciones',
		array(
			'action' => 'index',
			'controller' => 'publications',
			'admin' => true
			)
		);

	$this->Html->addCrumb($publication['Publication']['title'],
		array(
			'action' => 'view',
			'controller' => 'publications',
			'admin' => true,
			$publication['Publication']['id'],
			Format::clean($publication['Publication']['title'])
			)
		);
?>

<div class="row profile">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption">&nbsp;</div>
				<div class="actions">
					<?php
						echo $ownAccount ? $this->Html->link('<i class="fa fa-pencil"></i> Editar', array('controller'=>'publications', 'action' => 'edit', 'admin'=>true, $publication['Publication']['id'], Format::clean($publication['Publication']['title'])), array('class'=>'btn default yellow-stripe', 'escape'=>false)).'&nbsp;' : '';
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Publicaciones', array('controller'=>'publications', 'action' => 'index', 'admin'=>true), array('class'=>'btn default m-icon green-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-8">
						<form class="form-horizontal" role="form">
							<h2 class="margin-bottom-20"> <?php echo $publication['Publication']['title']; ?></h2>
							<h3 class="form-section">Datos de la publicación</h3>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-4">Descripción:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo !empty($publication['Publication']['description']) ? $publication['Publication']['description'] : '-'; ?>
											</p>
										</div>
									</div>
								</div>
							</div>
							<!--/row-->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-4">Visibilidad:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo $publication['Publication']['public'] == 1 ? '<span class="label label-info">Publicada</span>' : '<span class="label label-default">No publicada</span>'; ?>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-4">Fecha de publicación:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo !empty($publication['Publication']['publication_date']) ? $this->Time->format($publication['Publication']['publication_date'], '%d/%m/%Y - %H:%M') : '-'; ?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-4">Fecha de creación:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo !empty($publication['Publication']['created']) ? $this->Time->format($publication['Publication']['created'], '%d/%m/%Y - %H:%M') : '-'; ?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-4">Última modificación:</label>
										<div class="col-md-8">
											<p class="form-control-static">
												<?php echo !empty($publication['Publication']['modified']) ? $this->Time->format($publication['Publication']['modified'], '%d/%m/%Y - %H:%M') : '-'; ?>
											</p>
										</div>
									</div>
								</div>
								<!--/span-->
							</div>
							<!--/row-->

							<h3 class="form-section">Documento</h3>
							<div class="row">
								<div class="col-md-8">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-advance table-hover">
											<thead>
												<tr>
													<th>
														Nombre
													</th>
													<th class="hidden-xs">
														Tamaño
													</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<i class="fa fa-download"></i>
														<?php
															echo $this->Html->link($publication['Publication']['publication_name'], array('controller' => 'publications', 'action' => 'download', $publication['Publication']['id'], 'admin' => false));
														?>
													</td>
													<td class="hidden-xs">
														<?php echo $this->Number->toReadableSize($publication['Publication']['publication_size']); ?>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
	echo $this->Html->scriptBlock(
		'AdminView.init();',
		array('inline' => false)
	);
?>