<?php
	echo $this->Html->css(array(
		'plugins/select2/select2',
		'plugins/select2/select2-metronic',
		'plugins/data-tables/DT_bootstrap'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/select2/select2.min',
		'plugins/data-tables/jquery.dataTables',
		'plugins/data-tables/DT_bootstrap',
		'/js/publications/admin_index'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Publicaciones',
		array(
			'action' => 'index',
			'controller' => 'publications',
			'admin' => true
		)
	);
?>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box grey">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-download"></i>Publicaciones</div>
				<div class="actions">
					<?php  echo $this->Html->link('<i class="fa fa-plus"></i> Agregar</a>',
						array(
							'action' => 'add',
							'controller' => 'publications'
						),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="publications_table">
						<thead>
							<th>Publicación</th>
							<th>Autor</th>
							<th>Fecha de publicación</th>
							<th>Última modificación</th>
							<th class="actions">Acciones</th>
						</thead>
						<tbody>
							<?php foreach ($publications as $publication): ?>
							<tr>
								<td>
									<?php echo isset($publication['Publication']) && !empty($publication['Publication']['title']) ? $publication['Publication']['title'] : '-'; ?>
								</td>
								<td>
									<?php echo isset($publication['User']) && !empty($publication['User']['fullname']) ? $this->Html->link($publication['User']['fullname'], array('controller' => 'users', 'action' => 'view', $publication['User']['id'], Format::clean($publication['User']['fullname']))) : '-'; ?>
								</td>
								<td>
									<?php echo !empty($publication['Publication']['publication_date']) ? $this->Time->format($publication['Publication']['publication_date'], '%d/%m/%Y - %H:%M') : '<span class="label label-warning">NO PUBLICADO</span>'; ?>
								</td>
								<td>
									<?php echo $this->Time->format($publication['Publication']['modified'], '%d/%m/%Y - %H:%M'); ?>
								</td>
								<td class="actions">
									<div class="btn-group btn-group btn-group-justified">
										<?php
											echo $this->Html->link('<i class="fa fa-info-circle"></i> Ver', array('action' => 'view', $publication['Publication']['id'], Format::clean($publication['Publication']['title'])), array('class'=>'btn btn-sm btn-info', 'escape'=>false));
											if ($publication['Publication']['user_id'] == $loggedUser['id']) :
												echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $publication['Publication']['id'], Format::clean($publication['User']['fullname'])), array('class'=>'btn btn-sm yellow', 'escape'=>false));
												echo $this->Html->link('<i class="fa fa-trash-o"></i> Eliminar', array('action' => 'delete', $publication['Publication']['id']), array('id' => 'delete-btn', 'class'=>'btn btn-sm red', 'escape' => false, 'data-id' => $publication['Publication']['id'], 'data-name' => $publication['Publication']['title']));

												$option = array('icon' => $publication['Publication']['main'] == 0 ? 'star-o':'star', 'title' => $publication['Publication']['main'] == 0 ? 'Destacar':'Quitar destacado');
												echo $this->Html->link('<i class="fa fa-'.$option['icon'].'"></i>', array('action' => 'toggle_main', $publication['Publication']['id']), array('class'=>'btn btn-sm default', 'escape' => false, 'title' => $option['title']));

												$option = array('icon' => $publication['Publication']['public'] == 0 ? 'eye-slash':'eye', 'title' => $publication['Publication']['public'] == 0 ? 'Marcar como pública':'Marcar como privada');
												echo $this->Html->link('<i class="fa fa-'.$option['icon'].'"></i>', array('action' => 'toggle_public', $publication['Publication']['id']), array('class'=>'btn btn-sm default', 'escape' => false, 'title' => $option['title']));
											endif;
											echo $this->Html->link('<i class="fa fa-download"></i>', array('action' => 'download', $publication['Publication']['id'], 'admin' => false), array('class'=>'btn btn-sm default', 'escape' => false, 'title' => 'Descargar'));
										?>
									</div>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'AdminIndex.init();',
		array('inline' => false)
	);
?>