<?php
echo $this->Html->script(array(
	'/js/researches/index'
	), array('inline' => false)
);
?>
<!-- BEGIN BREADCRUMBS -->
<div class="row breadcrumbs margin-bottom-40">
	<div class="container">
		<div class="col-md-4 col-sm-4">
			<h1>Publicaciones</h1>
		</div>
		<div class="col-md-8 col-sm-8">
			<ul class="pull-right breadcrumb">
				<li>
					<?php
					echo $this->Html->link('Inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => false));
					?>
				</li>
				<li class="active">Publicaciones</li>
			</ul>
		</div>
	</div>
</div>
<!-- END BREADCRUMBS -->

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN CONTAINER -->
		<div class="container min-hight portfolio-page margin-bottom-40">
			<div class="table-responsive">
				<table class="table table-hover">
				<thead>
				<tr>
					<th>Título</th>
					<th>Autor</th>
					<th>Fecha de publicación</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($publications as $pub) : ?>
				<tr>
					<td><?php echo $this->Html->link($pub['Publication']['title'], array('controller' => 'publications', 'action' => 'view', $pub['Publication']['id'], Format::clean($pub['Publication']['title']), 'admin' => false)); ?></td>
					<td><?php echo $this->Html->link($pub['User']['title'].' '.$pub['User']['fullname'], array('controller' => 'profiles', 'action' => 'view', $pub['User']['Profile']['id'])) ?></td>
					<td><?php echo $this->Time->format($pub['Publication']['publication_date'], '%d/%m/%Y - %H:%M'); ?></td>
				</tr>
				<?php endforeach; ?>
				</tbody>
				</table>
			</div>
		</div>
		<!-- END CONTAINER -->
	</div>
</div>

<?php
	echo $this->Html->scriptBlock(
		'Index.init();',
		array('inline' => false)
	);
?>