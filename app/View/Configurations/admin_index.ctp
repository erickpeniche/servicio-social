<?php
	echo $this->Html->css(array(
		'plugins/select2/select2',
		'plugins/select2/select2-metronic',
		'plugins/data-tables/DT_bootstrap'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/select2/select2.min',
		'plugins/data-tables/jquery.dataTables',
		'plugins/data-tables/DT_bootstrap',
		'/js/configurations/admin_index'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Configuraciones',
		array(
			'action' => 'index',
			'controller' => 'configurations',
			'admin' => true
			)
		);
?>
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<!-- BEGIN TAB PORTLET-->
		<div class="portlet box grey tabbable">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-cogs"></i>Configuraciones</div>
			</div>
			<div class="portlet-body">
				<div class=" portlet-tabs">
					<ul class="nav nav-tabs">
						<li><a href="#social" data-toggle="tab"><i class="fa fa-group"></i>&nbsp;Redes Sociales</a></li>
						<li><a href="#website" data-toggle="tab"><i class="fa fa-globe"></i>&nbsp;Datos del sitio web</a></li>
						<li class="active"><a href="#contacto" data-toggle="tab"><i class="fa fa-envelope"></i>&nbsp;Información de contacto</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="contacto">
							<?php echo $this->element('admin/configurations/contact-table');?>
						</div>
						<div class="tab-pane" id="website">
							<?php echo $this->element('admin/configurations/website-table');?>
						</div>
						<div class="tab-pane" id="social">
							<?php echo $this->element('admin/configurations/social-table');?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END TAB PORTLET-->
		<!-- END SAMPLE TABLE PORTLET-->
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'AdminIndex.init();',
		array('inline' => false)
	);
?>