<?php
	$categoryId = $this->params['pass'][0];
	$category = '';
	echo $this->Html->script(array(
		'plugins/jquery-validation/dist/jquery.validate.min',
		'plugins/jquery-validation/localization/messages_es',
		'plugins/jquery-validation/dist/additional-methods.min',
		'/js/configurations/admin_edit'
		), array('inline' => false)
	);

	if($isMap){
		echo $this->Html->script(array(
			'https://maps.googleapis.com/maps/api/js?key=' . $config['App']['services']['google-maps']['api-key'] . '&sensor=false',
			), array('inline' => false)
		);
	}

	switch ($categoryId) {
		case 1:
			$category = '#contacto';
			break;
		case 2:
			$category = '#website';
			break;
		case 3:
			$category = '#social';
			break;

	}

	$this->Html->addCrumb('Configuraciones',
		array(
			'action' => 'index'.$category,
			'controller' => 'configurations',
			'admin' => true
			)
		);

	$this->Html->addCrumb('Editar',
		array(
			'action' => 'edit',
			'controller' => 'configurations',
			'admin' => true,
			$categoryId,
			$configuration['Configuration']['id'],
			Format::clean($configuration['Configuration']['name'])
			)
		);

	$this->Html->addCrumb($configuration['Configuration']['name'], null);

	switch ($configuration['Configuration']['icon']) {
		case 'googleplus':
			$icon = 'google-plus';
			break;

		case 'wordpress':
			$icon = 'krw';
			break;

		default:
			$icon = $configuration['Configuration']['icon'];
			break;
	}
?>

<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-edit"></i>Editar Configuración</div>
				<div class="actions">
					<?php
						echo $this->Html->link('<i class="m-icon-swapleft"></i> Configuraciones', array('controller'=>'configurations', 'action' => 'index'.$category, 'admin'=>true), array('class'=>'btn default m-icon dark-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<?php
				echo $this->Form->create('Configuration',
					array(
						'inputDefaults' => array(
							'div' => false,
							'label' => false,
							'class' => 'form-control',
							'error' => false
						),
						'class' => 'form-horizontal form-row-seperated',
						'novalidate' => true
					)
				);

				echo $this->Form->hidden('id');
				echo $this->Form->hidden('category');
			?>
				<?php if($isMap): ?>
				<div id="map-canvas"></div>
				<div class="form-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-dismiss="alert"></button>
						Existen errores en el formulario. Por favor verifica tus datos.
					</div>
					<div class="alert alert-success display-hide">
						<button class="close" data-dismiss="alert"></button>
						Todos los datos están correctos!
					</div>
					<?php echo $this->Form->hidden('isMap', array('value'=>$isMap)); ?>
					<div class="form-group">
						<label class="control-label col-md-3">&nbsp;</label>
						<div class="col-md-9">
							<a class="btn dark" id="clean-map"><i class="icon-undo"></i> Limpiar</a>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Dirección aproximada<span class="required">*</span></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('directions', array('readonly'=>true)); ?>
						</div>
					</div>
					<?php echo $this->Form->hidden('value'); ?>
				</div>
				<?php else: ?>
				<div class="form-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						Existen errores en el formulario. Por favor verifica tus datos.
					</div>
					<!-- Value -->
					<div class="form-group <?php echo $this->Form->isFieldError('value') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3"><?php echo $configuration['Configuration']['name']; ?><span class="required">*</span></label>
						<div class="col-md-9">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-<?php echo $icon; ?>"></i></span>
								<?php echo $this->Form->input('value'); ?>
							</div>
							<?php echo $this->Form->isFieldError('value') ? $this->Form->error('name', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
						</div>
					</div>
					<!-- END Value -->
				</div>
				<?php endif; ?>
				<div class="form-actions fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-offset-3 col-md-9">
								<?php
									echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
										array('type' => 'submit', 'class' => 'btn green')
										).'&nbsp;';

									$cancelOptions = array('action' => 'index'.$category, 'controller' => 'configurations', 'admin' => true);
									echo $this->Form->button('Cancelar',
										array(
											'type' => 'button',
											'class' => 'btn red',
											'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
											)
										);
								?>
							</div>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'AdminEdit.init();',
		array('inline' => false)
	);
?>