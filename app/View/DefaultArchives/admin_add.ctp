<?php
	echo $this->Html->css(array(
		'plugins/bootstrap-fileinput/bootstrap-fileinput'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/bootstrap-fileinput/bootstrap-fileinput',
		'plugins/jquery-validation/dist/jquery.validate.min',
		'plugins/jquery-validation/localization/messages_es',
		'plugins/jquery-validation/dist/additional-methods.min',
		'/js/default_archives/admin_add'
		), array('inline' => false)
	);

	$this->Html->addCrumb('Galería de documentos', null);
	$this->Html->addCrumb(ucfirst($query['plural']), null);
	$this->Html->addCrumb($query['name'], null);
	$this->Html->addCrumb('Agregar', null);
?>
<div class="row">
	<div class="col-md-12">
		<div class="portlet">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-plus"></i>Nuevo Documento</div>
				<div class="actions">
					<?php
						echo $this->Html->link('<i class="m-icon-swapleft"></i> ' . ucfirst($query['singular']), array('controller'=>$query['controller'], 'action' => 'view', $query['foreign_key'], $query['slug'], 'admin'=>true), array('class'=>'btn default m-icon green-stripe', 'escape'=>false));
					?>
				</div>
			</div>
			<div class="portlet-body form">
			<!-- BEGIN FORM-->
			<?php
				echo $this->Form->create('DefaultArchive',
					array(
						'inputDefaults' => array(
							'div' => false,
							'label' => false,
							'class' => 'form-control',
							'error' => false
						),
						'class' => 'form-horizontal form-row-seperated',
						'enctype' => 'multipart/form-data',
						'novalidate' => true
					)
				);

				echo $this->Form->hidden('foreign_key', array('value' => $query['foreign_key']));
				echo $this->Form->hidden('model', array('value' => $query['model']));
				echo $this->Form->hidden('query', array('value' => http_build_query($query)));
			?>
				<div class="form-body">
					<div class="alert alert-danger display-hide">
						<button class="close" data-close="alert"></button>
						Existen errores en el formulario. Por favor verifica tus datos.
					</div>

					<!-- File upload -->
					<div class="form-group last <?php echo $this->Form->isFieldError('file') ? 'has-error' : ''; ?>">
						<label class="control-label col-md-3">Documento<span class="required">*</span></label>
						<div class="col-md-9">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="input-group input-large">
									<div class="form-control uneditable-input span3" data-trigger="fileinput">
										<i class="fa fa-file fileinput-exists"></i>&nbsp;
										<span class="fileinput-filename">
										</span>
									</div>
									<span class="input-group-addon btn default btn-file">
										<span class="fileinput-new">
											 Seleccionar
										</span>
										<span class="fileinput-exists">
											 Cambiar
										</span>
										<?php
											echo $this->Form->file('file');
										?>
									</span>
									<a href="#" class="input-group-addon btn default fileinput-exists" data-dismiss="fileinput">
										 Eliminar
									</a>
								</div>
								<?php echo $this->Form->isFieldError('file') ? $this->Form->error('file', null, array('wrap'=>'span', 'class'=>'help-block')) : ''; ?>
							</div>
							<div class="clearfix margin-top-10">
								<span class="label label-danger"> NOTA!</span>
								<span>El archivo no debe ser mayor a <strong>20 MB</strong>.</span>
							</div>
						</div>
					</div>
					<!-- END File upload -->
				</div>
				<div class="form-actions fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-offset-3 col-md-9">
								<?php
									echo $this->Form->button('<i class="fa fa-check"></i> Guardar',
										array('type' => 'submit', 'class' => 'btn green')
										).'&nbsp;';

									$cancelOptions = array('controller'=>$query['controller'], 'action' => 'view', $query['foreign_key'], $query['slug'], 'admin'=>true);
									echo $this->Form->button('Cancelar',
										array(
											'type' => 'button',
											'class' => 'btn red',
											'onclick' => "location.href='".$this->Html->url($cancelOptions)."'"
											)
										);
								?>
							</div>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
			</div>
		</div>
	</div>
</div>
<?php
	echo $this->Html->scriptBlock(
		'FormValidation.init();',
		array('inline' => false)
	);
?>
