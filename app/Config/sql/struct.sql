DROP TABLE IF EXISTS `configurations`;

CREATE TABLE `configurations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `category` tinyint(10) NOT NULL,
  `is_map` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `configurations` (`id`, `key`, `value`, `name`, `icon`, `category`, `is_map`, `created`, `modified`)
VALUES
	(1,'email','','Correo(s) de contacto','envelope',1,0,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(2,'address','','Dirección','road',1,0,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(3,'phone','','Teléfono','phone',1,0,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(4,'map','','Ubicación en Google Maps','map-marker',1,1,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(5,'title','','Título del sitio','pencil',2,0,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(6,'keywords','','Palabras clave de búsqueda','search',2,0,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(7,'description','','Descripción del sitio','edit',2,0,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(8,'twitter','','Twitter','twitter',3,0,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(9,'facebook','','Facebook','facebook',3,0,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(10,'youtube','','YouTube','youtube',3,0,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(11,'googleplus','','Google+','googleplus',3,0,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(12,'linkedin','','LinkedIn','linkedin',3,0,'2014-01-20 11:05:50','2014-01-20 11:05:50'),
	(13,'wordpress','','Wordpress','wordpress',3,0,'2014-01-20 11:05:50','2014-01-20 11:05:50');

DROP TABLE IF EXISTS `default_images`;

CREATE TABLE IF NOT EXISTS `default_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) NOT NULL,
  `description` varchar(255) NULL,
  `size` int(11) NOT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `default_archives`;

CREATE TABLE IF NOT EXISTS `default_archives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `main_images`;

CREATE TABLE IF NOT EXISTS `main_images` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `description` VARCHAR(255) NOT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `link` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NOT NULL ,
  `modified` DATETIME NOT NULL ,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) NULL,
  `topic` varchar(255) NULL,
  `responsable` varchar(255) NULL,
  `place` varchar(255) NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` text,
  `coworkers` text,
  `public` tinyint(1) DEFAULT '0',
  `main` tinyint(1) DEFAULT '0',
  `working` tinyint(1) DEFAULT '0',
  `created` datetime NULL,
  `modified` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `researches`;

CREATE TABLE `researches` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) NULL,
  `topic` varchar(255) NULL,
  `responsable` varchar(255) NULL,
  `place` varchar(255) NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` text,
  `coworkers` text,
  `public` tinyint(1) DEFAULT '0',
  `main` tinyint(1) DEFAULT '0',
  `working` tinyint(1) DEFAULT '0',
  `created` datetime NULL,
  `modified` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `publications`;

CREATE TABLE `publications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) NULL,
  `description` varchar(255) NULL,
  `publication_key` varchar(255) NULL,
  `publication_name` varchar(255) NULL,
  `publication_size` int(20) DEFAULT NULL,
  `public` tinyint(1) DEFAULT '0',
  `publication_date` datetime NULL,
  `main` tinyint(1) DEFAULT '0',
  `created` datetime NULL,
  `modified` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `profiles`;

CREATE TABLE `profiles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `birth_place` varchar(255) NULL,
  `biography` text,
  `profession` varchar(255) NULL,
  `occupation` varchar(255) NULL,
  `workplace` varchar(255) NULL,
  `education` text,
  `experience` text,
  `laboral_areas` text,
  `courses` text,
  `languages` text,
  `interests` text,
  `contact_email` varchar(255) NULL,
  `contact_phones` varchar(255) NULL,
  `curriculum_key` varchar(255) NULL,
  `curriculum_name` varchar(255) NULL,
  `curriculum_size` int(20) DEFAULT NULL,
  `created` datetime NULL,
  `modified` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `posts`;

CREATE TABLE IF NOT EXISTS `posts` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `user_id` INT(11) NOT NULL,
  `title` VARCHAR(255) NOT NULL ,
  `content` TEXT NOT NULL ,
  `image` VARCHAR(255) NULL DEFAULT NULL ,
  `tags` VARCHAR(455) NULL DEFAULT NULL ,
  `published` TINYINT(1) NOT NULL ,
  `published_date` datetime NULL DEFAULT NULL ,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `comments`;

CREATE TABLE IF NOT EXISTS `comments` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `post_id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL ,
  `email` VARCHAR(255) NULL DEFAULT NULL ,
  `content` TEXT NOT NULL ,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `paternal_surname` VARCHAR(255) NOT NULL,
  `maternal_surname` VARCHAR(255) NOT NULL,
  `birthday` date DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `rfc` varchar(255) DEFAULT NULL,
  `curp` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `role` tinyint(10) DEFAULT '1',
  `active` tinyint(1) DEFAULT '0',
  `reset_password_code` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `email`, `password`, `title`, `name`, `paternal_surname`, `maternal_surname`, `birthday`, `telephone`, `rfc`, `curp`, `image`, `role`, `active`, `reset_password_code`, `created`, `modified`)
VALUES
	(1,'erickpeniche@gmail.com','af9558431ea5f40a25bd060254bd6360dd355b98',NULL,'Erick','Peniche','Castro','1991-04-23','9993386721','PECE910423AV5','PECE910423HYNNSR08',NULL,1,1,NULL,'2014-01-20 11:05:50','2014-01-20 11:05:50');

