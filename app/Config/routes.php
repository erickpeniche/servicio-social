<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'home'));

/**
 * Admin login
 */
	Router::connect('/admin', array('controller' => 'users', 'action' => 'login', 'admin' => true));
	Router::connect('/admin/logout', array('controller' => 'users', 'action' => 'logout', 'admin' => true));
	Router::connect('/admin/inicio', array('controller' => 'pages', 'action' => 'home', 'admin' => true));

/**
 * Site routes
 */
	Router::connect('/olvide-contrasena', array('controller' => 'users', 'action' => 'forget_password'));
	Router::connect('/reestablecer-acceso/*', array('controller' => 'users', 'action' => 'change_password'));
	Router::connect('/validar-email', array('controller' => 'users', 'action' => 'validate_email'));

/**
 * Admin Projects
 */
	Router::connect('/admin/proyectos', array('controller' => 'projects', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/proyecto/agregar', array('controller' => 'projects', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/proyecto/ver/*', array('controller' => 'projects', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/proyecto/editar/*', array('controller' => 'projects', 'action' => 'edit', 'admin' => true));

/**
 * Admin Publications
 */
	Router::connect('/admin/publicaciones', array('controller' => 'publications', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/publicacion/agregar', array('controller' => 'publications', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/publicacion/ver/*', array('controller' => 'publications', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/publicacion/editar/*', array('controller' => 'publications', 'action' => 'edit', 'admin' => true));

/**
 * Admin Profiles
 */
	Router::connect('/admin/perfiles', array('controller' => 'profiles', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/perfil/ver/*', array('controller' => 'profiles', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/perfil/editar/*', array('controller' => 'profiles', 'action' => 'edit', 'admin' => true));

/**
 * Admin Users
 */
	Router::connect('/admin/usuarios', array('controller' => 'users', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/usuario/nuevo', array('controller' => 'users', 'action' => 'add', 'admin' => true));
	Router::connect('/admin/usuario/ver/*', array('controller' => 'users', 'action' => 'view', 'admin' => true));
	Router::connect('/admin/usuario/editar/*', array('controller' => 'users', 'action' => 'edit', 'admin' => true));
	Router::connect('/admin/usuario/eliminar/*', array('controller' => 'users', 'action' => 'delete', 'admin' => true));

/**
 * Admin Configurations
 */
	Router::connect('/admin/configuraciones', array('controller' => 'configurations', 'action' => 'index', 'admin' => true));
	Router::connect('/admin/configuracion/editar/*', array('controller' => 'configurations', 'action' => 'edit', 'admin' => true));


	Router::connect('/investigadores', array('controller' => 'users', 'action' => 'index', 'admin' => false));
	Router::connect('/proyectos', array('controller' => 'projects', 'action' => 'index', 'admin' => false));
	Router::connect('/proyecto/ver/*', array('controller' => 'projects', 'action' => 'view', 'admin' => false));
	Router::connect('/publicaciones', array('controller' => 'publications', 'action' => 'index', 'admin' => false));
	Router::connect('/publicacion/ver/*', array('controller' => 'publications', 'action' => 'view', 'admin' => false));
	Router::connect('/blog', array('controller' => 'posts', 'action' => 'index', 'admin' => false));

	Router::connect('/investigador/*', array('controller' => 'profiles', 'action' => 'view', 'admin' => false));
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
