<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $locale = null;

	public $components = array(
		'Session',
		'Cookie',
		'Auth' => array(
			'loginAction' => array(
				'controller' => 'users',
				'action' => 'login',
				'admin' => true
			),
			'authError' => 'Debes iniciar sesión para ver este contenido',
			'loginRedirect' => array('controller' => 'pages', 'action' => 'home', 'admin' => true),
			'logoutRedirect' => array('controller' => 'pages', 'action' => 'home', 'admin' => false),
			'authorize' => array('Controller'),
			'authenticate' => array(
				'Form' => array(
					'fields' => array('username' => 'email'),
					'scope' => array('active' => 1)
				)
			)
		),
		'RequestHandler'
	);

	public $helpers = array('Js' => array('Jquery'), 'Number', 'Text', 'Time');

	public function beforeFilter() {
		$config = Configure::read();
		$clientIP = $this->request->clientIp();

		$this->Auth->allow('home', 'index', 'view', 'contact', 'forget_password', 'add', 'sitemap', 'email', 'change_password');

		// Localization
		$this->locale = $config['App']['locale'];
		Configure::write('Config.language', $this->locale);

		// DB Configurations
		$this->loadModel('Configuration');
		$configurations = $this->Configuration->find('list', array(
			'fields' => array('key', 'value')
		));
		Configure::write('App.configurations', $configurations);

		$config = Configure::read();

		$this->loadModel('User');
		$sessionUser = $this->User->findById($this->Session->read('Auth.User.id'));

		$this->set(array(
			'config' => $config,
			'locale' => $this->locale
		));
		$this->set(compact('sessionUser'));
	}

	public function isAuthorized($user) {
		return true;
	}

	public function isSuperUser() {
		if ($this->Session->read('Auth.User')) {
			if($this->Session->read('Auth.User.role')==1){
				return true;
			}
		}

		return false;
	}
}
