<?php
App::uses('AppController', 'Controller');
/**
 * MainImages Controller
 *
 * @property MainImage $MainImage
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MainImagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->layout = 'admin/default';

		$this->set('mainImages', $this->MainImage->find('all'));
		$this->set('activeMenu', 'main_images');
	}

	public function admin_add() {
		$this->layout = 'admin/default';

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->MainImage->set($this->request->data);

			if($this->MainImage->validates()){
				$setup = $this->MainImage->images;

				$image = null;
				$display = null;
				$view = null;

				$this->MainImage->create();

				//File upload
				$file = $this->data['MainImage']['image'];
				$fileDir = WWW_ROOT . 'img' . DS . 'images'. DS;
				$fileName = '';
				$fileExtension = '';

				//A file was uploaded
				if (!(empty($file['tmp_name']) && $file['error'] == 4)) {

					//There is no error, the file was uploaded with success
					if ($file['error'] == 0) {

						//Tells whether the file was uploaded via HTTP POST
						if (is_uploaded_file($file['tmp_name'])) {
							App::import('Vendor', 'Bitmap');
							App::import('Vendor', 'BitmapException');

							if (Bitmap::isImage($file['tmp_name'])) {

								try {
									$fileName = md5($file['tmp_name'] . (mt_rand() * 100000));
									$imageSize = getimagesize($file['tmp_name']);
									$width = $imageSize[0];
									$height = $imageSize[1];

									foreach($setup as $key => $set) {
										$img = new Bitmap($file['tmp_name']);
										$img->open();
										switch ($set['mode']) {
											case 'resizeToWidth':
												$img->resizeToWidth($set['width']);
												break;
											case 'resizeToHeight':
												$img->resizeToHeight($set['height']);
												break;
											case 'resizeToWidthHeight':
												$img->resizeToWidthHeight($set['width'], $set['height']);
												break;
											case 'crop':
												if ($set['align'] == 'left') {
													$x1 = 0;
													$x2 = $set['width'];
												} else if ($set['align'] == 'right'){
													$x1 = $width - $set['width'];
													$x2 = $width;
												} else {
													$x1 = ($width / 2) - ($set['width'] / 2);
													$x2 = ($width / 2) + ($set['width'] / 2);
												}

												if ($set['valign'] == 'top') {
													$y1 = 0;
													$y2 = $set['height'];
												} else if ($set['valign'] == 'bottom'){
													$y1 = $height - $set['height'];
													$y2 = $height;
												} else {
													$y1 = ($height / 2) - ($set['height'] / 2);
													$y2 = ($height / 2) + ($set['height'] / 2);
												}

												$img->crop($x1, $y1, $x2, $y2);
												break;
											case 'resizeAndCrop':
												$aspectRatio = $height / $width;
												if ($aspectRatio < ($set['height'] / $set['width']))
												{
													$img->resizeToHeight($set['height']);
												}
												else
												{
													$img->resizeToWidth($set['width']);
												}

												if ($set['align'] == 'left') {
													$x1 = 0;
													$x2 = $set['width'];
												} else if ($set['align'] == 'right'){
													$x1 = $img->getWidth() - $set['width'];
													$x2 = $img->getWidth();
												} else {
													$x1 = ($img->getWidth() / 2) - ($set['width'] / 2);
													$x2 = ($img->getWidth() / 2) + ($set['width'] / 2);
												}

												if ($set['valign'] == 'top') {
													$y1 = 0;
													$y2 = $set['height'];
												} else if ($set['valign'] == 'bottom'){
													$y1 = $img->getHeight() - $set['height'];
													$y2 = $img->getHeight();
												} else {
													$y1 = ($img->getHeight() / 2) - ($set['height'] / 2);
													$y2 = ($img->getHeight() / 2) + ($set['height'] / 2);
												}

												$img->crop($x1, $y1, $x2, $y2);

												break;
											default :
												break;
										}

										if(isset($set['thumbnail']) && !empty($set['thumbnail'])){
											$display = $key;
										}

										if(isset($set['view']) && !empty($set['view'])) {
											$view = $key;
										}

										if (isset($set['effect']) && $set['effect']) {
											$img->{$set['effect']}();
										}

										if (isset($set['mask']) && $set['mask']) {
											$mask = new Bitmap(WWW_ROOT . 'img' . DS . 'filters' . DS . $set['mask']);
											$mask->open();
											$img->mask($mask);
											$mask->dispose();
										}

										if (isset($set['ext']) && $set['ext']) {
											$img->changeType($set['ext']);
										}

										if (isset($set['quality']) && $set['quality']) {
											$quality = $set['quality'];
										} else {
											$quality = 100;
										}

										$img->save($fileDir, $key . '_' . $fileName, $quality);
										$img->dispose();
										$fileExtension = $img->getExtension();
									}

									if (empty($display)) {
										$display = $key;
									}
									if (empty($view)) {
										$view = $key;
									}

									$this->request->data['MainImage']['image'] = $fileName . '.' .  $fileExtension;
								} catch (BitmapException $e) {
									$this->MainImage->invalidate('file', 'bitmapError');
								}
							} else {
								$this->MainImage->invalidate('file', 'unsupportedType');
							}
						} else {
							$this->MainImage->invalidate('file', 'maliciousUpload');
						}
					} else {
						//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
						$this->MainImage->invalidate('file', 'uploadError');
					}
				}

				if ($this->MainImage->save($this->request->data, false)) {
					$this->Session->setFlash(__('La imagen de portada ha sido creada.'), 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
					return $this->redirect(array('action' => 'index'));
				} else {
					//Deletes copied images
					if (!empty($fileName) && !array_key_exists('image', $this->MainImage->invalidFields())) {
						foreach($setup as $key => $set) {
							unlink($fileDir . DS . $key . '_' . $fileName . '.' . $fileExtension);
						}
					}
					$this->Session->setFlash(__('La imagen de portada no se pudo crear.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error')));
				}
			}else{
				$this->Session->setFlash('No se ha podido agregar la imagen', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			}
		}
		$this->set('activeMenu', 'main_images');
	}

	public function admin_delete($id = null) {
		$this->MainImage->id = $id;
		if (!$this->MainImage->exists()) {
			throw new NotFoundException(__('Invalid main image'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MainImage->delete()) {
			$this->Session->setFlash(__('La portada ha sido eliminada correctamente.'), 'admin/flash/toastr', array('title'=>'Atención!', 'type'=>'warning'));
		} else {
			$this->Session->setFlash(__('No se pudo eliminar la portada.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
