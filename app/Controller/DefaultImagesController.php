<?php
App::uses('AppController', 'Controller');
/**
 * DefaultImages Controller
 *
 * @property DefaultImage $DefaultImage
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class DefaultImagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function admin_add() {
		$this->layout = 'admin/default';

		$query = $this->request->query;

		if ($this->request->is('post') || $this->request->is('put')) {
			parse_str($this->request->data['DefaultImage']['query'], $query);
			unset($this->request->data['DefaultImage']['query']);

			$model = ucfirst($this->request->data['DefaultImage']['model']);
			$foreignKey = $this->request->data['DefaultImage']['foreign_key'];

			$this->DefaultImage->set($this->request->data);

			if($this->DefaultImage->validates()){
				App::import('model', $model);
				$this->{$model} = new $model();
				$mod = $this->{$model}->find('first', array(
					'conditions' => array($model . '.id' => $foreignKey),
					'contain' => false
				));

				$setup = $this->{$model}->images;

				$image = null;
				$display = null;
				$view = null;

				if ($mod) {
					$this->DefaultImage->create();

					//File upload
					$file = $this->data['DefaultImage']['file'];
					$fileDir = WWW_ROOT . 'img' . DS . 'images'. DS;
					$fileName = '';
					$fileExtension = '';

					//A file was uploaded
					if (!(empty($file['tmp_name']) && $file['error'] == 4)) {

						//There is no error, the file was uploaded with success
						if ($file['error'] == 0) {

							//Tells whether the file was uploaded via HTTP POST
							if (is_uploaded_file($file['tmp_name'])) {
								App::import('Vendor', 'Bitmap');
								App::import('Vendor', 'BitmapException');

								if (Bitmap::isImage($file['tmp_name'])) {

									try {
										$fileName = md5($file['tmp_name'] . (mt_rand() * 100000));
										$imageSize = getimagesize($file['tmp_name']);
										$width = $imageSize[0];
										$height = $imageSize[1];

										foreach($setup as $key => $set) {
											$img = new Bitmap($file['tmp_name']);
											$img->open();
											switch ($set['mode']) {
												case 'resizeToWidth':
													$img->resizeToWidth($set['width']);
													break;
												case 'resizeToHeight':
													$img->resizeToHeight($set['height']);
													break;
												case 'resizeToWidthHeight':
													$img->resizeToWidthHeight($set['width'], $set['height']);
													break;
												case 'crop':
													if ($set['align'] == 'left') {
														$x1 = 0;
														$x2 = $set['width'];
													} else if ($set['align'] == 'right'){
														$x1 = $width - $set['width'];
														$x2 = $width;
													} else {
														$x1 = ($width / 2) - ($set['width'] / 2);
														$x2 = ($width / 2) + ($set['width'] / 2);
													}

													if ($set['valign'] == 'top') {
														$y1 = 0;
														$y2 = $set['height'];
													} else if ($set['valign'] == 'bottom'){
														$y1 = $height - $set['height'];
														$y2 = $height;
													} else {
														$y1 = ($height / 2) - ($set['height'] / 2);
														$y2 = ($height / 2) + ($set['height'] / 2);
													}

													$img->crop($x1, $y1, $x2, $y2);
													break;
												case 'resizeAndCrop':
													$aspectRatio = $height / $width;
													if ($aspectRatio < ($set['height'] / $set['width']))
													{
														$img->resizeToHeight($set['height']);
													}
													else
													{
														$img->resizeToWidth($set['width']);
													}

													if ($set['align'] == 'left') {
														$x1 = 0;
														$x2 = $set['width'];
													} else if ($set['align'] == 'right'){
														$x1 = $img->getWidth() - $set['width'];
														$x2 = $img->getWidth();
													} else {
														$x1 = ($img->getWidth() / 2) - ($set['width'] / 2);
														$x2 = ($img->getWidth() / 2) + ($set['width'] / 2);
													}

													if ($set['valign'] == 'top') {
														$y1 = 0;
														$y2 = $set['height'];
													} else if ($set['valign'] == 'bottom'){
														$y1 = $img->getHeight() - $set['height'];
														$y2 = $img->getHeight();
													} else {
														$y1 = ($img->getHeight() / 2) - ($set['height'] / 2);
														$y2 = ($img->getHeight() / 2) + ($set['height'] / 2);
													}

													$img->crop($x1, $y1, $x2, $y2);

													break;
												default :
													break;
											}

											if(isset($set['thumbnail']) && !empty($set['thumbnail'])){
												$display = $key;
											}

											if(isset($set['view']) && !empty($set['view'])) {
												$view = $key;
											}

											if (isset($set['effect']) && $set['effect']) {
												$img->{$set['effect']}();
											}

											if (isset($set['mask']) && $set['mask']) {
												$mask = new Bitmap(WWW_ROOT . 'img' . DS . 'filters' . DS . $set['mask']);
												$mask->open();
												$img->mask($mask);
												$mask->dispose();
											}

											if (isset($set['ext']) && $set['ext']) {
												$img->changeType($set['ext']);
											}

											if (isset($set['quality']) && $set['quality']) {
												$quality = $set['quality'];
											} else {
												$quality = 100;
											}

											$img->save($fileDir, $key . '_' . $fileName, $quality);
											$img->dispose();
											$fileExtension = $img->getExtension();
										}

										if (empty($display)) {
											$display = $key;
										}
										if (empty($view)) {
											$view = $key;
										}

										$this->request->data['DefaultImage']['file'] = $fileName . '.' .  $fileExtension;
										$this->request->data['DefaultImage']['size'] = $file['size'];

									} catch (BitmapException $e) {
										$this->DefaultImage->invalidate('file', 'bitmapError');
									}
								} else {
									$this->DefaultImage->invalidate('file', 'unsupportedType');
								}
							} else {
								$this->DefaultImage->invalidate('file', 'maliciousUpload');
							}
						} else {
							//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
							$this->DefaultImage->invalidate('file', 'uploadError');
						}
					}

					if ($this->DefaultImage->save($this->request->data, false)) {
						$this->Session->setFlash('Se ha agregado la imagen correctamente', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
						$this->redirect(array('controller' => $query['controller'] , 'action' => 'view', $query['foreign_key'], $query['slug'], 'admin' => true));
					} else {
						//Deletes copied images
						if (!empty($fileName) && !array_key_exists('image', $this->DefaultImage->invalidFields())) {
							foreach($setup as $key => $set) {
								unlink($fileDir . DS . $key . '_' . $fileName . '.' . $fileExtension);
							}
						}
						$this->Session->setFlash('No se ha podido agregar la imagen', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
					}
				}
			}else{
				$this->Session->setFlash('No se ha podido agregar la imagen', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			}
		}

		$this->set(compact('query'));
		$this->set('activeMenu', $query['controller']);
	}

	public function admin_delete($id = null) {
		$query = $this->request->query;

		$this->DefaultImage->id = $id;
		if (!$this->DefaultImage->exists()) {
			throw new NotFoundException(__('Invalid default image'));
		}
		$loggedUser = $this->Session->read('Auth.User');
		$image = $this->DefaultImage->findById($id);
		$ownAccount = $image[$query['model']]['user_id'] == $loggedUser['id'] ? true : false;

		if (!$ownAccount) {
			$this->Session->setFlash(__('No puedes realizar esta acción.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error')));
			return $this->redirect(array('controller' => '', 'action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ($this->DefaultImage->delete()) {
			$this->Session->setFlash('La imagen ha sido eliminada correctamente', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
			$this->redirect(array('controller' => $query['controller'], 'action' => 'view', $query['foreign_key'], $query['slug']));
		}
		$this->Session->setFlash('No se pudo eliminar la imagen', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
		$this->redirect(array('controller' => $query['controller'], 'action' => 'view', $query['foreign_key'], $query['slug']));
	}

	public function isSuperUser($user) {
		if (isset($user['role']) && $user['role'] === 'Super User') {
			return true;
		}
		return false;
	}

}
