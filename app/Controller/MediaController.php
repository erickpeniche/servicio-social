<?php
App::uses('AppController', 'Controller');
/**
 * Media Controller
 *
 * @property Media $Media
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class MediaController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function admin_add($model = null, $id = null, $name = null) {
		$this->layout = 'admin/default';

		if (!$this->Media->$model->exists($id)) {
			$this->Session->setFlash('El $model no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'home', 'controller' => 'pages', 'admin' => true));
		}

		$data = $this->Media->$model->findById($id);

		if ($this->request->is('post')) {
			$this->request->data['Media']['foreign_key'] = $id;
			$this->request->data['Media']['model'] = $model;
			$this->Media->create();
			if ($this->Media->save($this->request->data)) {
				$this->Session->setFlash(__('El media <strong>' . $this->request->data['Media']['name'] . '</strong> ha sido creado.'), 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El media no se pudo crear.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error')));
			}
		}

		$this->set('data', $this->Media->$model->findById($id));
	}

	public function admin_edit($id = null, $name = null) {
		$this->layout = 'admin/default';
		$viewTitle = 'Editar';
		$viewSubTitle = 'Media';

		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		if (!$this->Media->exists($id)) {
			$this->Session->setFlash('El media no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Media->save($this->request->data)) {
				$this->Session->setFlash(__('El media ha sido editado.'), 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'view', $id, Format::clean($media['Media']['name'])));
			} else {
				$this->Session->setFlash(__('El media no se pudo editar.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			}
		} else {
			$options = array('conditions' => array('Media.' . $this->Media->primaryKey => $id));
			$this->request->data = $this->Media->find('first', $options);
		}
		$projects = $this->Media->Project->find('list');
		$this->set(compact('projects'));
		$media = $this->request->data;
		$this->set(compact('viewTitle', 'viewSubTitle', 'media'));
		$this->set('activeMenu', 'media');
	}

	public function admin_delete($id = null) {
		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		if ($this->Session->read('Auth.User.id')==$id) {
			$this->Session->setFlash('No puedes realizar esta accion. (No te eliminarías a ti mismo o si?)', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			$this->redirect(array('action' => 'index', 'admin' => true));
		}*/

		$this->Media->id = $id;
		if (!$this->Media->exists()) {
			throw new NotFoundException(__('Invalid media'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Media->delete()) {
			$this->Session->setFlash(__('The media eliminado correctamente.'), 'admin/flash/toastr', array('title'=>'Atención!', 'type'=>'warning'));
		} else {
			$this->Session->setFlash(__('No se pudo eliminar el media.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function beforeSave() $return = false;
		$image = null;
		$erase = false;
		$media = null;

		if (!empty($this->id)) {
			$media = $this->findById($this->data['Media']['id']);
			$image = $media['Media']['key']. '.' . $media['Media']['format'];
		}

		//File upload
		$file = isset($this->data['Media']['file']) ? $this->data['Media']['file'] : null;
		$fileDir = WWW_ROOT . 'files' . DS . 'media';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					App::import('Vendor', 'Bitmap');
					App::import('Vendor', 'BitmapException');

					if (Bitmap::isImage($file['tmp_name'])) {

						try {
							$fileName = md5($file['tmp_name'] . (rand() * 100000));
							$imageSize = getimagesize($file['tmp_name']);
							$width = $imageSize[0];
							$height = $imageSize[1];

							$img = new Bitmap($file['tmp_name']);
							$img->open();
							$img->save($fileDir, 'user_img_' . $fileName);
							$img->dispose();
							$fileExtension = $img->getExtension();

							// $set['height'] = 170;
							// $set['width'] = 310;
							// $set['align'] = 'center';
							// $set['valign'] = 'middle';

							// $img_edt = new Bitmap($file['tmp_name']);
							// $img_edt->open();
							// $img_edt->resizeAndCrop2($imageSize, $set);
							// $img_edt->save($fileDir, 'user_img_edt_' . $fileName);
							// $img_edt->dispose();
							// $fileExtension = $img_edt->getExtension();

							$set['height'] = 29;
							$set['width'] = 29;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$avatar = new Bitmap($file['tmp_name']);
							$avatar->open();
							$avatar->resizeAndCrop2($imageSize, $set);
							$avatar->save($fileDir, 'avatar_' . $fileName);
							$avatar->dispose();

							$this->data['User']['size'] = $file['size'];
							$this->data['User']['image'] = $fileName . '.' .  $fileExtension;
							$return = true;
							$erase = true;
						} catch (BitmapException $e) {
							$this->invalidate('image', 'bitmapError');
						}
					} else {
						$this->invalidate('image', 'unsupportedType');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('image', 'uploadError');
			}
		} else {
			if (isset($this->data['User']['erase']) && $this->data['User']['erase']) {
				$this->data['User']['image'] = null;
			} else {
				unset($this->data['User']['image']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($image) && ((isset($this->data['User']['erase']) && $this->data['User']['erase']) || $erase)) {
			unlink($fileDir . DS . 'avatar_' . $image);
			unlink($fileDir . DS . 'user_img_' . $image);
			//unlink($fileDir . DS . 'user_img_edt_' . $image);
		}

		return $return;{
		
	}
}