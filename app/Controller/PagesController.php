<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function home() {
		$this->loadModel('MainImage');
		$this->loadModel('Project');
		$this->loadModel('Publication');

		$covers = $this->MainImage->find('all');
		$projects = $this->Project->find('all',
			array(
				'conditions' => array('Project.main' => 1),
				'order' => array('Project.created DESC')
			)
		);
		$publications = $this->Publication->find('all',
			array(
				'conditions' => array('Publication.main' => 1),
				'order' => array('Publication.created DESC')
			)
		);

		$this->set('activeMenu', 'home');
		$this->set(compact('covers', 'projects', 'publications'));
	}

	public function admin_home() {
		$this->layout = 'admin/default';
		$viewTitle = 'Panel de Administración';
		$viewSubTitle = Configure::read('App.configurations.title');

		$this->loadModel('User');
		$this->loadModel('Post');
		$this->loadModel('Research');
		$this->loadModel('Project');
		$this->loadModel('MainImage');

		$totalUsers = $this->User->find('count');
		$totalPosts = $this->Post->find('count');
		$totalResearches = $this->Research->find('count');
		$totalProjects = $this->Project->find('count');
		$totalProfiles = $totalUsers - 1;
		$totalImages = $this->MainImage->find('count');

		$this->set(compact('totalUsers', 'viewTitle', 'viewSubTitle', 'totalPosts', 'totalProjects', 'totalResearches', 'totalProfiles', 'totalImages'));
		$this->set('activeMenu', 'home');
	}
}
