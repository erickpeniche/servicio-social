<?php
App::uses('AppController', 'Controller');
/**
 * Profiles Controller
 *
 * @property Profile $Profile
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProfilesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->layout = 'admin/default';
		$viewTitle = 'Perfiles';

		$loggedUser = $this->Session->read('Auth.User');

		$this->set('profiles', $this->Profile->find('all'));
		$this->set('activeMenu', 'profiles');
		$this->set(compact('viewTitle', 'loggedUser'));
	}

	public function admin_view($id = null, $name = null) {
		$this->layout = 'admin/default';

		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $id == $loggedUser['id'] ? true : false;

		$this->loadModel('Project');
		$projects = $this->Project->find('all', array('conditions' => array('Project.user_id' => $loggedUser['id'])));

		$this->loadModel('Research');
		$researches = $this->Research->find('all', array('conditions' => array('Research.user_id' => $loggedUser['id'])));

		/*if(!AppController::isSuperUser() && !$ownAccount){
			$this->Session->setFlash('No cuentas con los permisos para acceder a esta sección.', 'admin/flash/toastr', array('title'=>'Acceso denegado!', 'type'=>'warning'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		if (!$this->Profile->exists($id)) {
			$this->Session->setFlash('El perfil no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$this->set('profile', $this->Profile->findById($id));
		$this->set(compact('ownAccount', 'projects', 'researches'));
		$this->set('activeMenu', 'profiles');
	}

	public function admin_edit($id = null, $name = null) {
		$this->layout = 'admin/default';
		$viewTitle = 'Editar';
		$viewSubTitle = 'Profile';

		if (!$this->Profile->exists($id)) {
			$this->Session->setFlash('El perfil no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$loggedUser = $this->Session->read('Auth.User');
		$profile = $this->Profile->findById($id);
		$ownAccount = $profile['Profile']['user_id'] == $loggedUser['id'] ? true : false;
		if (!$ownAccount) {
			$this->Session->setFlash('No cuentas con los permisos para acceder a esta sección.', 'admin/flash/toastr', array('title'=>'Acceso denegado!', 'type'=>'warning'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Profile->saveAll($this->request->data)) {
				$this->Session->setFlash(__('El profile ha sido editado.'), 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'view', $id, Format::clean($profile['User']['fullname'])));
			} else {
				debug($this->Profile->validationErrors);
				exit();
				$this->Session->setFlash(__('El profile no se pudo editar.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			}
		} else {
			$options = array('conditions' => array('Profile.' . $this->Profile->primaryKey => $id), 'contain' => 'Media');
			$this->request->data = $this->Profile->find('first', $options);
		}

		$this->set(compact('viewTitle', 'viewSubTitle', 'profile'));
		$this->set('activeMenu', 'profiles');
	}

	public function downloadCV($id = null) {
		if (!$this->Profile->exists($id)) {
			$this->Session->setFlash('El curriculum no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$profile = $this->Profile->findById($id);
		if (empty($profile['Profile']['curriculum_key'])) {
			$this->Session->setFlash('El curriculum no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$this->viewClass = 'Media';

		$params = array(
			'id'        => 'cv_' . $profile['Profile']['curriculum_key'],
			'name'      => 'Curriculum Vitae - ' . $profile['User']['fullname'],
			'download'  => true,
			'path'      => WEBROOT_DIR . DS . 'files' . DS . 'documents' . DS
			);
		$this->set($params);
	}

	public function view($id) {
		if (!$this->Profile->exists($id)) {
			$this->Session->setFlash('El perfil no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			throw new NotFoundException(__('Invalid profile'));
		}

		$profile = $this->Profile->find('first', array(
			'conditions' => array('Profile.id' => $id),
			'contain' => array(
				'User' => array(
					'Publication'
					)
				)
			)
		);

		$this->loadModel('Project');
		$this->loadModel('Research');

		$projects = $this->Project->find('all', array('conditions' => array('Project.user_id' => $profile['Profile']['user_id'])));
		$researches = $this->Research->find('all', array('conditions' => array('Research.user_id' => $profile['Profile']['user_id'])));

		$this->set('activeMenu', 'users');
		$this->set(compact('profile', 'projects', 'researches'));
	}
}