<?php
App::uses('AppController', 'Controller');
/**
 * Configurations Controller
 *
 * @property Configuration $Configuration
 */
class ConfigurationsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin/default';
		$viewTitle = 'Configuraciones';

		if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}

		$configurations = $this->Configuration->find('all');
		$this->set(compact('viewTitle', 'configurations'));
		$this->set('activeMenu', 'configurations');
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($category = null, $id = null, $name = null) {
		$this->layout = 'admin/default';
		$viewTitle = 'Editar';
		$viewSubTitle = 'Configuraciones';

		if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}

		if (!$this->Configuration->exists($id)) {
			throw new NotFoundException(__('Invalid configuration'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Configuration->save($this->request->data)) {
				$this->Session->setFlash('La configuración ha sido editada.', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				switch ($category) {
					case 1:
						$category = '#contacto';
						break;
					case 2:
						$category = '#website';
						break;
					case 3:
						$category = '#social';
						break;

				}
				$this->redirect(array('action' => 'index'.$category));
			} else {
				$this->Session->setFlash('La configuración no se pudo editar.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			}
		}

		$options = array('conditions' => array('Configuration.' . $this->Configuration->primaryKey => $id));
		$configuration = $this->Configuration->find('first', $options);
		$this->request->data = $configuration;

		$isMap = $configuration['Configuration']['is_map']=='1' ? true : false;

		$this->set(compact('viewTitle', 'viewSubTitle', 'configuration', 'isMap'));
		$this->set('activeMenu', 'configurations');
	}
}
