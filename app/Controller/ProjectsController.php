<?php
App::uses('AppController', 'Controller');
/**
 * Projects Controller
 *
 * @property Project $Project
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProjectsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->layout = 'admin/default';
		$viewTitle = 'projects';

		$loggedUser = $this->Session->read('Auth.User');
		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		$this->set('projects', $this->Project->find('all'));
		$this->set('activeMenu', 'projects');
		$this->set(compact('viewTitle', 'loggedUser'));
	}

	public function admin_view($id = null, $name = null) {
		$this->layout = 'admin/default';

		if (!$this->Project->exists($id)) {
			$this->Session->setFlash('El proyecto no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$project = $this->Project->findById($id);

		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $project['Project']['user_id'] == $loggedUser['id'] ? true : false;

		/*if(!AppController::isSuperUser() && !$ownAccount){
			$this->Session->setFlash('No cuentas con los permisos para acceder a esta sección.', 'admin/flash/toastr', array('title'=>'Acceso denegado!', 'type'=>'warning'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		$this->set(compact('ownAccount', 'project'));
		$this->set('activeMenu', 'projects');
	}

	public function admin_add() {
		$this->layout = 'admin/default';
		$viewTitle = 'Nuevo';
		$viewSubTitle = 'Proyecto';

		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		if ($this->request->is('post')) {
			$this->Project->create();
			if ($this->Project->save($this->request->data)) {
				$this->Session->setFlash(__('El proyecto <strong>' . $this->request->data['Project']['title'] . '</strong> ha sido creado.'), 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El proyecto no se pudo crear.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error')));
			}
		}
		$users = $this->Project->User->find('list');
		$this->set(compact('users'));
		$this->set(compact('viewTitle', 'viewSubTitle'));
		$this->set('activeMenu', 'projects');
	}

	public function admin_edit($id = null, $name = null) {
		$this->layout = 'admin/default';
		$viewTitle = 'Editar';
		$viewSubTitle = 'Project';

		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		if (!$this->Project->exists($id)) {
			$this->Session->setFlash('El project no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Project->save($this->request->data)) {
				$this->Session->setFlash(__('El project ha sido editado.'), 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'view', $id, Format::clean($project['Project']['name'])));
			} else {
				$this->Session->setFlash(__('El project no se pudo editar.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			}
		} else {
			$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
			$this->request->data = $this->Project->find('first', $options);
		}
		$users = $this->Project->User->find('list');
		$this->set(compact('users'));
		$project = $this->request->data;
		$this->set(compact('viewTitle', 'viewSubTitle', 'project'));
		$this->set('activeMenu', 'projects');
	}

	public function admin_delete($id = null) {
		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		if ($this->Session->read('Auth.User.id')==$id) {
			$this->Session->setFlash('No puedes realizar esta accion. (No te eliminarías a ti mismo o si?)', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			$this->redirect(array('action' => 'index', 'admin' => true));
		}*/

		$this->Project->id = $id;
		if (!$this->Project->exists()) {
			throw new NotFoundException(__('Invalid project'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Project->delete()) {
			$this->Session->setFlash(__('The project eliminado correctamente.'), 'admin/flash/toastr', array('title'=>'Atención!', 'type'=>'warning'));
		} else {
			$this->Session->setFlash(__('No se pudo eliminar el project.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_toggle_main($id = null) {
		if (!$this->Project->exists($id)) {
			$this->Session->setFlash('El proyecto no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$project = $this->Project->findById($id);
		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $project['Project']['user_id'] == $loggedUser['id'] ? true : false;

		if (!$ownAccount) {
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		} else {
			$this->Project->id = $id;
			$value = $project['Project']['main'] == 0 ? 1 : 0;
			if ($this->Project->saveField('main', $value)) {
				$this->Session->setFlash($value == 1 ? 'El proyecto se ha marcado como destacado' : 'El proyecto se ha desmarcado como destacado', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'index', 'controller' => 'projects'));
			}
		}
	}

	public function admin_toggle_public($id = null) {
		if (!$this->Project->exists($id)) {
			$this->Session->setFlash('El proyecto no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$project = $this->Project->findById($id);
		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $project['Project']['user_id'] == $loggedUser['id'] ? true : false;

		if (!$ownAccount) {
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		} else {
			$this->Project->id = $id;
			$value = $project['Project']['public'] == 0 ? 1 : 0;
			if ($this->Project->saveField('public', $value)) {
				$this->Session->setFlash($value == 1 ? 'El proyecto se ha marcado como <strong>público</strong>' : 'El proyecto se ha marcado como <strong>privado</strong>', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'index', 'controller' => 'projects'));
			}
		}
	}

	public function index() {
		$projects = $this->Project->find('all', array('conditions' => array('Project.public' => 1)));
		$users = $this->Project->User->find('all', array('contain' => false, 'conditions' => array('User.id !=' => 1)));

		$this->set(compact('projects', 'users'));
		$this->set('activeMenu', 'projects');
	}

	public function view($id = null, $name = null) {
		if (!$this->Project->exists($id)) {
			$this->Session->setFlash('El perfil no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			throw new NotFoundException(__('Invalid profile'));
		}

		$project = $this->Project->findById($id);

		$this->set('activeMenu', 'projects');
		$this->set(compact('project'));
	}
}
