<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function admin_login() {
		$this->layout = 'admin/login';

		if($this->Auth->loggedIn()) {
			return $this->redirect($this->Auth->redirect());
		}

		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				if (!empty($this->request->data) && isset($this->request->data['remember']) && $this->request->data['remember'] == 1) {
					$cookie = $this->Session->read('Auth');
					$this->Cookie->write('Auth', $cookie, true, '+4 weeks');
					unset($this->request->data['remember']);
				}
				$this->Session->setFlash($this->Auth->user('fullname').'!', 'admin/flash/toastr', array('title'=>'Bienvenido(a)', 'type'=>'info'));
				return $this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash('La combinación de usuario y contraseña no son válidas o el usuario se encuentra inactivo.', 'admin/flash/toastr', array('title'=>'Acceso denegado!', 'type'=>'error'), 'auth');
			}
		}

		if (empty($this->request->data)) {
			$cookie = $this->Cookie->read('Auth.User');
			if (!is_null($cookie)) {
				if ($this->Auth->login($cookie)) {
					return $this->redirect($this->Auth->redirect());
				}
			}
		}
	}

	public function admin_logout(){
		$cookie = $this->Cookie->read('Auth');
		if (!is_null($cookie)) {
			$this->Cookie->destroy();
		}
		$this->Session->delete('Auth');
		return $this->redirect($this->Auth->logout());
	}

	public function forget_password(){
		$this->autoRender = false;
		$this->request->onlyAllow('ajax');

		$data = array(
			'content' => null,
			'error' => null
		);

		$user = $this->User->findByEmail($this->request->data['User']['recovery_email']);
		if (!empty($user)) {
			$data['content'] = array(
				'email' => $user['User']['email']
			);
			App::uses('CakeEmail', 'Network/Email');
			$this->loadModel('Configuration');
			$config = $this->Configuration->find('list', array('fields' => array('key', 'value')));
			$code = md5(rand() * 100000);
			$update = array('id' => $user['User']['id'], 'reset_password_code' => $code);
			$this->User->save($update);
			$Email = new CakeEmail('service');
			$Email->from(array('noreply@yinkapublicidad.com' => $config['title']))
				->template('forgot_password', 'metronic')
				->emailFormat('html')
				->helpers(array('Html', 'Text'))
				->viewVars(
					array(
						'config' => $config,
						'user' => $user,
						'code' => $code
					)
				)
				->to($this->request->data['User']['recovery_email'])
				->subject('Reestablecer Contraseña - ');

			if (!($Email->send())) {
				$data['error'] = 'No se envió el email';
				$this->response->statusCode(500);
			}
		} else {
			$data['error'] = 404;
			$this->response->statusCode(404);
		}

		return json_encode($data);
	}

	public function change_password($code = null){
		$this->layout = 'admin/login';
		$user = $this->User->findByResetPasswordCode($code);
		if (!empty($user)) {
			if ($this->request->is('post') || $this->request->is('put')) {
				$this->request->data['User']['reset_password_code'] = null;
				$this->request->data['User']['id'] = $user['User']['id'];

				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash('Inicia sesión con tu nueva contraseña', 'admin/flash/toastr', array('title'=>'Contraseña guardada', 'type'=>'success'));
					$this->redirect(array('controller' => 'users', 'action' => 'login', 'admin' => true));
				}else{
					$this->Session->setFlash('No se ha podido guardar tu contraseña. Verifica los errores.', 'admin/flash/toastr', array('title'=>'Error', 'type'=>'error'));
					$this->set(compact('code'));
				}
			}else{
				$this->set(compact('code'));
			}
		} else {
			$this->Session->setFlash('El código no existe o ha expirado.', 'admin/flash/toastr', array('title'=>'Acceso denegado!', 'type'=>'error'));
			$this->redirect(array('controller' => 'users', 'action' => 'login', 'admin' => true));
		}
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = 'admin/default';
		$viewTitle = 'Usuarios';

		$loggedUser = $this->Session->read('Auth.User');
		if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		$this->set('users', $this->User->find('all'));
		$this->set('activeMenu', 'users');
		$this->set(compact('viewTitle', 'loggedUser'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = 'admin/default';

		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $id == $loggedUser['id'] ? true : false;

		if(!AppController::isSuperUser() && !$ownAccount){
			$this->Session->setFlash('No cuentas con los permisos para acceder a esta sección.', 'admin/flash/toastr', array('title'=>'Acceso denegado!', 'type'=>'warning'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}

		$viewTitle = 'Ver';
		$viewSubTitle = 'Usuario';
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}


		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
		$this->set(compact('viewTitle', 'viewSubTitle', 'ownAccount'));
		$this->set('activeMenu', 'users');
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = 'admin/default';

		if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		$viewTitle = 'Nuevo';
		$viewSubTitle = 'Usuario';

		if ($this->request->is('post')) {
			$this->User->validator()->add('email', 'isUnique', array(
				'rule' => 'isUnique',
				'message' => 'El correo electrónico ya se encuentra en uso'
			));

			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->User->Profile->save(array('Profile' => array('user_id' => $this->User->id)));
				$this->Session->setFlash('El usuario <strong>'.$this->request->data['User']['name'] . ' ' . $this->request->data['User']['paternal_surname'] .'</strong> ha sido creado.', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('El usuario no se pudo crear.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			}
		}
		$this->set(compact('viewTitle', 'viewSubTitle'));
		$this->set('activeMenu', 'users');
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null, $name = null) {
		$this->layout = 'admin/default';

		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $id == $loggedUser['id'] ? true : false;

		if(!AppController::isSuperUser() && !$ownAccount){
			$this->Session->setFlash('No cuentas con los permisos para acceder a esta sección.', 'admin/flash/toastr', array('title'=>'Acceso denegado!', 'type'=>'warning'));
			return $this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		$viewTitle = 'Editar';
		$viewSubTitle = 'Usuario';


		if (!$this->User->exists($id)) {
			$this->Session->setFlash('El usuario no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$user = $this->User->findById($id);

		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$data = array(
				'content' => null,
				'error' => null
			);
			if (!$this->User->save($this->request->data)) {
				$data['error'] = 500;
				$this->response->statusCode(500);
			}

			return json_encode($data);
		} elseif($this->request->is('post') || $this->request->is('put')) {
			if(!empty($this->data['User']['image'])){
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash('La imagen del usuario ha sido editada.', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				} else {
					$this->Session->setFlash('La imagen del usuario no se pudo editar.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
				}
				return $this->redirect(array('action' => 'edit', $id, Format::clean($user['User']['fullname']), '#' => 'avatar'));
			}else{
				if ($this->User->save($this->request->data)) {
					$this->Session->setFlash('La contraseña del usuario ha sido editada.', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
					return $this->redirect(array('action' => 'logout', 'admin' => true, '#' => false));
				} else {
					$this->Session->setFlash('No se pudo editar tu contraseña, inténtalo de nuevo.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
				}
			}
		}

		$this->request->data = $user;
		unset($this->request->data['User']['password']);
		$this->set(compact('user', 'viewTitle', 'viewSubTitle', 'ownAccount'));
		$this->set('activeMenu', 'users');
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		if ($this->Session->read('Auth.User.id')==$id) {
			$this->Session->setFlash('No puedes realizar esta accion. (No te eliminarías a ti mismo o si?)', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			$this->redirect(array('action' => 'index', 'admin' => true));
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash('Usuario eliminado correctamente', 'admin/flash/toastr', array('title'=>'Atención!', 'type'=>'warning'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('No se pudo eliminar al usuario.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
		$this->redirect(array('action' => 'index'));
	}

	public function index() {
		$users = $this->User->find('all', array('conditions' => array('User.id !=' => 1)));
		$this->set('activeMenu', 'users');
		$this->set(compact('users'));
	}
}
