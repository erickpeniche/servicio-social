<?php
App::uses('AppController', 'Controller');
/**
 * DefaultArchives Controller
 *
 * @property DefaultArchive $DefaultArchive
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class DefaultArchivesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function admin_add() {
		$this->layout = 'admin/default';

		$query = $this->request->query;

		if ($this->request->is('post') || $this->request->is('put')) {
			parse_str($this->request->data['DefaultArchive']['query'], $query);
			unset($this->request->data['DefaultArchive']['query']);

			$model = ucfirst($this->request->data['DefaultArchive']['model']);
			$foreignKey = $this->request->data['DefaultArchive']['foreign_key'];

			$this->DefaultArchive->set($this->request->data);

			if($this->DefaultArchive->validates()){
				App::import('model', $model);
				$this->{$model} = new $model();
				$mod = $this->{$model}->find('first', array(
					'conditions' => array($model . '.id' => $foreignKey),
					'contain' => false
				));

				$image = null;
				$display = null;
				$view = null;

				if ($mod) {
					$this->DefaultArchive->create();

					//File upload
					$file = $this->data['DefaultArchive']['file'];
					$fileDir = WWW_ROOT . 'files' . DS . 'documents'. DS;
					$fileName = '';
					$fileExtension = '';

					//A file was uploaded
					if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
						//There is no error, the file was uploaded with success
						if ($file['error'] == 0) {

							//Tells whether the file was uploaded via HTTP POST
							if (is_uploaded_file($file['tmp_name'])) {
								try {
									$fileName = md5($file['tmp_name'] . (rand() * 100000));
									$fileSize = filesize($file['tmp_name']);
									$fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);

									move_uploaded_file($file['tmp_name'], $fileDir . DS . strtolower($query['model']) . '_' . $fileName .'.'. $fileExtension);

									$this->request->data['DefaultArchive']['size'] = $fileSize;
									$this->request->data['DefaultArchive']['file'] = $fileName . '.' .  $fileExtension;
									$this->request->data['DefaultArchive']['name'] = $file['name'];

									$erase = true;
								} catch (BitmapException $e) {
									$this->invalidate('image', 'bitmapError');
								}
							} else {
								$this->invalidate('image', 'maliciousUpload');
							}
						} else {
							//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
							$this->invalidate('file', 'uploadError');
						}
					}

					if ($this->DefaultArchive->save($this->request->data, false)) {
						$this->Session->setFlash('Se ha agregado el documento correctamente', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
						$this->redirect(array('controller' => $query['controller'] , 'action' => 'view', $query['foreign_key'], $query['slug'], 'admin' => true));
					} else {
						//Deletes copied images
						if (!empty($fileName) && !array_key_exists('image', $this->DefaultArchive->invalidFields())) {
							unlink($fileDir . DS . strtolower($query['model']) . '_' . $fileName . '.' . $fileExtension);
						}
						$this->Session->setFlash('No se ha podido subir el documento', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
					}
				}
			}else{
				$this->Session->setFlash('No se ha podido agregar el documento', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			}
		}

		$this->set(compact('query'));
		$this->set('activeMenu', $query['controller']);
	}

	public function admin_delete($id = null) {
		$query = $this->request->query;

		$this->DefaultArchive->id = $id;
		if (!$this->DefaultArchive->exists()) {
			throw new NotFoundException(__('Invalid default archive'));
		}
		$loggedUser = $this->Session->read('Auth.User');
		$archive = $this->DefaultArchive->findById($id);
		$ownAccount = $archive[$query['model']]['user_id'] == $loggedUser['id'] ? true : false;

		if (!$ownAccount) {
			$this->Session->setFlash(__('No puedes realizar esta acción.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error')));
			return $this->redirect(array('controller' => '', 'action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ($this->DefaultArchive->delete()) {
			$this->Session->setFlash('El documento ha sido eliminado correctamente', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
			$this->redirect(array('controller' => $query['controller'], 'action' => 'view', $query['foreign_key'], $query['slug']));
		}
		$this->Session->setFlash('No se pudo eliminar el documento', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
		$this->redirect(array('controller' => $query['controller'], 'action' => 'view', $query['foreign_key'], $query['slug']));
	}

	public function download($id = null) {
		$query = $this->request->query;

		if (!$this->DefaultArchive->exists($id)) {
			$this->Session->setFlash('El documento no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'home', 'controller' => 'pages', 'admin' => true));
		}

		$document = $this->DefaultArchive->findById($id);

		$this->viewClass = 'Media';

		$params = array(
			'id'        => strtolower($document['DefaultArchive']['model']) . '_' . $document['DefaultArchive']['file'],
			'name'      => $document['DefaultArchive']['name'],
			'download'  => true,
			'path'      => WEBROOT_DIR . DS . 'files' . DS . 'documents' . DS
			);
		$this->set($params);
	}

	public function isSuperUser($user) {
		if (isset($user['role']) && $user['role'] === 'Super User') {
			return true;
		}
		return false;
	}

}
