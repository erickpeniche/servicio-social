<?php
App::uses('AppController', 'Controller');
/**
 * Posts Controller
 *
 * @property Post $Post
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PostsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->layout = 'admin/default';

		$loggedUser = $this->Session->read('Auth.User');
		/*$loggedUser = $this->Session->read('Auth.User');
		if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		$this->set('posts', $this->Post->find('all'));
		$this->set('activeMenu', 'posts');
		$this->set(compact('loggedUser'));
	}

	public function admin_view($id = null, $name = null) {
		$this->layout = 'admin/default';

		if (!$this->Post->exists($id)) {
			$this->Session->setFlash('El post no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$post = $this->Post->findById($id);

		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $post['Post'] ['user_id'] == $loggedUser['id'] ? true : false;

		/*if(!AppController::isSuperUser() && !$ownAccount){
			$this->Session->setFlash('No cuentas con los permisos para acceder a esta sección.', 'admin/flash/toastr', array('title'=>'Acceso denegado!', 'type'=>'warning'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		$this->set(compact('ownAccount', 'post'));
		$this->set('activeMenu', 'posts');
	}

	public function admin_add() {
		$this->layout = 'admin/default';

		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		if ($this->request->is('post')) {
			$this->Post->create();
			if ($this->Post->save($this->request->data)) {
				$this->Session->setFlash(__('El Post <strong>' . $this->request->data['Post']['title'] . '</strong> ha sido creado.'), 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El Post no se pudo crear.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error')));
			}
		}
		$this->set('activeMenu', 'posts');
	}

	public function admin_edit($id = null, $name = null) {
		$this->layout = 'admin/default';

		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		if (!$this->Post->exists($id)) {
			$this->Session->setFlash('El post no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Post->save($this->request->data)) {
				$this->Session->setFlash(__('El post ha sido editado.'), 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'view', $id, Format::clean($post['Post']['name'])));
			} else {
				$this->Session->setFlash(__('El post no se pudo editar.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			}
		} else {
			$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
			$this->request->data = $this->Post->find('first', $options);
		}
		$post = $this->request->data;
		$this->set(compact('post'));
		$this->set('activeMenu', 'posts');
	}

	public function admin_delete($id = null) {
		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		if ($this->Session->read('Auth.User.id')==$id) {
			$this->Session->setFlash('No puedes realizar esta accion. (No te eliminarías a ti mismo o si?)', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			$this->redirect(array('action' => 'index', 'admin' => true));
		}*/

		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid post'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Post->delete()) {
			$this->Session->setFlash(__('The post eliminado correctamente.'), 'admin/flash/toastr', array('title'=>'Atención!', 'type'=>'warning'));
		} else {
			$this->Session->setFlash(__('No se pudo eliminar el post.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_toggle_publish($id = null) {
		if (!$this->Post->exists($id)) {
			$this->Session->setFlash('La entrada no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$post = $this->Post->findById($id);
		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $post['Post']['user_id'] == $loggedUser['id'] ? true : false;

		if (!$ownAccount) {
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		} else {
			$this->Post->id = $id;
			$value = $post['Post']['published'] == 0 ? 1 : 0;
			$this->Post->set(array(
				'published' => $value,
				'published_date' => $value == 1 ? date("Y-m-d H:i:s") : $post['Post']['published_date']
			));
			if ($this->Post->save()) {
				$this->Session->setFlash($value == 1 ? 'La entrada se ha publicado' : 'La entrada se ha ocultado de las publicaciones', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'index', 'controller' => 'posts'));
			}
		}
	}

	public function index() {
		$posts = $this->Post->find('all');
		$this->set(compact('posts'));
		$this->set('activeMenu', 'blog');
	}

	public function view($id = null, $name = null) {
		if (!$this->Post->exists($id)) {
			$this->Session->setFlash('El post no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			throw new NotFoundException(__('Invalid profile'));
		}

		$post = $this->Post->findById($id);

		$this->set('activeMenu', 'blog');
		$this->set(compact('post'));
	}
}