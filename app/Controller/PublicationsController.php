<?php
App::uses('AppController', 'Controller');
/**
 * Publicationes Controller
 *
 * @property Publication $Publication
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PublicationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->layout = 'admin/default';

		$loggedUser = $this->Session->read('Auth.User');
		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		$this->set('publications', $this->Publication->find('all'));
		$this->set('activeMenu', 'publications');
		$this->set(compact('loggedUser'));
	}

	public function admin_view($id = null, $name = null) {
		$this->layout = 'admin/default';

		if (!$this->Publication->exists($id)) {
			$this->Session->setFlash('La publicación no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$publication = $this->Publication->findById($id);

		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $publication['Publication']['user_id'] == $loggedUser['id'] ? true : false;

		/*if(!AppController::isSuperUser() && !$ownAccount){
			$this->Session->setFlash('No cuentas con los permisos para acceder a esta sección.', 'admin/flash/toastr', array('title'=>'Acceso denegado!', 'type'=>'warning'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		$this->set(compact('ownAccount', 'publication'));
		$this->set('activeMenu', 'publications');
	}

	public function admin_add() {
		$this->layout = 'admin/default';

		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		if ($this->request->is('post')) {
			if ($this->request->data['Publication']['public'] == 1) {
				$this->request->data['Publication']['publication_date'] = date("Y-m-d H:i:s");
			}
			$this->Publication->create();
			if ($this->Publication->save($this->request->data)) {
				$this->Session->setFlash('La publicación <strong>' . $this->request->data['Publication']['title'] . '</strong> ha sido creada.', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('La publicación no se pudo crear.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			}
		}
		$this->set('activeMenu', 'publications');
	}

	public function admin_edit($id = null, $name = null) {
		$this->layout = 'admin/default';

		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		if (!$this->Publication->exists($id)) {
			$this->Session->setFlash('La publicación no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$options = array('conditions' => array('Publication.' . $this->Publication->primaryKey => $id));
		$publication = $this->Publication->find('first', $options);

		if ($this->request->is(array('post', 'put'))) {
			if ($publication['Publication']['public'] == 0) {
				if ($this->request->data['Publication']['public'] == 1) {
					$this->request->data['Publication']['publication_date'] = date("Y-m-d H:i:s");
				}
			} else {
				if ($this->request->data['Publication']['public'] == 0) {
					$this->request->data['Publication']['publication_date'] = null;
				}
			}
			if ($this->Publication->save($this->request->data)) {
				$this->Session->setFlash(__('La publicación ha sido editada.'), 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'view', $id, Format::clean($publication['Publication']['title'])));
			} else {
				$this->Session->setFlash(__('La publicación no se pudo editar.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			}
		} else {
			$this->request->data = $publication;
		}
		$publication = $this->request->data;
		$this->set(compact('publication'));
		$this->set('activeMenu', 'publications');
	}

	public function admin_delete($id = null) {
		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		if ($this->Session->read('Auth.User.id')==$id) {
			$this->Session->setFlash('No puedes realizar esta accion. (No te eliminarías a ti mismo o si?)', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			$this->redirect(array('action' => 'index', 'admin' => true));
		}*/

		$this->Publication->id = $id;
		if (!$this->Publication->exists()) {
			throw new NotFoundException(__('Invalid publication'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Publication->delete()) {
			$this->Session->setFlash(__('The publication eliminado correctamente.'), 'admin/flash/toastr', array('title'=>'Atención!', 'type'=>'warning'));
		} else {
			$this->Session->setFlash(__('No se pudo eliminar el publication.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function admin_toggle_main($id = null) {
		if (!$this->Publication->exists($id)) {
			$this->Session->setFlash('La publicación no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$publication = $this->Publication->findById($id);
		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $publication['Publication']['user_id'] == $loggedUser['id'] ? true : false;

		if (!$ownAccount) {
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		} else {
			$this->Publication->id = $id;
			$value = $publication['Publication']['main'] == 0 ? 1 : 0;

			if ($this->Publication->saveField('main', $value)) {
				$this->Session->setFlash($value == 1 ? 'La publicación se ha marcado como destacada' : 'La publicación se ha desmarcado como destacada', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'index', 'controller' => 'publications'));
			}
		}
	}

	public function admin_toggle_public($id = null) {
		if (!$this->Publication->exists($id)) {
			$this->Session->setFlash('La publicación no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$publication = $this->Publication->findById($id);
		$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $publication['Publication']['user_id'] == $loggedUser['id'] ? true : false;

		if (!$ownAccount) {
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		} else {
			$this->Publication->id = $id;
			$value = $publication['Publication']['public'] == false ? true : false;

			if ($value) {
				$data = array('public' => 1, 'publication_date' => date("Y-m-d H:i:s"));
			} else {
				$data = array('public' => 0, 'publication_date' => null);
			}

			$this->Publication->set($data);

			if ($this->Publication->save()) {
				$this->Session->setFlash($value == 1 ? 'La publicación se ha marcado como <strong>pública</strong>' : 'La publicación se ha marcado como <strong>privada</strong>', 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'index', 'controller' => 'publications'));
			}
		}
	}

	public function download($id = null) {
		if (!$this->Publication->exists($id)) {
			$this->Session->setFlash('La publicación no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$publication = $this->Publication->findById($id);
		if (empty($publication['Publication']['publication_key'])) {
			$this->Session->setFlash('El documento no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		$this->viewClass = 'Media';

		$params = array(
			'id'        => 'pub_' . $publication['Publication']['publication_key'],
			'name'      => $publication['Publication']['title'],
			'download'  => true,
			'path'      => WEBROOT_DIR . DS . 'files' . DS . 'documents' . DS
			);
		$this->set($params);
	}

	public function index() {
		$publications = $this->Publication->find('all', array(
			'conditions' => array('Publication.public' => 1),
			'contain' => array(
				'User' => array(
					'Profile'
				)
			),
			'order' => array('Publication.publication_date DESC')
		));
		$users = $this->Publication->User->find('all', array('contain' => false, 'conditions' => array('User.id !=' => 1)));

		$this->set(compact('publications', 'users'));
		$this->set('activeMenu', 'publications');
	}

	public function view($id = null, $name = null) {
		if (!$this->Publication->exists($id)) {
			$this->Session->setFlash('La publicación no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			throw new NotFoundException(__('Invalid profile'));
		}

		$publication = $this->Publication->find('first', array(
			'conditions' => array('Publication.id' => $id),
			'contain' => array(
				'User' => array(
					'Profile'
					)
				)
			)
		);

		$this->set('activeMenu', 'publications');
		$this->set(compact('publication'));
	}
}