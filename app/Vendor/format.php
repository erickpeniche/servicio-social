<?php
class Format {
	
	function clean($text) {
		$vowels = array('a','e','i','o','u','A','E','I','O','U');
		$accents = array('á','é','í','ó','ú','Á','É','Í','Ó','Ú');
		$inverse = array('à','è','ì','ò','ù','À','È','Ì','Ò','Ù');
		$circumflex = array('â','ê','î','ô','û','Â','Ê','Î','Ô','Û');
		$dieresis = array('ä','ë','ï','ö','ü','Ä','Ë','Ï','Ö','Ü');
	
		$clean = $text;
		//Replace accents (´)
		$clean = str_replace($accents, $vowels, $clean);
		//Replace inverse accents (`)
		$clean = str_replace($inverse, $vowels, $clean);
		//Replace dieresis (¨)
		$clean = str_replace($dieresis, $vowels, $clean);
		//Replace circumflex
		$clean = str_replace($circumflex, $vowels, $clean);
		//"ñ" -> "n", " " -> "-"
		$clean = str_replace('ñ', 'n', $clean);
		$clean = str_replace(' ', '-', $clean);
		$clean = preg_replace("/[^a-zA-Z0-9_\-]/", "", $clean);
	
		$clean = strtolower($clean);
	
		return $clean;
	}
   	
	function toParagraph($text) {
		// Use \n for newline on all systems
		$html = preg_replace("/(\r\n|\n|\r)/", "\n", $text);
		
		// Only allow two newlines in a row.
		$html = preg_replace("/\n\n+/", "\n\n", $html);
		
		// Put <p>..</p> around paragraphs
		$html = preg_replace('/\n?(.+?)(\n\n|\z)/s', "<p>$1</p>", $html);
		
		// Convert newlines not preceded by </p> to a <br /> tag
		$html = preg_replace('|(?<!</p>)\s*\n|', "<br />", $html); 		
		
		return $html;
	}
	
	function pad($strInput = "", $intPadLength, $strPadString = "&nbsp;", $intPadType = STR_PAD_RIGHT) {
		if (strlen(trim(strip_tags($strInput))) < intval($intPadLength)) {	
			switch ($intPadType) {
				// STR_PAD_LEFT
				case 0:
				$offsetLeft = intval($intPadLength - strlen(trim(strip_tags($strInput))));
				$offsetRight = 0;
				break;
	
				// STR_PAD_RIGHT
				case 1:
				$offsetLeft = 0;
				$offsetRight = intval($intPadLength - strlen(trim(strip_tags($strInput))));
				break;
	
				// STR_PAD_BOTH
				case 2:
				$offsetLeft = intval(($intPadLength - strlen(trim(strip_tags($strInput)))) / 2);
				$offsetRight = round(($intPadLength - strlen(trim(strip_tags($strInput)))) / 2, 0);
				break;
	
				// STR_PAD_RIGHT
				default:
				$offsetLeft = 0;
				$offsetRight = intval($intPadLength - strlen(trim(strip_tags($strInput))));
				break;
			}
	
			$strPadded = str_repeat($strPadString, $offsetLeft) . $strInput . str_repeat($strPadString, $offsetRight);
			unset($strInput, $offsetLeft, $offsetRight);
	
			return $strPadded;
		} else {
			return $strInput;
		}
	}
}
?>
