<?php
require(LIBS . 'object.php');
require(LIBS . 'file.php');
require(LIBS . 'set.php');

class Package {
/**
 * Constructor
 *
 * @param array	$resources 	File names formatted using notation folder.subfolder.file_name. 
 * 							Ex. File lib/scriptaculous/scriptaculous.js should be written 
 * 							lib.scriptaculous.scriptaculous  
 * @param string $type	Package type. Valid types are: js and css
 * @param boolean $compressed	If compressed is set to true, find resources in build folder,
 * 								otherwise find in src
 * @param boolean $cache	Read content from cache if $cache is set to true  
 */	
	function __construct($resources, $type = 'js', $compressed = false, $cache = true) {
		$this->output = "";
		$this->resources = $resources;		
		$this->type = strtolower($type);
		$this->compressed = $compressed;
		$this->cache = $cache;
		
		$this->version = 'src';		
		if ($compressed) {
			$this->version = 'build';
		}			

		//Construct cache file
		$basePath = CACHE . $this->type . DS;
		$filePath = $basePath . $this->version . DS . implode('-', $this->resources) . '.' . $this->type;
		$this->file = new File($filePath);	

		if (!$this->isCached() || !$this->cache) {
			$this->create();
		}
		
		//Reads content from cache only if output has not been assigned on create method 		
		if (empty($this->output) && $this->file->exists()) {
			$this->output = $this->file->read();
		}
	}
/**
 * Returns true if the package is cached.
 *
 * @return boolean True if package is cached
 */	
	function isCached() {		
		return $this->file->exists();
	}
/**
 * Creates package by grouping the resources in a file. It saves the result on disk 
 * only if all resources exists
 *
 * @return True on success
 */	
	function create() {
		$content = '';
		$allFilesExist = true;		
		$basePath = constant(strtoupper($this->type)) . $this->version . DS;
		
		foreach ($this->resources as $resource) {			
			$filePath = $basePath . str_replace('.', '/', $resource) . '.' . $this->type;
			$file = new File($filePath);			
			
			if ($file->exists()) {
				$content .= $file->read() . "\n";
			} else {
				$allFilesExist = false;
			}
		}

		if (!empty($content)) {
			//Caches only when all the files exist and cache is activated
			if ($allFilesExist && $this->cache) {
				if ($this->file->create()) {
					$content = $this->file->prepare($content);			
					
					//Saves the cache on disk
					$this->file->write($content);				
				}				
			}
			
			$this->output = $content;
			return true;
		}
		
		return false;
	}
/**
 * Render the package with http headers
 *
 * @return string Output
 */	
	function render() {	
		if (!empty($this->output)) {
			$contentType = 'text/css';
			if ($this->type == 'js') {
				$contentType = 'application/javascript';
			}
			
			header("Date: " . date("D, j M Y G:i:s ", $this->file->lastChange()) . 'GMT');
			header("Content-Type: " . $contentType);
			header("Expires: " . gmdate("D, j M Y H:i:s", time() + 24*60*60) . " GMT");
			header("Cache-Control: cache"); // HTTP/1.1
			header("Pragma: cache");        // HTTP/1.0
			
			print($this->output);			
		}		
	}
/**
 * Returns the package as a string
 *
 * @return string Output
 */	
	function toString() {		
		return $this->output;
	}
}
?>