var Login = function () {

	var handleLogin = function() {
		$('.login-form').validate({
				errorElement: 'span', //default input error message container
				errorClass: 'help-block', // default input error message class
				focusInvalid: false, // do not focus the last invalid input
				rules: {
					'data[User][email]': {
						required: true,
						email: true
					},
					'data[User][password]': {
						required: true
					},
					remember: {
						required: false
					}
				},

				invalidHandler: function (event, validator) { //display error alert on form submit
					$('.alert-danger', $('.login-form')).show();
				},

				highlight: function (element) { // hightlight error inputs
					$(element)
						.closest('.form-group').addClass('has-error'); // set error class to the control group
				},

				success: function (label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},

				errorPlacement: function (error, element) {
					error.insertAfter(element.closest('.input-icon'));
				},

				submitHandler: function (form) {
					form.submit();
				}
			});

			$('.login-form input').keypress(function (e) {
				if (e.which == 13) {
					if ($('.login-form').validate().form()) {
						$('.login-form').submit();
					}
					return false;
				}
			});
	}

	var handleForgetPassword = function () {
		var validator = $('.forget-form').validate({
				errorElement: 'span', //default input error message container
				errorClass: 'help-block', // default input error message class
				focusInvalid: true, // do not focus the last invalid input
				ignore: "",
				rules: {
					'data[User][recovery_email]': {
						required: true,
						email: true
					}
				},

				invalidHandler: function (event, validator) { //display error alert on form submit

				},

				highlight: function (element) { // hightlight error inputs
					$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
				},

				success: function (label) {
					label.closest('.form-group').removeClass('has-error');
					label.remove();
				},

				errorPlacement: function (error, element) {
					error.insertAfter(element.closest('.input-icon'));
				},

				submitHandler: function (form) {
					$.ajax({
						async:true,
						type:"POST",
						url:"olvide-contrasena",
						data:$(form).serialize(),
						error:function (response) {
							if (response.status == 404) {
								toastr.options = {
									closeButton: true,
									//positionClass: "toast-bottom-right",
									timeOut: "7000"
								}
								toastr.error("El correo electrónico no se encuentra registrado en nuestro sistema", "Error");
								validator.showErrors({
									"data[User][recovery_email]": "Correo electrónico no válido"
								});
							}
						},
						success:function (data, textStatus) {
							toastr.options = {
								closeButton: true,
								//positionClass: "toast-bottom-right",
								timeOut: "7000"
							}
							var user = JSON.parse(data);
							toastr.success("Se ha enviado un correo electrónico a <strong>"+user.content.email+"</strong> con las instrucciones para reestablecer tu contraseña");
							form.reset();
						},
						beforeSend: function (){
							$(form).find('#UserRecoveryEmail').attr('readonly', 'readonly').toggleClass('spinner');
							$('.loading-btn-frm').button('loading');
						},
						complete: function (){
							$(form).find('#UserRecoveryEmail').removeAttr('readonly').toggleClass('spinner');
							$('.loading-btn-frm').button('reset');
						}
					});
				}
			});

			$('.forget-form input').keypress(function (e) {
				if (e.which == 13) {
					if ($('.forget-form').validate().form()) {
						$('.forget-form').submit();
					}
					return false;
				}
			});

			jQuery('#forget-password').click(function () {
				jQuery('.login-form').hide();
				jQuery('.forget-form').show();
			});

			jQuery('#back-btn').click(function () {
				jQuery('.login-form').show();
				jQuery('.forget-form').hide();
			});
	}

	return {
		//main function to initiate the module
		init: function () {

			handleLogin();
			handleForgetPassword();

			$.backstretch([
				"http://yinkapublicidad.com/images/bg/a.jpg",
				"http://yinkapublicidad.com/images/bg/b.jpg",
				"http://yinkapublicidad.com/images/bg/c.jpg",
				"http://yinkapublicidad.com/images/bg/g.jpg",
				"http://yinkapublicidad.com/images/bg/h.jpg"
				], {
				  fade: 1000,
				  duration: 5000
			});
		}

	};

}();