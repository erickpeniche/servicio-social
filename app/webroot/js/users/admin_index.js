var AdminIndex = function () {

	var handleDataTable = function() {
		if (!jQuery().dataTable) {
			return;
		}

		// begin first table
		$('#users_table').dataTable({
			"aLengthMenu": [
				[5, 10, 15, 20, -1],
				[5, 10, 15, 20, "Todos"] // change per page values here
			],
			// set the initial value
			"iDisplayLength": 10,
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "Mostrar _MENU_ registros por página",
				"sZeroRecords": "No hay resultados",
				"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros totales",
				"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
				"sEmptyTable": "No existen usuarios, haz click en el boton de NUEVO para agregar un usuario",
				"sInfoFiltered": "(filtrados de _MAX_ registros totales)",
				"sSearch": "Buscar:",
				"oPaginate": {
					"sPrevious": "Anterior",
					"sNext": "Siguiente"
				}
			},
			"aoColumnDefs": [
				{
					'bSortable': false,
					'aTargets': [8]
				}
			]
		});

		jQuery('#users_table_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
		jQuery('#users_table_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
		jQuery('#users_table_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
	}

	var handleDeleteButton = function () {
		$('a#delete-btn').on('click', function (e) {
			e.preventDefault();
			var id = $(this).data('id');
			var url = $(this).attr('href');
			bootbox.dialog({
				message: "Confirmas que deseas eliminar a <strong>" + $(this).data('name') + "</strong>?",
				title: "Eliminar Usuario",
				buttons: {
					cancel: {
						label: 'Cancelar',
						className: 'default',
						callback: function() {
							return true;
						}
					},
					confirm: {
						label: '<i class="fa fa-trash-o"></i> Eliminar',
						className: "btn-danger",
						callback: function(){
							$('<form action="' + url + '" method="post"></form>').appendTo('body').submit();
							return true;
						}
					}
				}
			});
		});
	}

	return {
		//main function to initiate the module
		init: function () {
			handleDataTable();
			handleDeleteButton();
		}
	};
}();