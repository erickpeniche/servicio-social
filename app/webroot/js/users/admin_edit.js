var Edit = function () {

	var handlePersonalInfoValidation = function() {
		var form = $('.personal-form');
		var error = $('.alert-danger', form);

		form.validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: true, // focus the last invalid input
			ignore: "#UserTitle, #UserBirthday, #UserTelephone, #UserRfc, #UserCurp",
			rules: {
				'data[User][name]': {
					minlength: 2,
					required: true
				},
				'data[User][paternal_surname]': {
					minlength: 2,
					required: true
				},
				'data[User][maternal_surname]': {
					minlength: 2,
					required: true
				}
			},

			errorPlacement: function (error, element) { // render error placement for each input type
				if (element.parent(".input-group").size() > 0) {
					error.insertAfter(element.parent(".input-group"));
				} else if (element.attr("data-error-container")) {
					error.appendTo(element.attr("data-error-container"));
				} else if (element.parents('.radio-list').size() > 0) {
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) {
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) {
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit
				error.show();
				App.scrollTo(error, -200);
			},

			highlight: function (element) { // hightlight error inputs
				$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				label.closest('.form-group').removeClass('has-error'); // set success class to the control group
				label.remove();
			},

			submitHandler: function (form) {
				error.hide();
				$.ajax({
					async:true,
					type:"POST",
					url:"\/admin\/usuario\/editar\/"+$('#UserId').val(),
					data:$(form).serialize(),
					error:function (response) {
						if (response.status == 404) {
							toastr.error("No se encuentra la ruta de la acción", "Error");
						}
					},
					success:function (data, textStatus) {
						toastr.options = {
							closeButton: true,
							//positionClass: "toast-bottom-right",
							timeOut: "7000"
						}
						var user = JSON.parse(data);
						toastr.success("Se ha guardado la información con éxito!");
					},
					beforeSend: function (){
						$('.personal-form input').each(function(index) {
							$(this).attr('readonly', 'readonly').toggleClass('spinner');
						});
						$('.loading-btn-frm').button('loading');
						$('.cancel-btn-frm').button('loading');
					},
					complete: function (){
						$('.personal-form input').each(function(index) {
							$(this).removeAttr('readonly').toggleClass('spinner');
						});
						$('.loading-btn-frm').button('reset');
						$('.cancel-btn-frm').button('reset');
					}
				});
			}
		});
	}

	var handleAccountValidation = function() {
		$('.account-form').submit(function(event) {
			event.preventDefault();
			var $form = $(this);

			$.ajax({
				type:"POST",
				url:"\/admin\/usuario\/editar\/"+$('#UserId').val(),
				data: $form.serialize(),
				error:function (response) {
					if (response.status == 404) {
						toastr.error("No se encuentra la ruta de la acción", "Error");
					}
				},
				success:function (data, textStatus) {
					toastr.options = {
						closeButton: true,
						//positionClass: "toast-bottom-right",
						timeOut: "7000"
					}
					var user = JSON.parse(data);
					toastr.success("Se ha guardado la información con éxito!");
				},
				beforeSend: function (){
					$('.loading-btn-frm').button('loading');
					$('.cancel-btn-frm').button('loading');
				},
				complete: function (){
					$('.loading-btn-frm').button('reset');
					$('.cancel-btn-frm').button('reset');
				}
			});
		});
	}

	var handleAvatarValidation = function() {
		var form = $('.avatar-form');
		var error = $('.alert-danger', form);

		form.validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: true, // focus the last invalid input
			rules: {
				'data[User][image]': {
					required: true,
					extension: "jpg|jpeg|gif|png",
					accept: "image/jpeg,image/png,image/gif"
				}
			},

			errorPlacement: function (error, element) { // render error placement for each input type
				if (element.parent(".input-group").size() > 0) {
					error.insertAfter(element.parent(".input-group"));
				} else if (element.parents('.btn-file').size() > 0) {
					error.insertAfter($('div.fileinput-new.thumbnail'));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit
				error.show();
				App.scrollTo(error, -200);
			},

			highlight: function (element) { // hightlight error inputs
				$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				console.log(label);
				label.closest('.form-group').removeClass('has-error'); // set success class to the control group
				label.remove();
			},

			submitHandler: function (form) {
				error.hide();
				form.submit();
			}
		});
	}

	var handlePasswordValidation = function() {
		var form = $('.password-form');
		var error = $('.alert-danger', form);

		form.validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: true, // focus the last invalid input
			rules: {
				'data[User][_current_password]': {
					required: true
				},
				'data[User][new_password]': {
					minlength: 8,
					maxlength: 16,
					required: true
				},
				'data[User][password_confirm]': {
					equalTo: '#UserNewPassword',
					required: true
				}
			},

			errorPlacement: function (error, element) { // render error placement for each input type
				if (element.parent(".input-group").size() > 0) {
					error.insertAfter(element.parent(".input-group"));
				} else if (element.attr("data-error-container")) {
					error.appendTo(element.attr("data-error-container"));
				} else if (element.parents('.radio-list').size() > 0) {
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) {
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) {
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else if (element.parents('.btn-file').size() > 0) {
					error.insertAfter($('div.fileinput-new.thumbnail'));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit
				error.show();
				App.scrollTo(error, -200);
			},

			highlight: function (element) { // hightlight error inputs
				$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				console.log(label);
				label.closest('.form-group').removeClass('has-error'); // set success class to the control group
				label.remove();
			},

			submitHandler: function (form) {
				error.hide();
				form.submit();
			}
		});
	}

	var handleDatePickers = function () {
		if (jQuery().datepicker) {
			$('.date-picker').datepicker({
				rtl: App.isRTL(),
				autoclose: true,
				language: 'es'
			});
			$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
		}
	}

	return {
		//main function to initiate the module
		init: function () {
			handleDatePickers();
			handlePersonalInfoValidation();
			handleAccountValidation();
			handleAvatarValidation();
			handlePasswordValidation();
		}

	};
}();