var ChangePassword = function () {

	var handleChangePassword = function() {
		$('.change-password-form').validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			rules: {
				'data[User][new_password]': {
					required: true,
					minlength: 8,
					maxlength: 14
				},
				'data[User][password_confirm]': {
					required: true,
					equalTo: "#UserNewPassword"
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit
				$('.alert-danger', $('.change-password-form')).show();
			},

			highlight: function (element) { // hightlight error inputs
				$(element)
					.closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				label.closest('.form-group').removeClass('has-error');
				label.remove();
			},

			errorPlacement: function (error, element) {
				error.insertAfter(element.closest('.input-icon'));
			},

			submitHandler: function (form) {
				form.submit();
			}
		});

		$('.change-password-form input').keypress(function (e) {
			if (e.which == 13) {
				if ($('.change-password-form').validate().form()) {
					$('.change-password-form').submit();
				}
				return false;
			}
		});
	}

	return {
		//main function to initiate the module
		init: function () {
			handleChangePassword();
		}

	};

}();