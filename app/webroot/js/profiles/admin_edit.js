var FormValidation = function () {

	var handleValidation = function() {
		var form = $('#ProfileAdminEditForm');
		var error = $('.alert-danger', form);

		form.validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: true, // focus the last invalid input
			rules: {
				'data[Profile][curriculum]': {
					required: false,
					extension: "doc|docx|pdf|txt|html",
					accept: "application/pdf,application/msword,text/plain,text/html"
				}
			},

			errorPlacement: function (error, element) { // render error placement for each input type
				if (element.parent(".input-group").size() > 0) {
					error.insertAfter(element.parent(".input-group"));
				} else if (element.attr("data-error-container")) {
					error.appendTo(element.attr("data-error-container"));
				} else if (element.parents('.radio-list').size() > 0) {
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) {
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) {
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else if (element.parents('.btn-file').size() > 0) {
					error.insertAfter($('div.fileinput-new.thumbnail'));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit
				error.show();
				App.scrollTo(error, -200);
			},

			highlight: function (element) { // hightlight error inputs
				$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				console.log(label);
				label.closest('.form-group').removeClass('has-error'); // set success class to the control group
				label.remove();
			},

			submitHandler: function (form) {
				error.hide();
				form.submit();
			}
		});
	}

	return {
		//main function to initiate the module
		init: function () {
			handleValidation();
		}

	};
}();