var AdminIndex = function () {

	var handleDataTable = function(tableId) {
		if (!jQuery().dataTable) {
			return;
		}

		// begin first table
		$('#'+tableId+'').dataTable({
			"aLengthMenu": [
				[5, 10, 15, 20, -1],
				[5, 10, 15, 20, "Todos"] // change per page values here
			],
			// set the initial value
			"iDisplayLength": 10,
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "Mostrar _MENU_ registros por página",
				"sZeroRecords": "No hay resultados",
				"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros totales",
				"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
				"sEmptyTable": "No existen configuraciones para esta categoría",
				"sInfoFiltered": "(filtrados de _MAX_ registros totales)",
				"sSearch": "Buscar:",
				"oPaginate": {
					"sPrevious": "Anterior",
					"sNext": "Siguiente"
				}
			},
			"aoColumnDefs": [
				{
					'bSortable': false,
					'aTargets': [2]
				}
			]
		});

		jQuery('#'+tableId+'_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
		jQuery('#'+tableId+'_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
		jQuery('#'+tableId+'_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
	}

	return {
		//main function to initiate the module
		init: function () {
			handleDataTable('contact_data');
			handleDataTable('website_data');
			handleDataTable('social_data');
		}
	};
}();