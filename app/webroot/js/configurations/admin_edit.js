var AdminEdit = function () {

	var settings = {
		fields: {
			coordinates: null,
			directions: null
		},
		center: null,
		zoom: 0,
		map: null,
		geocoder: null,
		mapOptions: null,
		marker: null
	}

	var handleValidation = function() {
		var form = $('#ConfigurationAdminEditForm');
		var error = $('.alert-danger', form);

		form.validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: true, // focus the last invalid input
			rules: {
				'data[Configuration][value]': {
					required: true
				}
			},

			errorPlacement: function (error, element) { // render error placement for each input type
				if (element.parent(".input-group").size() > 0) {
					error.insertAfter(element.parent(".input-group"));
				} else if (element.attr("data-error-container")) {
					error.appendTo(element.attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit
				error.show();
				App.scrollTo(error, -200);
			},

			highlight: function (element) { // hightlight error inputs
				$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				console.log(label);
				label.closest('.form-group').removeClass('has-error'); // set success class to the control group
				label.remove();
			},

			submitHandler: function (form) {
				error.hide();
				form.submit();
			}
		});
	}

	var initGoogleMaps = function() {
		settings.fields.coordinates = document.getElementById('ConfigurationValue');
		settings.fields.directions = document.getElementById('ConfigurationDirections');

		if (settings.fields.coordinates.value) {
			var tempCoordinates = settings.fields.coordinates.value.split(',');
			settings.center = new google.maps.LatLng(tempCoordinates[0], tempCoordinates[1]);
			settings.zoom = 15;
		} else {
			settings.center = new google.maps.LatLng(19.47695, -99.118652);
			settings.zoom = 4;
		}


		settings.geocoder = new google.maps.Geocoder();
		$('#map-canvas').height('450px');

		settings.mapOptions = {
			center: settings.center,
			zoom: settings.zoom,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			streetViewControl: false,
			mapTypeControl: false
		};

		settings.map = new google.maps.Map(document.getElementById("map-canvas"), settings.mapOptions);
		settings.marker = new google.maps.Marker({
			position: settings.center,
			map: settings.map,
			draggable:true
		});

		google.maps.event.addListener(settings.map, 'click', onMapClick);
		google.maps.event.addListener(settings.marker, 'dragend', onMapClick);

		$('#clean-map').bind('click', onClean);
	}

	var onClean = function() {
		settings.fields.coordinates.value = '';
		settings.fields.directions.value = '';
		settings.map.setCenter(settings.center);
		settings.map.setZoom(settings.zoom);
		settings.marker.setPosition(settings.center);
	}

	var onMapClick = function(e) {
		settings.fields.coordinates.value = e.latLng.lat()+', '+e.latLng.lng();
		settings.marker.setPosition(e.latLng);
		settings.map.setCenter(e.latLng);
		codeLatLng(e.latLng);
	}

	var codeLatLng = function(point) {
		settings.geocoder.geocode({'latLng': point}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					settings.fields.directions.value = results[0].formatted_address;
				} else {
					alert('No se encuentran resultados');
				}
			} else {
				alert('No se puede localizar este punto en el mapa');
			}
		});
	}

	return {
		//main function to initiate the module
		init: function () {
			handleValidation();
			if(document.getElementById('ConfigurationIsMap')){
				initGoogleMaps();
			}
		}
	};
}();