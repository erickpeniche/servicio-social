var AdminIndex = function () {

	var handleDeleteButton = function () {
		$('a#delete-btn').on('click', function (e) {
			e.preventDefault();
			var id = $(this).data('id');
			var url = $(this).attr('href');
			bootbox.dialog({
				message: "Confirmas que deseas eliminar la imagen de portada?",
				title: "Eliminar Portada",
				buttons: {
					cancel: {
						label: 'Cancelar',
						className: 'default',
						callback: function() {
							return true;
						}
					},
					confirm: {
						label: '<i class="fa fa-trash-o"></i> Eliminar',
						className: "btn-danger",
						callback: function(){
							$('<form action="' + url + '" method="post"></form>').appendTo('body').submit();
							return true;
						}
					}
				}
			});
		});
	}

	return {
		//main function to initiate the module
		init: function () {
			$('.mix-grid').mixitup();
			handleDeleteButton();
		}

	};
}();