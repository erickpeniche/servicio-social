var AdminIndex = function () {

	var handleDataTable = function() {
		if (!jQuery().dataTable) {
			return;
		}

		// begin first table
		$('#posts_table').dataTable({
			"aLengthMenu": [
				[5, 10, 15, 20, -1],
				[5, 10, 15, 20, "Todos"] // change per page values here
			],
			// set the initial value
			"iDisplayLength": 10,
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "Mostrar _MENU_ registros por página",
				"sZeroRecords": "No hay resultados",
				"sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros totales",
				"sInfoEmpty": "Mostrando 0 a 0 de 0 registros",
				"sEmptyTable": "No existen posts, haz click en el boton de AGREGAR para añadir una Post",
				"sInfoFiltered": "(filtrados de _MAX_ registros totales)",
				"sSearch": "Buscar:",
				"oPaginate": {
					"sPrevious": "Anterior",
					"sNext": "Siguiente"
				}
			},
			"aoColumnDefs": [
				{
					'bSortable': false,
					'aTargets': [4]
				}
			]
		});

		jQuery('#posts_table_wrapper .dataTables_filter input').addClass("form-control input-medium input-inline"); // modify table search input
		jQuery('#posts_table_wrapper .dataTables_length select').addClass("form-control input-xsmall"); // modify table per page dropdown
		jQuery('#posts_table_wrapper .dataTables_length select').select2(); // initialize select2 dropdown
	}

	var handleDeleteButton = function () {
		$('a#delete-btn').on('click', function (e) {
			e.preventDefault();
			var id = $(this).data('id');
			var url = $(this).attr('href');
			bootbox.dialog({
				message: "Confirmas que deseas eliminar el Post <strong>" + $(this).data('name') + "</strong>?",
				title: "Eliminar Post",
				buttons: {
					cancel: {
						label: 'Cancelar',
						className: 'default',
						callback: function() {
							return true;
						}
					},
					confirm: {
						label: '<i class="fa fa-trash-o"></i> Eliminar',
						className: "btn-danger",
						callback: function(){
							$('<form action="' + url + '" method="post"></form>').appendTo('body').submit();
							return true;
						}
					}
				}
			});
		});
	}

	return {
		//main function to initiate the module
		init: function () {
			handleDataTable();
			handleDeleteButton();
		}
	};
}();