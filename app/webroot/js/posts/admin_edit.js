var AdminEdit = function () {

	var handleValidation = function() {
		var form = $('#PostAdminEditForm');
		var error = $('.alert-danger', form);

		form.validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: true, // focus the last invalid input
			ignore: "input:hidden:not(input:hidden.required), #PostImage",
			rules: {
				'data[Post][title]': {
					minlength: 2,
					required: true
				},
				'data[Post][content]': {
					required: function(textarea) {
						CKEDITOR.instances[textarea.id].updateElement(); // update textarea
						var editorcontent = textarea.value;
						return editorcontent.length === 0;
					}
				}
			},

			errorPlacement: function (error, element) { // render error placement for each input type
				if (element.parent(".input-group").size() > 0) {
					error.insertAfter(element.parent(".input-group"));
				} else if (element.attr("data-error-container")) {
					error.appendTo(element.attr("data-error-container"));
				} else if (element.parents('.radio-list').size() > 0) {
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) {
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) {
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else if (element.parents('.btn-file').size() > 0) {
					error.insertAfter($('div.fileinput-new.thumbnail'));
				} else {
					console.log(element);
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},

			invalidHandler: function (event, validator) { //display error alert on form submit
				error.show();
				App.scrollTo(error, -200);
			},

			highlight: function (element) { // hightlight error inputs
				$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
			},

			unhighlight: function (element) { // revert the change done by hightlight
				$(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
			},

			success: function (label) {
				label.closest('.form-group').removeClass('has-error'); // set success class to the control group
				label.remove();
			},

			submitHandler: function (form) {
				error.hide();
				form.submit();
			}
		});
	}

	var handleTagsInput = function () {
		if (!jQuery().tagsInput) {
			return;
		}
		$('#PostTags').tagsInput({
			width: 'auto',
			defaultText: 'agregar',
		});
	}

	return {
		//main function to initiate the module
		init: function () {
			handleValidation();
			handleTagsInput();
		}

	};
}();