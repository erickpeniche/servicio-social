<?php
App::uses('AppModel', 'Model');
/**
 * DefaultArchive Model
 *
 * @property Project $Project
 * @property Research $Research
 */
class DefaultArchive extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Project' => array(
			'className' => 'Project',
			'foreignKey' => 'foreign_key',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Research' => array(
			'className' => 'Research',
			'foreignKey' => 'foreign_key',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $validate = array(
		'file' => array(
			'uploadError' => array(
				'rule' => array('uploadError'),
				'message' => 'No se pudo subir la imagen'
			),
			'fileSize' => array(
				'rule' => array('fileSize', '<=', '20MB'),
				'message' => 'La imagen debe pesar 20MB o menos'
			)
		)
	);

	public function beforeDelete() {
		$document = $this->findById($this->id);

		if (!empty($document['DefaultArchive']['file'])) {
			$documentDirectory = WWW_ROOT . 'files' . DS . 'documents'. DS;
			unlink($documentDirectory . DS . strtolower($document['DefaultArchive']['model']) . '_' . $document['DefaultArchive']['file']);
		}
		return true;
	}
}
