<?php
App::uses('AppModel', 'Model');
/**
 * MainImage Model
 *
 */
class MainImage extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'image' => array(
			'uploadError' => array(
				'rule' => array('uploadError'),
				'message' => 'No se pudo subir la imagen'
			),
			'fileType' => array(
				'rule' => array('mimeType', array('image/jpeg', 'image/png', 'image/gif', 'image/jpg')),
				'message' => 'Formato de archivo no admitido'
			),
			'fileSize' => array(
				'rule' => array('fileSize', '<=', '20MB'),
				'message' => 'La imagen debe pesar 20MB o menos'
			)
		),
	);

	var $images = array( //mode: raw[default], resizeToWidth, resizeToHeight, resizeToWidthHeight, crop, resizeAndCrop
		'main_slider' => array(
			'mode' => 'resizeAndCrop',
			'width' => 1920,
			'height' => 445,
			'align' => 'center', //left, center, right
			'valign' => 'middle', //top, middle, bottom
			'thumbnail' => false,
			'view' => true
		),
		'main_index' => array(
			'mode' => 'resizeAndCrop',
			'width' => 800,
			'height' => 600,
			'align' => 'center', //left, center, right
			'valign' => 'middle', //top, middle, bottom
			'thumbnail' => false,
			'view' => true
		),
		'main_image' => array(
			'mode' => 'raw',
			'width' => 800,
			'height' => 600,
			'align' => 'center', //left, center, right
			'valign' => 'middle', //top, middle, bottom
			'thumbnail' => false,
			'view' => true
		)
	);

	public function beforeSave() {
		// Link value
		if (isset($this->data['MainImage']['link'])) {
			$protocols = array('http', 'https', 'ftp');
			$length = count($protocols);
			for ($j = 0; $j < $length; $j++) {
				if (stripos($this->data['MainImage']['link'], $protocols[$j]) !== false) {
					$this->data['MainImage']['link'] = $this->data['MainImage']['link'];
					break;
				}

				if ($j == $length - 1) {
					if(!empty($this->data['MainImage']['link'])){
						$this->data['MainImage']['link'] = 'http://' . $this->data['MainImage']['link'];
					} else{
						$this->data['MainImage']['link'] = null;
					}
				}
			}
		}

		return true;
	}

	public function beforeDelete() {
		$image = $this->findById($this->id);

		if (!empty($image['MainImage']['image'])) {
			if (isset($this->images)) {
				$setup = $this->images;
				$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;
				foreach($setup as $key => $set) {
					unlink($imageDirectory . DS . $key . '_' . $image['MainImage']['image']);
				}
			} else {
				debug('tienes que tener un set de imagenes');
				exit();
			}
		}
		return true;
	}
}
