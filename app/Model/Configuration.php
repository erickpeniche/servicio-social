<?php
App::uses('AppModel', 'Model');
/**
 * Configuration Model
 *
 */
class Configuration extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'eng_value' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Este campo es requerido"><i class="icon-exclamation-sign"></i></span>'
			),
		),
		'chi_value' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'<span class="input-error tooltips" data-original-title="Este campo es requerido"><i class="icon-exclamation-sign"></i></span>'
			),
		)
	);

	function beforeSave() {
		// Link value
		if ((isset($this->data['Configuration']['value']) && !empty($this->data['Configuration']['value'])) && (isset($this->data['Configuration']['category']) && $this->data['Configuration']['category']==3)) {
			$protocols = array('http', 'https', 'ftp');
			$length = count($protocols);
			for ($j = 0; $j < $length; $j++) {
				if (stripos($this->data['Configuration']['value'], $protocols[$j]) !== false) {
					$this->data['Configuration']['value'] = $this->data['Configuration']['value'];
					break;
				}

				if ($j == $length - 1) {
					if(!empty($this->data['Configuration']['value'])){
						$this->data['Configuration']['value'] = 'http://' . $this->data['Configuration']['value'];
					} else{
						$this->data['Configuration']['value'] = null;
					}
				}
			}
		}
		return true;
	}
}
