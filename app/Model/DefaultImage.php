<?php
App::uses('AppModel', 'Model');
/**
 * DefaultImage Model
 *
 * @property Project $Project
 * @property Research $Research
 */
class DefaultImage extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Project' => array(
			'className' => 'Project',
			'foreignKey' => 'foreign_key',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Research' => array(
			'className' => 'Research',
			'foreignKey' => 'foreign_key',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $validate = array(
		'file' => array(
			'uploadError' => array(
				'rule' => array('uploadError'),
				'message' => 'No se pudo subir la imagen'
			),
			'fileType' => array(
				'rule' => array('mimeType', array('image/jpeg', 'image/png', 'image/gif', 'image/jpg')),
				'message' => 'Formato de archivo no admitido'
			),
			'fileSize' => array(
				'rule' => array('fileSize', '<=', '20MB'),
				'message' => 'La imagen debe pesar 20MB o menos'
			)
		)
	);

	public function beforeDelete() {
		$image = $this->findById($this->id);

		$model = $image['DefaultImage']['model'];
		App::import('model', $model);
		$this->{$model} = new $model();

		if (!empty($image['DefaultImage']['file'])) {
			if (isset($this->{$model}->images)) {
				$setup = $this->{$model}->images;
				$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;
				foreach($setup as $key => $set) {
					unlink($imageDirectory . DS . $key . '_' . $image['DefaultImage']['file']);
				}
			} else {
				$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;
				unlink($imageDirectory . DS . $image['DefaultImage']['file']);
				unlink($imageDirectory . DS . $image['DefaultImage']['file']);
			}
		}
		return true;
	}
}
