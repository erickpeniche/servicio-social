<?php
App::uses('AppModel', 'Model');
/**
 * Publication Model
 *
 * @property User $User
 */
class Publication extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

	public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message'=>'Este campo es requerido'
			),
		),
		'file' => array(
			'uploadError' => array(
				'rule' => array('uploadError'),
				'message' => 'Selecciona un archivo',
			),
			'fileType' => array(
				'rule' => array('mimeType', array('application/pdf', 'application/msword', 'text/plain', 'text/html')),
				'message' => 'Formato de archivo no admitido'
			),
			'fileSize' => array(
				'rule' => array('fileSize', '<=', '10MB'),
				'message' => 'El documento debe pesar 10MB o menos'
			)
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function beforeValidate() {
		if (isset($this->data['Publication']) && !empty($this->data['Publication']['edit'])) {
			$file = isset($this->data['Publication']['file']) ? $this->data['Publication']['file'] : null;
			if ($file && $file['error'] === 4) {
				unset($this->data['Publication']['file']);
			}
			unset($this->data['Publication']['edit']);
		}

		return true;
	}

	public function beforeSave() {
		$loggedUser = CakeSession::read('Auth.User');
		$this->data['Publication']['user_id'] = $loggedUser['id'];

		$return = false;
		$document = null;
		$erase = false;
		$publication = null;

		if (!empty($this->id)) {
			$publication = $this->findById($this->data['Publication']['id']);
			$document = $publication['Publication']['publication_key'];
		}

		//File upload
		$file = isset($this->data['Publication']['file']) ? $this->data['Publication']['file'] : null;
		$fileDir = WWW_ROOT . 'files' . DS . 'documents';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					try {
						$fileName = md5($file['tmp_name'] . (rand() * 100000));
						$fileSize = filesize($file['tmp_name']);
						$fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);

						if (move_uploaded_file($file['tmp_name'], $fileDir . DS . 'pub_' . $fileName .'.'. $fileExtension)) {
							$return = true;
						} else {
							$return = false;
						}

						$this->data['Publication']['publication_size'] = $fileSize;
						$this->data['Publication']['publication_key'] = $fileName . '.' .  $fileExtension;
						$this->data['Publication']['publication_name'] = $file['name'];

						$erase = true;
					} catch (BitmapException $e) {
						$this->invalidate('image', 'bitmapError');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('file', 'uploadError');
			}
		} else {
			if (isset($this->data['Publication']['erase']) && $this->data['Publication']['erase']) {
				$this->data['Publication']['file'] = null;
			} else {
				unset($this->data['Publication']['file']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($document) && ((isset($this->data['Publication']['erase']) && $this->data['Publication']['erase']) || $erase)) {
			unlink($fileDir . DS . 'pub_' . $document);
		}

		return $return;
	}

	function beforeDelete() {
		$publication = $this->findById($this->data['Publication']['id']);

		if (!empty($publication['Publication']['publication_key'])) {
			$imageDirectory = WWW_ROOT . 'files' . DS . 'documents' . DS;
			unlink($imageDirectory . 'pub_' . $publication['Publication']['publication_key']);
		}

		return true;
	}
}
