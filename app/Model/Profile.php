<?php
App::uses('AppModel', 'Model');
/**
 * Profile Model
 *
 * @property Media $File
 * @property User $User
 */
class Profile extends AppModel {
	public $name = 'Profile';

	public $alias = 'Profile';

	public $actsAs = array('Containable');

	public $validate = array(
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message'=>'Introduzca una dirección de correo electrónico válida',
				'on' => 'update'
			),
		),
		'curriculum' => array(
			'uploadError' => array(
				'rule' => array('uploadError'),
				'message' => 'No se pudo subir el currículum'
			),
			'fileType' => array(
				'rule' => array('mimeType', array('application/pdf', 'application/msword', 'text/plain', 'text/html')),
				'message' => 'Formato de archivo no admitido'
			),
			'fileSize' => array(
				'rule' => array('fileSize', '<=', '10MB'),
				'message' => 'El currículum debe pesar 10MB o menos'
			)
		)
	);

	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function beforeValidate() {
		$file = isset($this->data['Profile']['curriculum']) ? $this->data['Profile']['curriculum'] : null;
		if ($file && $file['error'] === 4) {
			unset($this->data['Profile']['curriculum']);
		}

		return true;
	}

	public function beforeSave() {
		$return = false;
		$document = null;
		$erase = false;
		$profile = null;

		if (!empty($this->id)) {
			$profile = $this->findById($this->data['Profile']['id']);
			$document = $profile['Profile']['curriculum_key'];
		}

		//File upload
		$file = isset($this->data['Profile']['curriculum']) ? $this->data['Profile']['curriculum'] : null;
		$fileDir = WWW_ROOT . 'files' . DS . 'documents';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					try {
						$fileName = md5($file['tmp_name'] . (rand() * 100000));
						$fileSize = filesize($file['tmp_name']);
						$fileExtension = pathinfo($file['name'], PATHINFO_EXTENSION);

						if (move_uploaded_file($file['tmp_name'], $fileDir . DS . 'cv_' . $fileName .'.'. $fileExtension)) {
							$return = true;
						} else {
							$return = false;
						}

						$this->data['Profile']['curriculum_size'] = $fileSize;
						$this->data['Profile']['curriculum_key'] = $fileName . '.' .  $fileExtension;
						$this->data['Profile']['curriculum_name'] = $file['name'];

						$erase = true;
					} catch (BitmapException $e) {
						$this->invalidate('image', 'bitmapError');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('file', 'uploadError');
			}
		} else {
			if (isset($this->data['Profile']['erase']) && $this->data['Profile']['erase']) {
				$this->data['Profile']['curriculum'] = null;
			} else {
				unset($this->data['Profile']['curriculum']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($document) && ((isset($this->data['Profile']['erase']) && $this->data['Profile']['erase']) || $erase)) {
			unlink($fileDir . DS . 'cv_' . $document);
		}

		return $return;
	}

	function beforeDelete() {
		$profile = $this->findById($this->data['Profile']['id']);

		if (!empty($profile['Profile']['curriculum_key'])) {
			$imageDirectory = WWW_ROOT . 'files' . DS . 'documents' . DS;
			unlink($imageDirectory . 'cv_' . $profile['Profile']['curriculum_key']);
		}

		return true;
	}
}
