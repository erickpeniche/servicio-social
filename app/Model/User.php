<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Group $Group
 */
class User extends AppModel {

	public $name = 'User';

	public $actsAs = array('Containable');

	public $displayField = 'name';

	public $virtualFields = array('fullname' => 'CONCAT(User.name, " ", User.paternal_surname, " ", User.maternal_surname)');

	public $hasOne = array(
		'Profile' => array(
			'className' => 'Profile',
			'foreignKey' => 'user_id',
			'dependent' => true
		)
	);

	public $hasMany = array(
		'Publication' => array(
			'className' => 'Publication',
			'foreignKey' => 'user_id',
			'dependent' => true,
			'conditions' => array('Publication.public' => true)
		)
	);

	public $validate = array(
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message'=>'Introduzca una dirección de correo electrónico válida'
			),
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'Este campo es requerido'
			),
			'unique' => array(
				'rule' => array('isUnique'),
				'message' => 'Este correo electrónico ya se encuentra registrado'
			)
		),
		'password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'Este campo es requerido'
			)
		),
		'new_password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message'=>'Este campo es requerido'
			),
			'different' => array(
				'rule' => array('isNewPassword'),
				'message' => 'La contraseña debe ser diferente a la anterior',
				'on' => 'update'
			),
			'minlength' => array(
				'rule' => array('minlength', 8),
				'message' => 'La contraseña debe contener al menos 8 caracteres'
			),
			'maxlength' => array(
				'rule' => array('maxlength', 16),
				'message' => 'La contraseña debe contener hasta 16 caracteres'
			),
		),
		'password_confirm' => array(
			'match'=>array(
				'rule' => array('isEqualToField', 'new_password'),
				'message' => 'Las contraseñas no coinciden'
			)
		),
		'_current_password' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo es requerido'
			),
			'isCurrentPassword' => array(
				'rule' => 'isCurrentPassword',
				'message' => 'La contraseña actual es incorrecta'
			)
		),
		'title' => array(
			'maxLenth' => array(
				'rule' => array('maxLength', 8),
				'message' => 'El título no debe ser mayor a 8 caracteres',
				'required' => false
			),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo es requerido'
			),
		),
		'paternal_surname' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo es requerido'
			),
		),
		'maternal_surname' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Este campo es requerido'
			),
		),
		'birthday' => array(
			'date' => array(
				'rule' => array('date', 'ymd'),
				'message' => 'Fecha de nacimiento inválida',
				'allowEmpty' => true
			),
		),
		'image' => array(
			'uploadError' => array(
				'rule' => array('uploadError'),
				'message' => 'No se pudo subir la imagen'
			),
			'fileType' => array(
				'rule' => array('mimeType', array('image/jpeg', 'image/png', 'image/gif', 'image/jpg')),
				'message' => 'Formato de archivo no admitido'
			),
			'fileSize' => array(
				'rule' => array('fileSize', '<=', '5MB'),
				'message' => 'La imagen debe pesar 5MB o menos'
			)
		)
	);

	function isNewPassword($data){
		$user = $this->findById($this->data['User']['id']);
		return !(AuthComponent::password($data['new_password'], null, true) == $user['User']['password']);
	}

	public function isCurrentPassword($data) {
		$user = $this->findById(SessionComponent::read('Auth.User.id'), 'User.password');
		return $user['User']['password'] == AuthComponent::password($data['_current_password']);
	}

	function beforeValidate(){
		if (isset($this->data['User']['image']) && $this->data['User']['image']['error'] == 4) {
			unset($this->data['User']['image']);
		}
	}

	function beforeSave() {
		if (isset($this->data['User']['password'])) {
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password'], null, true);
		}

		if (isset($this->data['User']['new_password'])) {
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['new_password'], null, true);
			unset($this->data['User']['new_password']);
			unset($this->data['User']['password_confirm']);
		}

		if (isset($this->data['User']['_current_password'])) {
			unset($this->data['User']['_current_password']);
		}

		$return = false;
		$image = null;
		$erase = false;
		$user = null;

		if (!empty($this->id)) {
			$user = $this->find('first', array('conditions' => array('User.id' => $this->data['User']['id'])));
			$image = $user['User']['image'];
		}

		//File upload
		$file = isset($this->data['User']['image']) ? $this->data['User']['image'] : null;
		$fileDir = WWW_ROOT . 'img' . DS . 'images';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					App::import('Vendor', 'Bitmap');
					App::import('Vendor', 'BitmapException');

					if (Bitmap::isImage($file['tmp_name'])) {

						try {
							$fileName = md5($file['tmp_name'] . (rand() * 100000));
							$imageSize = getimagesize($file['tmp_name']);
							$width = $imageSize[0];
							$height = $imageSize[1];

							$img = new Bitmap($file['tmp_name']);
							$img->open();
							$img->save($fileDir, 'user_img_' . $fileName);
							$img->dispose();
							$fileExtension = $img->getExtension();

							$set['height'] = 800;
							$set['width'] = 800;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$profile = new Bitmap($file['tmp_name']);
							$profile->open();
							$profile->resizeAndCrop2($imageSize, $set);
							$profile->save($fileDir, 'user_profile_' . $fileName);
							$profile->dispose();

							$set['height'] = 29;
							$set['width'] = 29;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$avatar = new Bitmap($file['tmp_name']);
							$avatar->open();
							$avatar->resizeAndCrop2($imageSize, $set);
							$avatar->save($fileDir, 'avatar_' . $fileName);
							$avatar->dispose();

							$this->data['User']['size'] = $file['size'];
							$this->data['User']['image'] = $fileName . '.' .  $fileExtension;
							$return = true;
							$erase = true;
						} catch (BitmapException $e) {
							$this->invalidate('image', 'bitmapError');
						}
					} else {
						$this->invalidate('image', 'unsupportedType');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('image', 'uploadError');
			}
		} else {
			if (isset($this->data['User']['erase']) && $this->data['User']['erase']) {
				$this->data['User']['image'] = null;
			} else {
				unset($this->data['User']['image']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($image) && ((isset($this->data['User']['erase']) && $this->data['User']['erase']) || $erase)) {
			unlink($fileDir . DS . 'avatar_' . $image);
			unlink($fileDir . DS . 'user_img_' . $image);
			unlink($fileDir . DS . 'user_profile_' . $image);
		}

		return $return;
	}

	function beforeDelete() {
		$user = $this->find('first', array('conditions' => array('User.id' => $this->id), 'contain' => false));

		if (!empty($user['User']['image'])) {
			$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;
			unlink($imageDirectory. 'avatar_' . $user['User']['image']);
			unlink($imageDirectory. 'user_img_' . $user['User']['image']);
			unlink($imageDirectory. 'user_profile_' . $user['User']['image']);
		}

		return true;
	}
}
