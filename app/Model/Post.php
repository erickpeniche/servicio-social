<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 * @property User $User
 * @property Comment $Comment
 */
class Post extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message'=>'Este campo es requerido'
			),
		),
		'content' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message'=>'Este campo es requerido'
			),
		),
		'image' => array(
			'uploadError' => array(
				'rule' => array('uploadError'),
				'message' => 'No se pudo subir la imagen'
			),
			'fileType' => array(
				'rule' => array('mimeType', array('image/jpeg', 'image/png', 'image/gif', 'image/jpg')),
				'message' => 'Formato de archivo no admitido'
			),
			'fileSize' => array(
				'rule' => array('fileSize', '<=', '5MB'),
				'message' => 'La imagen debe pesar 5MB o menos'
			)
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	/*public $hasMany = array(
		'Comment' => array(
			'className' => 'Comment',
			'foreignKey' => 'post_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);*/

	public function beforeValidate(){
		if (isset($this->data['Post']['image']) && $this->data['Post']['image']['error'] == 4) {
			unset($this->data['Post']['image']);
		}
	}

	public function beforeSave() {
		$loggedUser = CakeSession::read('Auth.User');
		$this->data['Post']['user_id'] = $loggedUser['id'];

		$return = false;
		$image = null;
		$erase = false;
		$post = null;

		if (!empty($this->id)) {
			$post = $this->find('first', array('conditions' => array('Post.id' => $this->data['Post']['id'])));
			$image = $post['Post']['image'];
		}

		//File upload
		$file = isset($this->data['Post']['image']) ? $this->data['Post']['image'] : null;
		$fileDir = WWW_ROOT . 'img' . DS . 'images';
		$fileName = '';
		$fileExtension = '';

		//A file was uploaded
		if (!empty($file) && !empty($file['tmp_name']) && $file['error'] != 4) {
			//There is no error, the file was uploaded with success
			if ($file['error'] == 0) {

				//Tells whether the file was uploaded via HTTP POST
				if (is_uploaded_file($file['tmp_name'])) {
					App::import('Vendor', 'Bitmap');
					App::import('Vendor', 'BitmapException');

					if (Bitmap::isImage($file['tmp_name'])) {

						try {
							$fileName = md5($file['tmp_name'] . (rand() * 100000));
							$imageSize = getimagesize($file['tmp_name']);
							$width = $imageSize[0];
							$height = $imageSize[1];

							$img = new Bitmap($file['tmp_name']);
							$img->open();
							$img->save($fileDir, 'post_img_' . $fileName);
							$img->dispose();

							$set['height'] = 150;
							$set['width'] = 200;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$img_edt = new Bitmap($file['tmp_name']);
							$img_edt->open();
							$img_edt->resizeAndCrop2($imageSize, $set);
							$img_edt->save($fileDir, 'post_img_edt_' . $fileName);
							$img_edt->dispose();

							$set['height'] = 381;
							$set['width'] = 946;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$view = new Bitmap($file['tmp_name']);
							$view->open();
							$view->resizeAndCrop2($imageSize, $set);
							$view->save($fileDir, 'post_view_' . $fileName);
							$view->dispose();

							$set['height'] = 600;
							$set['width'] = 800;
							$set['align'] = 'center';
							$set['valign'] = 'middle';

							$list = new Bitmap($file['tmp_name']);
							$list->open();
							$list->resizeAndCrop2($imageSize, $set);
							$list->save($fileDir, 'post_list_' . $fileName);
							$list->dispose();

							$fileExtension = $list->getExtension();

							$this->data['Post']['image'] = $fileName . '.' .  $fileExtension;
							$return = true;
							$erase = true;
						} catch (BitmapException $e) {
							$this->invalidate('image', 'bitmapError');
						}
					} else {
						$this->invalidate('image', 'unsupportedType');
					}
				} else {
					$this->invalidate('image', 'maliciousUpload');
				}
			} else {
				//File upload error. See http://us3.php.net/manual/en/features.file-upload.errors.php
				$this->invalidate('image', 'uploadError');
			}
		} else {
			if (isset($this->data['Post']['erase']) && $this->data['Post']['erase']) {
				$this->data['Post']['image'] = null;
			} else {
				unset($this->data['Post']['image']);
			}
			$return = true;
		}

		// REMOVE PREVIOUS LOGO
		if (!empty($image) && ((isset($this->data['Post']['erase']) && $this->data['Post']['erase']) || $erase)) {
			unlink($fileDir . DS . 'post_img_' . $image);
			unlink($fileDir . DS . 'post_img_edt_' . $image);
			unlink($fileDir . DS . 'post_view_' . $image);
			unlink($fileDir . DS . 'post_list_' . $image);
		}

		if ($this->data['Post']['published'] == 1) {
			if (isset($post) && $post['Post']['published'] == 1) {
				$this->data['Post']['published_date'] = $post['Post']['published_date'];
			} else {
				$this->data['Post']['published_date'] = date("Y-m-d H:i:s");
			}
		}

		return $return;
	}

	public function beforeDelete() {
		$post = $this->findById($this->id);

		if (!empty($post['Post']['image'])) {
			$imageDirectory = WWW_ROOT . 'img' . DS . 'images' . DS;
			unlink($imageDirectory. 'post_img_' . $post['Post']['image']);
			unlink($imageDirectory. 'post_img_edt_' . $post['Post']['image']);
			unlink($imageDirectory. 'post_view_' . $post['Post']['image']);
			unlink($imageDirectory. 'post_list_' . $post['Post']['image']);
		}

		return true;
	}
}
