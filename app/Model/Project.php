<?php
App::uses('AppModel', 'Model');
/**
 * Project Model
 *
 * @property User $User
 * @property Media $Images
 * @property Media $Archives
 */
class Project extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message'=>'Este campo es requerido'
			),
		),
		'description' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message'=>'Este campo es requerido'
			),
		),
	);

	// Extra Images Setup
	var $images = array( //mode: raw[default], resizeToWidth, resizeToHeight, resizeToWidthHeight, crop, resizeAndCrop
		'project_preview' => array(
			'mode' => 'resizeAndCrop',
			'width' => 800,
			'height' => 600,
			'align' => 'center', //left, center, right
			'valign' => 'middle', //top, middle, bottom
			'thumbnail' => false,
			'view' => true
		),
		'project' => array(
			'mode' => 'raw',
			'width' => 800,
			'height' => 600,
			'align' => 'center', //left, center, right
			'valign' => 'middle', //top, middle, bottom
			'thumbnail' => false,
			'view' => true
		)
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $hasMany = array(
		'DefaultImage' => array(
			'className' => 'DefaultImage',
			'foreignKey' => 'foreign_key',
			'dependent' => false,
			'conditions' => array('model'=>'Project')
		),
		'DefaultArchive' => array(
			'className' => 'DefaultArchive',
			'foreignKey' => 'foreign_key',
			'dependent' => false,
			'conditions' => array('model'=>'Project')
		)
	);

	public function beforeSave() {
		$loggedUser = CakeSession::read('Auth.User');
		$this->data['Project']['user_id'] = $loggedUser['id'];

		return true;
	}

}
