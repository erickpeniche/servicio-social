<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	//Validation Method
	public function isEqualToField($data, $field) {
		if (!isset($this->data[$this->name][$field])) {
			return false;
		}

		$value = array_pop(array_keys($data));

		return ($data[$value] == $this->data[$this->name][$field]);
	}

	public function onlyIf($check, $relatedField, $validationRuleForRelatedField, $ruleForField) {
		$ruleForRelatedField = new CakeValidationRule(array('rule' => $validationRuleForRelatedField));
		$modelValidationMethods = $this->validator()->getMethods();

		if(
			$ruleForRelatedField->process($relatedField, $this->data[$this->alias], $modelValidationMethods) &&
			$ruleForRelatedField->isValid()
		)
		{
			$conditionalRule = new CakeValidationRule(array('rule' => $ruleForField));

			foreach ($check as $fieldName => $fieldValue) {
				return $conditionalRule->process($fieldName, $this->data[$this->alias], $modelValidationMethods) && $conditionalRule->isValid();
			}
		}

		return true;
	}

	public function onlyIfNot($check, $relatedField, $validationRuleForRelatedField, $ruleForField) {
		$ruleForRelatedField = new CakeValidationRule(array('rule' => $validationRuleForRelatedField));
		$modelValidationMethods = $this->validator()->getMethods();

		if(
			$ruleForRelatedField->process($relatedField, $this->data[$this->alias], $modelValidationMethods) &&
			!$ruleForRelatedField->isValid()
		)
		{
			$conditionalRule = new CakeValidationRule(array('rule' => $ruleForField));

			foreach ($check as $fieldName => $fieldValue) {
				return $conditionalRule->process($fieldName, $this->data[$this->alias], $modelValidationMethods) && $conditionalRule->isValid();
			}
		}

		return true;
	}
}
