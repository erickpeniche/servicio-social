<?php
/**
 * Bake Template for Controller action generation.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.actions
 * @since         CakePHP(tm) v 1.3
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

	public function <?php echo $admin ?>index() {
		$this->layout = 'admin/default';
		$viewTitle = '<?php echo $pluralName; ?>';

		/*$loggedUser = $this->Session->read('Auth.User');
		if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/custom_flash_security');
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		$this->set('<?php echo $pluralName ?>', $this-><?php echo $currentModelName ?>->find('all'));
		$this->set('activeMenu', '<?php echo strtolower($pluralName); ?>');
		$this->set(compact('viewTitle'));
	}

	public function <?php echo $admin ?>view($id = null, $name = null) {
		$this->layout = 'admin/default';
		$viewTitle = 'Ver';
		$viewSubTitle = '<?php echo $singularHumanName; ?>';

		/*$loggedUser = $this->Session->read('Auth.User');
		$ownAccount = $id == $loggedUser['id'] ? true : false;

		if(!AppController::isSuperUser() && !$ownAccount){
			$this->Session->setFlash('No cuentas con los permisos para acceder a esta sección.', 'admin/flash/toastr', array('title'=>'Acceso denegado!', 'type'=>'warning'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		if (!$this-><?php echo $currentModelName; ?>->exists($id)) {
			$this->Session->setFlash('El <?php echo strtolower($singularHumanName); ?> no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}
		$options = array('conditions' => array('<?php echo $currentModelName; ?>.' . $this-><?php echo $currentModelName; ?>->primaryKey => $id));
		$this->set('<?php echo $singularName; ?>', $this-><?php echo $currentModelName; ?>->find('first', $options));
		$this->set(compact('viewTitle', 'viewSubTitle'));
		$this->set('activeMenu', '<?php echo strtolower($pluralName); ?>');
	}

<?php $compact = array(); ?>
	public function <?php echo $admin ?>add() {
		$this->layout = 'admin/default';
		$viewTitle = 'Nuevo';
		$viewSubTitle = '<?php echo $singularHumanName; ?>';

		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		if ($this->request->is('post')) {
			$this-><?php echo $currentModelName; ?>->create();
			if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) {
<?php if ($wannaUseSession): ?>
				$this->Session->setFlash(__('El <?php echo strtolower($singularHumanName); ?> <strong>' . $this->request->data['<?php echo $currentModelName; ?>']['name'] . '</strong> ha sido creado.'), 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El <?php echo strtolower($singularHumanName); ?> no se pudo crear.', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error')));
<?php else: ?>
				return $this->flash(__('The <?php echo strtolower($singularHumanName); ?> has been saved.'), array('action' => 'index'));
<?php endif; ?>
			}
		}
<?php
	foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
		foreach ($modelObj->{$assoc} as $associationName => $relation):
			if (!empty($associationName)):
				$otherModelName = $this->_modelName($associationName);
				$otherPluralName = $this->_pluralName($associationName);
				echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
				$compact[] = "'{$otherPluralName}'";
			endif;
		endforeach;
	endforeach;
	if (!empty($compact)):
		echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
	endif;
?>
		$this->set(compact('viewTitle', 'viewSubTitle'));
		$this->set('activeMenu', '<?php echo strtolower($pluralName); ?>');
	}

<?php $compact = array(); ?>
	public function <?php echo $admin; ?>edit($id = null, $name = null) {
		$this->layout = 'admin/default';
		$viewTitle = 'Editar';
		$viewSubTitle = '<?php echo $singularHumanName; ?>';

		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}*/

		if (!$this-><?php echo $currentModelName; ?>->exists($id)) {
			$this->Session->setFlash('El <?php echo strtolower($singularHumanName); ?> no existe', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			return $this->redirect(array('action' => 'index', 'admin' => true));
		}

		if ($this->request->is(array('post', 'put'))) {
			if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) {
<?php if ($wannaUseSession): ?>
				$this->Session->setFlash(__('El <?php echo strtolower($singularHumanName); ?> ha sido editado.'), 'admin/flash/toastr', array('title'=>'Éxito!', 'type'=>'success'));
				return $this->redirect(array('action' => 'view', $id, Format::clean($<?php echo strtolower($singularHumanName); ?>['<?php echo $currentModelName; ?>']['name'])));
			} else {
				$this->Session->setFlash(__('El <?php echo strtolower($singularHumanName); ?> no se pudo editar.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
<?php else: ?>
				return $this->flash(__('The <?php echo strtolower($singularHumanName); ?> has been saved.'), array('action' => 'index'));
<?php endif; ?>
			}
		} else {
			$options = array('conditions' => array('<?php echo $currentModelName; ?>.' . $this-><?php echo $currentModelName; ?>->primaryKey => $id));
			$this->request->data = $this-><?php echo $currentModelName; ?>->find('first', $options);
		}
<?php
		foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
			foreach ($modelObj->{$assoc} as $associationName => $relation):
				if (!empty($associationName)):
					$otherModelName = $this->_modelName($associationName);
					$otherPluralName = $this->_pluralName($associationName);
					echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
					$compact[] = "'{$otherPluralName}'";
				endif;
			endforeach;
		endforeach;
		if (!empty($compact)):
			echo "\t\t\$this->set(compact(".join(', ', $compact)."));\n";
		endif;
	?>
		$<?php echo strtolower($singularHumanName); ?> = $this->request->data;
		$this->set(compact('viewTitle', 'viewSubTitle', '<?php echo strtolower($singularHumanName); ?>'));
		$this->set('activeMenu', '<?php echo strtolower($pluralName); ?>');
	}

	public function <?php echo $admin; ?>delete($id = null) {
		/*if(!AppController::isSuperUser()){
			$this->Session->setFlash('No tienes los permisos para acceder a esta sección.', 'admin/flash/gritter', array('title'=>'Acceso denegado!', 'image'=>'/img/flash/security.png'));
			$this->redirect(array('controller' => 'pages', 'action' => 'home', 'admin' => true));
		}
		if ($this->Session->read('Auth.User.id')==$id) {
			$this->Session->setFlash('No puedes realizar esta accion. (No te eliminarías a ti mismo o si?)', 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
			$this->redirect(array('action' => 'index', 'admin' => true));
		}*/

		$this-><?php echo $currentModelName; ?>->id = $id;
		if (!$this-><?php echo $currentModelName; ?>->exists()) {
			throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this-><?php echo $currentModelName; ?>->delete()) {
<?php if ($wannaUseSession): ?>
			$this->Session->setFlash(__('The <?php echo strtolower($singularHumanName); ?> eliminado correctamente.'), 'admin/flash/toastr', array('title'=>'Atención!', 'type'=>'warning'));
		} else {
			$this->Session->setFlash(__('No se pudo eliminar el <?php echo strtolower($singularHumanName); ?>.'), 'admin/flash/toastr', array('title'=>'Error!', 'type'=>'error'));
		}
		return $this->redirect(array('action' => 'index'));
<?php else: ?>
			return $this->flash(__('The <?php echo strtolower($singularHumanName); ?> has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The <?php echo strtolower($singularHumanName); ?> could not be deleted. Please, try again.'), array('action' => 'index'));
		}
<?php endif; ?>
	}
