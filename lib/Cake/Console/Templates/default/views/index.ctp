<?php echo "<?php\n"; ?>
	echo $this->Html->css(array(
		'plugins/select2/select2',
		'plugins/select2/select2-metronic',
		'plugins/data-tables/DT_bootstrap'
		), null, array('inline' => false)
	);

	echo $this->Html->script(array(
		'plugins/select2/select2.min',
		'plugins/data-tables/jquery.dataTables',
		'plugins/data-tables/DT_bootstrap',
		'/js/<?php echo $pluralVar; ?>/admin_index'
		), array('inline' => false)
	);

	$this->Html->addCrumb('<?php echo $pluralVar; ?>',
		array(
			'action' => 'index',
			'controller' => '<?php echo $pluralVar; ?>',
			'admin' => true
		)
	);
<?php echo "?>"; ?>

<div class="row">
	<div class="col-md-12">
		<!-- BEGIN SAMPLE TABLE PORTLET-->
		<div class="portlet box grey">
			<div class="portlet-title">
				<div class="caption"><i class="fa fa-"></i><?php echo $pluralVar; ?></div>
				<div class="actions">
					<?php echo "<?php "; ?> echo $this->Html->link('<i class="fa fa-plus"></i> Agregar</a>',
						array(
							'action' => 'add',
							'controller' => '<?php echo $pluralVar; ?>'
						),
						array(
							'class' => 'btn green',
							'escape' => false)
						);
					<?php echo "?>\n"; ?>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="<?php echo $pluralVar; ?>_table">
						<thead>
							<?php foreach ($fields as $field): ?>
								<th><?php echo $field; ?></th>
							<?php endforeach; ?>
								<th class="actions">Acciones</th>
						</thead>
						<tbody>
							<?php echo "<?php"; ?> foreach ($<?php echo $pluralVar; ?> as $<?php echo $singularVar; ?>): <?php echo "?>\n"; ?>
							<tr>
								<?php
									foreach ($fields as $field) {
										$isKey = false;
										if (!empty($associations['belongsTo'])) {
											foreach ($associations['belongsTo'] as $alias => $details) {
												if ($field === $details['foreignKey']) {
													$isKey = true;
													echo "<td>\n\t<?php echo isset(\${$singularVar}['{$alias}']) && !empty(\${$singularVar}['{$alias}']['{$details['displayField']}']) ? \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'], Format::clean(\${$singularVar}['{$alias}']['{$details['displayField']}']))) : '-'; ?>\n\t</td>\n";
													break;
												}
											}
										}
										if ($isKey !== true) {
											echo "<td><?php echo isset(\${$singularVar}['{$modelClass}']) && !empty(\${$singularVar}['{$modelClass}']['{$field}']) ? \${$singularVar}['{$modelClass}']['{$field}'] : '-'; ?></td>\n";
										}
									}
								?>
								<td class="actions">
									<div class="btn-group btn-group btn-group-justified">
										<?php echo "<?php\n"; ?>
											echo $this->Html->link('<i class="fa fa-info-circle"></i> Ver', array('action' => 'view', <?php echo "\${$singularVar}['{$modelClass}']['{$primaryKey}']"; ?>, Format::clean(<?php echo "\${$singularVar}['{$modelClass}']['{$field}']"; ?>)), array('class'=>'btn btn-sm btn-info', 'escape'=>false));

											echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', <?php echo "\${$singularVar}['{$modelClass}']['{$primaryKey}']"; ?>, Format::clean(<?php echo "\${$singularVar}['{$modelClass}']['{$field}']"; ?>)), array('class'=>'btn btn-sm yellow', 'escape'=>false));

											echo $this->Html->link('<i class="fa fa-trash-o"></i> Eliminar', array('action' => 'delete', <?php echo "\${$singularVar}['{$modelClass}']['{$primaryKey}']"; ?>), array('id' => 'delete-btn', 'class'=>'btn btn-sm red', 'escape' => false, 'data-id' => <?php echo "\${$singularVar}['{$modelClass}']['{$primaryKey}']"; ?>, 'data-name' => <?php echo "\${$singularVar}['{$modelClass}']['{$field}']"; ?>));
										<?php echo "?>\n"; ?>
									</div>
								</td>
							</tr>
							<?php echo "<?php"; ?> endforeach; <?php echo "?>\n"; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- END SAMPLE TABLE PORTLET-->
	</div>
</div>
<?php echo "<?php\n"; ?>
	echo $this->Html->scriptBlock(
		'AdminIndex.init();',
		array('inline' => false)
	);
<?php echo "?>"; ?>